name = Rei_Avatar
type = body+head
scale = 1
filename = Rei_Avatar/Rei_Avatar.fbx
texdir = Rei_Avatar/textures
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointLeftHand = LeftHand
joint = jointNeck = Neck
joint = jointHead = Head
joint = jointRightHand = RightHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Spine = 11
jointIndex = LeftHandIndex3 = 30
jointIndex = RightFoot = 3
jointIndex = hair = 35
jointIndex = LeftHandIndex4 = 31
jointIndex = LeftArm = 24
jointIndex = LeftToeBase = 9
jointIndex = Spine2 = 13
jointIndex = RightHandIndex3 = 21
jointIndex = RightToe_End = 5
jointIndex = LeftFoot = 8
jointIndex = LeftHand = 27
jointIndex = polySurface1 = 16
jointIndex = LeftHandIndex2 = 29
jointIndex = Head = 33
jointIndex = RightHand = 18
jointIndex = RightHandIndex2 = 20
jointIndex = LeftToe_End = 10
jointIndex = LeftForeArm = 26
jointIndex = Hips = 0
jointIndex = RightLeg = 2
jointIndex = polySurface3 = 36
jointIndex = RightForeArm = 17
jointIndex = LeftUpLeg = 6
jointIndex = RightUpLeg = 1
jointIndex = RightToeBase = 4
jointIndex = Spine1 = 12
jointIndex = LeftShoulder = 23
jointIndex = polySurface2 = 25
jointIndex = RightHandIndex4 = 22
jointIndex = LeftLeg = 7
jointIndex = RightHandIndex1 = 19
jointIndex = HeadTop_End = 34
jointIndex = SMD_root_1 = 37
jointIndex = Neck = 32
jointIndex = LeftHandIndex1 = 28
jointIndex = RightShoulder = 14
jointIndex = RightArm = 15
