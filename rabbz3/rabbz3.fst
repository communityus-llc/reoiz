name = rabbz3
type = body+head
scale = 1
filename = rabbz3/rabbz3.fbx
texdir = rabbz3/textures
joint = jointLeftHand = LeftHand
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointRoot = Hips
joint = jointNeck = Neck
joint = jointHead = HeadTop_End
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
bs = ChinUpperRaise = UpperLipUp_Right = 0.5
bs = ChinUpperRaise = UpperLipUp_Left = 0.5
bs = MouthFrown_R = Frown_Right = 1
bs = BrowsU_R = BrowsUp_Right = 1
bs = MouthFrown_L = Frown_Left = 1
bs = BrowsU_C = BrowsUp_Right = 1
bs = BrowsU_C = BrowsUp_Left = 1
bs = LipsLowerDown = LowerLipDown_Right = 0.7
bs = LipsLowerDown = LowerLipDown_Left = 0.7
bs = LipsLowerClose = LowerLipIn = 1
bs = EyeBlink_R = Blink_Right = 1
bs = MouthDimple_L = Smile_Left = 0.25
bs = LipsUpperUp = UpperLipUp_Right = 0.7
bs = LipsUpperUp = UpperLipUp_Left = 0.7
bs = JawRight = Jaw_Right = 1
bs = MouthRight = Midmouth_Right = 1
bs = EyeBlink_L = Blink_Left = 1
bs = LipsUpperClose = UpperLipIn = 1
bs = LipsPucker = MouthNarrow_Right = 1
bs = LipsPucker = MouthNarrow_Left = 1
bs = ChinLowerRaise = Jaw_Up = 1
bs = JawLeft = JawRotateY_Left = 0.5
bs = BrowsD_R = BrowsDown_Right = 1
bs = Puff = CheekPuff_Right = 1
bs = Puff = CheekPuff_Left = 1
bs = MouthSmile_R = Smile_Right = 1
bs = Sneer = Squint_Right = 0.5
bs = Sneer = Squint_Left = 0.5
bs = Sneer = NoseScrunch_Right = 0.75
bs = Sneer = NoseScrunch_Left = 0.75
bs = BrowsD_L = BrowsDown_Left = 1
bs = EyeOpen_R = EyesWide_Right = 1
bs = MouthDimple_R = Smile_Right = 0.25
bs = LipsLowerOpen = LowerLipOut = 1
bs = LipsFunnel = TongueUp = 1
bs = LipsFunnel = MouthWhistle_NarrowAdjust_Right = 0.5
bs = LipsFunnel = MouthWhistle_NarrowAdjust_Left = 0.5
bs = LipsFunnel = MouthNarrow_Right = 1
bs = LipsFunnel = MouthNarrow_Left = 1
bs = LipsFunnel = Jaw_Down = 0.36
bs = LipsFunnel = JawForeward = 0.39
bs = MouthSmile_L = Smile_Left = 1
bs = JawFwd = JawForeward = 1
bs = MouthLeft = Midmouth_Left = 1
bs = EyeOpen_L = EyesWide_Left = 1
bs = JawOpen = MouthOpen = 0.7
bs = LipsUpperOpen = UpperLipOut = 1
bs = EyeSquint_R = Squint_Right = 1
bs = EyeSquint_L = Squint_Left = 1
bs = BrowsU_L = BrowsUp_Left = 1
jointIndex = LeftFoot = 9
jointIndex = RightHand = 18
jointIndex = Head = 24
jointIndex = Hips = 0
jointIndex = LeftShoulder = 19
jointIndex = RightForeArm = 17
jointIndex = RightShoulder = 15
jointIndex = Neck = 23
jointIndex = LeftUpLeg = 7
jointIndex = LeftToe_End = 11
jointIndex = RightLeg = 3
jointIndex = Spine1 = 13
jointIndex = RightToe_End = 6
jointIndex = LeftLeg = 8
jointIndex = RightToeBase = 5
jointIndex = RightFoot = 4
jointIndex = LeftArm = 20
jointIndex = LeftToeBase = 10
jointIndex = Spine2 = 14
jointIndex = Spine = 12
jointIndex = HeadTop_End = 25
jointIndex = LeftHand = 22
jointIndex = LeftForeArm = 21
jointIndex = RightArm = 16
jointIndex = RightUpLeg = 2
jointIndex = default1 = 1
