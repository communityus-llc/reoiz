name = Braum_Avatar1
type = body+head
scale = 1
filename = Braum_Avatar1/Braum_Avatar1.fbx
texdir = Braum_Avatar1/textures
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointRoot = Hips
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftArm = 23
jointIndex = LeftHandIndex1 = 26
jointIndex = HeadTop_End = 32
jointIndex = Spine1 = 12
jointIndex = RightHandIndex4 = 21
jointIndex = LeftHandIndex2 = 27
jointIndex = LeftFoot = 8
jointIndex = RightHandIndex1 = 18
jointIndex = RightShoulder = 14
jointIndex = LeftUpLeg = 6
jointIndex = polySurface7 = 33
jointIndex = RightHand = 17
jointIndex = LeftHandIndex3 = 28
jointIndex = LeftHand = 25
jointIndex = LeftHandIndex4 = 29
jointIndex = Hips = 0
jointIndex = LeftLeg = 7
jointIndex = Spine2 = 13
jointIndex = RightFoot = 3
jointIndex = LeftForeArm = 24
jointIndex = LeftShoulder = 22
jointIndex = RightHandIndex2 = 19
jointIndex = RightForeArm = 16
jointIndex = RightHandIndex3 = 20
jointIndex = RightLeg = 2
jointIndex = Head = 31
jointIndex = RightArm = 15
jointIndex = LeftToeBase = 9
jointIndex = RightToeBase = 4
jointIndex = LeftToe_End = 10
jointIndex = RightUpLeg = 1
jointIndex = RightToe_End = 5
jointIndex = Spine = 11
jointIndex = Neck = 30
