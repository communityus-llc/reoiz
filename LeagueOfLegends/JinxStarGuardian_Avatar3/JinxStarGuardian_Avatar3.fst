name = JinxStarGuardian_Avatar3
type = body+head
scale = 1
filename = JinxStarGuardian_Avatar3/JinxStarGuardian_Avatar3.fbx
texdir = JinxStarGuardian_Avatar3/textures
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
joint = jointRoot = Hips
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointHead = Head
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandRing1 = 29
jointIndex = LeftHandThumb4 = 68
jointIndex = Head = 70
jointIndex = LeftHandIndex2 = 62
jointIndex = RightHandMiddle2 = 34
jointIndex = RightHandIndex4 = 40
jointIndex = polySurface6 = 5
jointIndex = RightHandPinky1 = 25
jointIndex = LeftFoot = 15
jointIndex = LeftHandIndex4 = 64
jointIndex = Neck = 69
jointIndex = LeftHandPinky2 = 50
jointIndex = HeadTop_End = 71
jointIndex = LeftHandMiddle3 = 59
jointIndex = LeftUpLeg = 13
jointIndex = Spine2 = 20
jointIndex = LeftShoulder = 45
jointIndex = LeftHandMiddle2 = 58
jointIndex = LeftHandRing2 = 54
jointIndex = LeftHandThumb3 = 67
jointIndex = RightToe_End = 12
jointIndex = RightHandThumb2 = 42
jointIndex = RightFoot = 10
jointIndex = RightHandThumb4 = 44
jointIndex = polySurface7 = 3
jointIndex = LeftLeg = 14
jointIndex = transform3 = 6
jointIndex = RightHandIndex2 = 38
jointIndex = LeftHandThumb2 = 66
jointIndex = LeftHandPinky4 = 52
jointIndex = RightHandRing4 = 32
jointIndex = LeftHandPinky1 = 49
jointIndex = transformJinx_Skin04 = 1
jointIndex = RightHandIndex1 = 37
jointIndex = LeftHandMiddle1 = 57
jointIndex = LeftHandRing1 = 53
jointIndex = RightHandPinky4 = 28
jointIndex = LeftToeBase = 16
jointIndex = LeftHandIndex1 = 61
jointIndex = RightUpLeg = 8
jointIndex = Hips = 7
jointIndex = transform1 = 2
jointIndex = RightHandThumb3 = 43
jointIndex = RightHandMiddle1 = 33
jointIndex = Spine = 18
jointIndex = LeftToe_End = 17
jointIndex = LeftHandThumb1 = 65
jointIndex = RightToeBase = 11
jointIndex = LeftHandRing3 = 55
jointIndex = RightHandIndex3 = 39
jointIndex = RightHandPinky3 = 27
jointIndex = RightShoulder = 21
jointIndex = RightHandRing3 = 31
jointIndex = RightLeg = 9
jointIndex = RightHandRing2 = 30
jointIndex = RightHandMiddle3 = 35
jointIndex = RightHandMiddle4 = 36
jointIndex = RightForeArm = 23
jointIndex = LeftHandRing4 = 56
jointIndex = RightHandThumb1 = 41
jointIndex = LeftHandIndex3 = 63
jointIndex = LeftHandPinky3 = 51
jointIndex = RightHand = 24
jointIndex = polySurface8 = 72
jointIndex = LeftForeArm = 47
jointIndex = Spine1 = 19
jointIndex = LeftHand = 48
jointIndex = RightArm = 22
jointIndex = LeftArm = 46
jointIndex = RightHandPinky2 = 26
jointIndex = LeftHandMiddle4 = 60
jointIndex = transform2 = 4
