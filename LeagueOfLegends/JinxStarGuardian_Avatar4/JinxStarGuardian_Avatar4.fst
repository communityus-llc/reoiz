name = JinxStarGuardian_Avatar4
type = body+head
scale = 0.8
filename = JinxStarGuardian_Avatar4/JinxStarGuardian_Avatar4.fbx
texdir = JinxStarGuardian_Avatar4/textures
joint = jointRoot = Hips
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointHead = Head
joint = jointLeftHand = LeftHand
joint = jointNeck = Neck
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandIndex1 = 30
jointIndex = LeftHandRing3 = 48
jointIndex = RightHandMiddle3 = 28
jointIndex = Neck = 62
jointIndex = LeftHandIndex4 = 57
jointIndex = transformJinx_Skin04 = 66
jointIndex = LeftArm = 39
jointIndex = Hips = 0
jointIndex = LeftHandThumb1 = 58
jointIndex = RightHandPinky1 = 18
jointIndex = RightFoot = 3
jointIndex = LeftHandRing2 = 47
jointIndex = Head = 63
jointIndex = LeftShoulder = 38
jointIndex = LeftToe_End = 10
jointIndex = RightHandIndex3 = 32
jointIndex = LeftLeg = 7
jointIndex = LeftHandThumb3 = 60
jointIndex = polySurface6 = 70
jointIndex = RightHandIndex4 = 33
jointIndex = LeftHandMiddle3 = 52
jointIndex = Spine2 = 13
jointIndex = LeftHandPinky2 = 43
jointIndex = LeftFoot = 8
jointIndex = LeftHandRing1 = 46
jointIndex = LeftUpLeg = 6
jointIndex = RightHandIndex2 = 31
jointIndex = LeftHandMiddle1 = 50
jointIndex = LeftHandIndex1 = 54
jointIndex = RightLeg = 2
jointIndex = LeftHandRing4 = 49
jointIndex = RightUpLeg = 1
jointIndex = RightShoulder = 14
jointIndex = LeftToeBase = 9
jointIndex = RightHandRing1 = 22
jointIndex = polySurface7 = 68
jointIndex = RightForeArm = 16
jointIndex = HeadTop_End = 64
jointIndex = RightHandRing2 = 23
jointIndex = RightHandMiddle4 = 29
jointIndex = RightHandMiddle1 = 26
jointIndex = LeftHand = 41
jointIndex = RightArm = 15
jointIndex = RightHandThumb4 = 37
jointIndex = RightHandPinky3 = 20
jointIndex = Spine1 = 12
jointIndex = polySurface8 = 72
jointIndex = LeftForeArm = 40
jointIndex = RightHandRing4 = 25
jointIndex = RightHandThumb3 = 36
jointIndex = LeftHandMiddle4 = 53
jointIndex = RightToeBase = 4
jointIndex = RightHandPinky2 = 19
jointIndex = LeftHandMiddle2 = 51
jointIndex = RightHandMiddle2 = 27
jointIndex = LeftHandIndex3 = 56
jointIndex = RightHandRing3 = 24
jointIndex = RightHandThumb1 = 34
jointIndex = LeftHandPinky4 = 45
jointIndex = LeftHandIndex2 = 55
jointIndex = LeftHandPinky1 = 42
jointIndex = Spine = 11
jointIndex = LeftHandThumb4 = 61
jointIndex = transform2 = 69
jointIndex = RightHandPinky4 = 21
jointIndex = transform1 = 67
jointIndex = transform3 = 71
jointIndex = RightToe_End = 5
jointIndex = LeftHandThumb2 = 59
jointIndex = LeftHandPinky3 = 44
jointIndex = RightHand = 17
jointIndex = RightHandThumb2 = 35
