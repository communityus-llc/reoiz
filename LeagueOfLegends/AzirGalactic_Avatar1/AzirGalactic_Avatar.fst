name = AzirGalactic_Avatar
type = body+head
scale = 0.9
filename = AzirGalactic_Avatar/AzirGalactic_Avatar.fbx
texdir = AzirGalactic_Avatar/textures
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointLeftHand = LeftHand
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLean = Spine
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandIndex1 = 22
jointIndex = RightForeArm = 16
jointIndex = RightToeBase = 4
jointIndex = RightShoulder = 14
jointIndex = Spine = 11
jointIndex = RightArm = 15
jointIndex = RightHandRing2 = 19
jointIndex = LeftToe_End = 10
jointIndex = LeftHandIndex2 = 39
jointIndex = RightHandThumb3 = 28
jointIndex = LeftArm = 31
jointIndex = LeftHandRing1 = 34
jointIndex = LeftHandIndex3 = 40
jointIndex = LeftFoot = 8
jointIndex = LeftHandThumb2 = 43
jointIndex = LeftLeg = 7
jointIndex = RightLeg = 2
jointIndex = RightHandRing4 = 21
jointIndex = LeftToeBase = 9
jointIndex = LeftHand = 33
jointIndex = RightHandThumb1 = 26
jointIndex = LeftHandRing2 = 35
jointIndex = RightHandRing3 = 20
jointIndex = RightHandRing1 = 18
jointIndex = RightToe_End = 5
jointIndex = Hips = 0
jointIndex = RightHandThumb2 = 27
jointIndex = LeftHandThumb1 = 42
jointIndex = RightHandIndex2 = 23
jointIndex = polySurface7 = 50
jointIndex = Head = 47
jointIndex = LeftUpLeg = 6
jointIndex = RightHandThumb4 = 29
jointIndex = Neck = 46
jointIndex = Spine2 = 13
jointIndex = HeadTop_End = 48
jointIndex = LeftHandRing4 = 37
jointIndex = RightHandIndex4 = 25
jointIndex = Spine1 = 12
jointIndex = LeftHandIndex4 = 41
jointIndex = polySurface8 = 49
jointIndex = RightHand = 17
jointIndex = LeftHandIndex1 = 38
jointIndex = RightUpLeg = 1
jointIndex = LeftForeArm = 32
jointIndex = RightHandIndex3 = 24
jointIndex = LeftHandThumb3 = 44
jointIndex = RightFoot = 3
jointIndex = LeftHandThumb4 = 45
jointIndex = LeftShoulder = 30
jointIndex = LeftHandRing3 = 36
