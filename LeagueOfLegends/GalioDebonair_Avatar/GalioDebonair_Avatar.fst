name = GalioDebonair_Avatar
type = body+head
scale = .85
filename = GalioDebonair_Avatar/GalioDebonair_Avatar.fbx
texdir = GalioDebonair_Avatar/textures
joint = jointRoot = Hips
joint = jointLean = Spine
joint = jointHead = Head
joint = jointNeck = Neck
joint = jointRightHand = RightHand
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandIndex4 = 25
jointIndex = Head = 47
jointIndex = LeftHandThumb1 = 42
jointIndex = LeftLeg = 7
jointIndex = RightUpLeg = 1
jointIndex = RightHandIndex2 = 23
jointIndex = RightLeg = 2
jointIndex = Spine2 = 13
jointIndex = RightForeArm = 16
jointIndex = RightHandMiddle4 = 21
jointIndex = HeadTop_End = 48
jointIndex = RightToeBase = 4
jointIndex = RightHandMiddle3 = 20
jointIndex = LeftHandMiddle4 = 37
jointIndex = LeftHandIndex1 = 38
jointIndex = RightHandThumb1 = 26
jointIndex = RightHandThumb4 = 29
jointIndex = RightHandMiddle1 = 18
jointIndex = RightHandMiddle2 = 19
jointIndex = RightToe_End = 5
jointIndex = LeftHandMiddle1 = 34
jointIndex = RightShoulder = 14
jointIndex = LeftHandIndex3 = 40
jointIndex = LeftShoulder = 30
jointIndex = LeftArm = 31
jointIndex = RightHandIndex3 = 24
jointIndex = LeftHandThumb4 = 45
jointIndex = LeftHandMiddle3 = 36
jointIndex = LeftFoot = 8
jointIndex = default1 = 49
jointIndex = LeftHandThumb2 = 43
jointIndex = RightHandThumb2 = 27
jointIndex = LeftHandThumb3 = 44
jointIndex = RightArm = 15
jointIndex = RightHandThumb3 = 28
jointIndex = RightHandIndex1 = 22
jointIndex = Spine1 = 12
jointIndex = LeftForeArm = 32
jointIndex = RightHand = 17
jointIndex = LeftUpLeg = 6
jointIndex = LeftHandIndex2 = 39
jointIndex = Neck = 46
jointIndex = LeftHandMiddle2 = 35
jointIndex = Hips = 0
jointIndex = RightFoot = 3
jointIndex = LeftToeBase = 9
jointIndex = Spine = 11
jointIndex = LeftHand = 33
jointIndex = LeftHandIndex4 = 41
jointIndex = LeftToe_End = 10
