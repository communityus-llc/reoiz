name = TristanaDragonTrainer_Avatar
type = body+head
scale = 1
filename = TristanaDragonTrainer_Avatar/TristanaDragonTrainer_Avatar.fbx
texdir = TristanaDragonTrainer_Avatar/textures
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
joint = jointHead = Head
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Spine = 11
jointIndex = Neck = 23
jointIndex = Head = 24
jointIndex = LeftShoulder = 18
jointIndex = RightToeBase = 4
jointIndex = LeftToeBase = 9
jointIndex = LeftUpLeg = 6
jointIndex = LeftHandThumb1 = 22
jointIndex = LeftToe_End = 10
jointIndex = LeftForeArm = 20
jointIndex = polySurface18 = 27
jointIndex = Spine2 = 13
jointIndex = RightUpLeg = 1
jointIndex = polySurface11 = 26
jointIndex = RightFoot = 3
jointIndex = LeftLeg = 7
jointIndex = RightShoulder = 14
jointIndex = LeftArm = 19
jointIndex = RightToe_End = 5
jointIndex = Hips = 0
jointIndex = HeadTop_End = 25
jointIndex = RightArm = 15
jointIndex = LeftFoot = 8
jointIndex = RightHand = 17
jointIndex = RightLeg = 2
jointIndex = LeftHand = 21
jointIndex = RightForeArm = 16
jointIndex = Spine1 = 12
