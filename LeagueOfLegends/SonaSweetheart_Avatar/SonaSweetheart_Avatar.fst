name = SonaSweetheart_Avatar
type = body+head
scale = 1
filename = SonaSweetheart_Avatar/SonaSweetheart_Avatar.fbx
texdir = SonaSweetheart_Avatar/textures
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandMiddle1 = 16
jointIndex = LeftHandIndex2 = 45
jointIndex = RightArm = 5
jointIndex = LeftHandMiddle1 = 40
jointIndex = LeftHandMiddle3 = 42
jointIndex = LeftHandRing4 = 39
jointIndex = LeftHandMiddle2 = 41
jointIndex = RightHandRing1 = 12
jointIndex = RightForeArm = 6
jointIndex = RightHandPinky4 = 11
jointIndex = RightHandThumb3 = 26
jointIndex = LeftHandThumb1 = 48
jointIndex = Spine = 1
jointIndex = LeftHandPinky4 = 35
jointIndex = RightHandIndex3 = 22
jointIndex = LeftShoulder = 28
jointIndex = Spine2 = 3
jointIndex = LeftForeArm = 30
jointIndex = LeftHandThumb4 = 51
jointIndex = LeftHandRing1 = 36
jointIndex = LeftHandPinky1 = 32
jointIndex = LeftHandIndex3 = 46
jointIndex = default1 = 55
jointIndex = LeftHandIndex1 = 44
jointIndex = RightHandThumb1 = 24
jointIndex = RightHandPinky2 = 9
jointIndex = RightHandMiddle3 = 18
jointIndex = RightHandRing4 = 15
jointIndex = LeftHandThumb2 = 49
jointIndex = RightHandMiddle4 = 19
jointIndex = Hips = 0
jointIndex = Head = 53
jointIndex = RightHandPinky3 = 10
jointIndex = LeftHandMiddle4 = 43
jointIndex = RightHandThumb2 = 25
jointIndex = Spine1 = 2
jointIndex = LeftHandThumb3 = 50
jointIndex = LeftHandRing3 = 38
jointIndex = RightHandThumb4 = 27
jointIndex = HeadTop_End = 54
jointIndex = RightHandRing2 = 13
jointIndex = LeftHand = 31
jointIndex = LeftHandPinky2 = 33
jointIndex = RightHand = 7
jointIndex = RightHandMiddle2 = 17
jointIndex = Neck = 52
jointIndex = RightHandRing3 = 14
jointIndex = RightHandIndex2 = 21
jointIndex = LeftArm = 29
jointIndex = RightShoulder = 4
jointIndex = LeftHandPinky3 = 34
jointIndex = RightHandPinky1 = 8
jointIndex = RightHandIndex4 = 23
jointIndex = RightHandIndex1 = 20
jointIndex = LeftHandIndex4 = 47
jointIndex = LeftHandRing2 = 37
