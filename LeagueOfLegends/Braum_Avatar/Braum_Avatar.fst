name = Braum_Avatar
type = body+head
scale = 1
filename = Braum_Avatar/Braum_Avatar.fbx
texdir = Braum_Avatar/textures
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointRoot = Hips
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Neck = 30
jointIndex = RightForeArm = 16
jointIndex = LeftToeBase = 9
jointIndex = RightHandIndex3 = 20
jointIndex = polySurface5 = 37
jointIndex = Spine2 = 13
jointIndex = RightHandIndex2 = 19
jointIndex = RightArm = 15
jointIndex = polySurface6 = 39
jointIndex = polySurface7 = 49
jointIndex = LeftArm = 23
jointIndex = transform7 = 34
jointIndex = LeftLeg = 7
jointIndex = RightFoot = 3
jointIndex = polySurface4 = 41
jointIndex = Spine = 11
jointIndex = polySurface2 = 45
jointIndex = transform5 = 46
jointIndex = Head = 31
jointIndex = transform6 = 38
jointIndex = RightToeBase = 4
jointIndex = LeftHandIndex3 = 28
jointIndex = transform8 = 40
jointIndex = transform2 = 42
jointIndex = LeftForeArm = 24
jointIndex = LeftHandIndex2 = 27
jointIndex = RightToe_End = 5
jointIndex = RightHandIndex4 = 21
jointIndex = Hips = 0
jointIndex = LeftHandIndex1 = 26
jointIndex = LeftToe_End = 10
jointIndex = polySurface3 = 43
jointIndex = polySurface1 = 47
jointIndex = transform3 = 48
jointIndex = RightHand = 17
jointIndex = transform4 = 44
jointIndex = RightLeg = 2
jointIndex = RightShoulder = 14
jointIndex = LeftFoot = 8
jointIndex = LeftShoulder = 22
jointIndex = LeftHand = 25
jointIndex = braum = 35
jointIndex = RightUpLeg = 1
jointIndex = HeadTop_End = 32
jointIndex = LeftUpLeg = 6
jointIndex = transform1 = 36
jointIndex = LeftHandIndex4 = 29
jointIndex = Spine1 = 12
jointIndex = RightHandIndex1 = 18
