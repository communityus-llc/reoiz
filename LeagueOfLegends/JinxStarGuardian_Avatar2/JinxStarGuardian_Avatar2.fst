name = JinxStarGuardian_Avatar2
type = body+head
scale = 1
filename = JinxStarGuardian_Avatar2/JinxStarGuardian_Avatar2.fbx
texdir = JinxStarGuardian_Avatar2/textures
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
joint = jointRoot = Hips
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointHead = Head
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandRing1 = 22
jointIndex = Head = 63
jointIndex = LeftHandThumb4 = 61
jointIndex = LeftHandIndex2 = 55
jointIndex = RightHandMiddle2 = 27
jointIndex = RightHandIndex4 = 33
jointIndex = polySurface6 = 70
jointIndex = RightHandPinky1 = 18
jointIndex = LeftFoot = 8
jointIndex = LeftHandIndex4 = 57
jointIndex = Neck = 62
jointIndex = LeftHandPinky2 = 43
jointIndex = HeadTop_End = 64
jointIndex = LeftHandMiddle3 = 52
jointIndex = LeftUpLeg = 6
jointIndex = Spine2 = 13
jointIndex = LeftShoulder = 38
jointIndex = LeftHandMiddle2 = 51
jointIndex = LeftHandRing2 = 47
jointIndex = LeftHandThumb3 = 60
jointIndex = RightToe_End = 5
jointIndex = RightHandThumb2 = 35
jointIndex = RightFoot = 3
jointIndex = RightHandThumb4 = 37
jointIndex = polySurface7 = 68
jointIndex = LeftLeg = 7
jointIndex = transform3 = 71
jointIndex = RightHandIndex2 = 31
jointIndex = LeftHandThumb2 = 59
jointIndex = LeftHandPinky4 = 45
jointIndex = RightHandRing4 = 25
jointIndex = LeftHandPinky1 = 42
jointIndex = transformJinx_Skin04 = 66
jointIndex = RightHandIndex1 = 30
jointIndex = LeftHandMiddle1 = 50
jointIndex = LeftHandRing1 = 46
jointIndex = RightHandPinky4 = 21
jointIndex = LeftToeBase = 9
jointIndex = LeftHandIndex1 = 54
jointIndex = RightUpLeg = 1
jointIndex = Hips = 0
jointIndex = transform1 = 67
jointIndex = RightHandThumb3 = 36
jointIndex = RightHandMiddle1 = 26
jointIndex = Spine = 11
jointIndex = LeftToe_End = 10
jointIndex = LeftHandThumb1 = 58
jointIndex = RightToeBase = 4
jointIndex = LeftHandRing3 = 48
jointIndex = RightHandIndex3 = 32
jointIndex = RightHandPinky3 = 20
jointIndex = RightShoulder = 14
jointIndex = RightHandRing3 = 24
jointIndex = RightLeg = 2
jointIndex = RightHandRing2 = 23
jointIndex = RightHandMiddle3 = 28
jointIndex = RightHandMiddle4 = 29
jointIndex = RightForeArm = 16
jointIndex = LeftHandRing4 = 49
jointIndex = RightHandThumb1 = 34
jointIndex = LeftHandIndex3 = 56
jointIndex = LeftHandPinky3 = 44
jointIndex = RightHand = 17
jointIndex = polySurface8 = 72
jointIndex = LeftForeArm = 40
jointIndex = Spine1 = 12
jointIndex = LeftHand = 41
jointIndex = RightArm = 15
jointIndex = LeftArm = 39
jointIndex = RightHandPinky2 = 19
jointIndex = LeftHandMiddle4 = 53
jointIndex = transform2 = 69
