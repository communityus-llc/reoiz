name = Aatrox_Avatar
type = body+head
scale = 1.6
filename = Aatrox_Avatar/Aatrox_Avatar.fbx
texdir = Aatrox_Avatar/textures
joint = jointLeftHand = LeftHand
joint = jointRoot = Hips
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointHead = Head
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandRing1 = 22
jointIndex = RightArm = 15
jointIndex = LeftHandThumb3 = 60
jointIndex = LeftFoot = 8
jointIndex = LeftHandMiddle3 = 52
jointIndex = RightHandMiddle3 = 28
jointIndex = LeftHandIndex4 = 57
jointIndex = RightHandPinky2 = 19
jointIndex = Hips = 0
jointIndex = LeftHandRing2 = 47
jointIndex = LeftHandThumb2 = 59
jointIndex = RightHandRing4 = 25
jointIndex = LeftHandIndex3 = 56
jointIndex = RightToe_End = 5
jointIndex = HeadTop_End = 64
jointIndex = LeftToeBase = 9
jointIndex = RightForeArm = 16
jointIndex = RightToeBase = 4
jointIndex = RightFoot = 3
jointIndex = LeftHandMiddle1 = 50
jointIndex = LeftHandRing4 = 49
jointIndex = RightHandPinky1 = 18
jointIndex = RightHandIndex2 = 31
jointIndex = LeftHandPinky3 = 44
jointIndex = LeftHandThumb1 = 58
jointIndex = RightLeg = 2
jointIndex = LeftHandRing1 = 46
jointIndex = RightHandThumb3 = 36
jointIndex = polySurface4 = 68
jointIndex = LeftShoulder = 38
jointIndex = RightHandThumb4 = 37
jointIndex = LeftHandIndex2 = 55
jointIndex = RightHandIndex1 = 30
jointIndex = RightHandIndex3 = 32
jointIndex = Spine1 = 12
jointIndex = RightShoulder = 14
jointIndex = LeftToe_End = 10
jointIndex = LeftArm = 39
jointIndex = LeftHandMiddle4 = 53
jointIndex = polySurface3 = 70
jointIndex = RightHandRing3 = 24
jointIndex = LeftHandRing3 = 48
jointIndex = transform2 = 71
jointIndex = LeftUpLeg = 6
jointIndex = LeftHand = 41
jointIndex = RightHand = 17
jointIndex = transformAatrox = 66
jointIndex = RightHandMiddle4 = 29
jointIndex = RightHandMiddle1 = 26
jointIndex = transform1 = 67
jointIndex = polySurface1 = 72
jointIndex = RightHandPinky4 = 21
jointIndex = RightHandThumb1 = 34
jointIndex = transform3 = 69
jointIndex = RightHandRing2 = 23
jointIndex = LeftHandPinky2 = 43
jointIndex = RightHandMiddle2 = 27
jointIndex = Spine2 = 13
jointIndex = RightHandPinky3 = 20
jointIndex = LeftHandMiddle2 = 51
jointIndex = LeftHandPinky4 = 45
jointIndex = Neck = 62
jointIndex = RightUpLeg = 1
jointIndex = LeftForeArm = 40
jointIndex = RightHandIndex4 = 33
jointIndex = Head = 63
jointIndex = LeftHandIndex1 = 54
jointIndex = Spine = 11
jointIndex = LeftHandPinky1 = 42
jointIndex = LeftLeg = 7
jointIndex = RightHandThumb2 = 35
jointIndex = LeftHandThumb4 = 61
