name = JinxStarGuardian_Avatar6
type = body+head
scale = .8
filename = JinxStarGuardian_Avatar6/JinxStarGuardian_Avatar6.fbx
texdir = JinxStarGuardian_Avatar6/textures
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointHead = Head
joint = jointNeck = Neck
joint = jointRoot = Hips
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftShoulder = 39
jointIndex = LeftLeg = 8
jointIndex = LeftToe_End = 11
jointIndex = LeftFoot = 9
jointIndex = LeftHandRing2 = 48
jointIndex = LeftUpLeg = 7
jointIndex = RightUpLeg = 2
jointIndex = RightLeg = 3
jointIndex = LeftHandRing3 = 49
jointIndex = LeftHandIndex3 = 57
jointIndex = RightForeArm = 17
jointIndex = LeftHandThumb1 = 59
jointIndex = RightHandMiddle3 = 29
jointIndex = LeftHandRing1 = 47
jointIndex = Neck = 63
jointIndex = RightHandPinky3 = 21
jointIndex = LeftHandThumb3 = 61
jointIndex = RightHandThumb3 = 37
jointIndex = LeftHandIndex1 = 55
jointIndex = LeftHandMiddle2 = 52
jointIndex = RightHandPinky2 = 20
jointIndex = LeftHandPinky3 = 45
jointIndex = LeftHandPinky1 = 43
jointIndex = LeftHandPinky4 = 46
jointIndex = RightHandPinky1 = 19
jointIndex = RightHandRing1 = 23
jointIndex = Spine2 = 14
jointIndex = RightHandRing4 = 26
jointIndex = RightHand = 18
jointIndex = LeftHandThumb2 = 60
jointIndex = LeftHandRing4 = 50
jointIndex = LeftHandMiddle1 = 51
jointIndex = RightHandMiddle2 = 28
jointIndex = LeftHandMiddle3 = 53
jointIndex = LeftHandMiddle4 = 54
jointIndex = Hips = 1
jointIndex = LeftArm = 40
jointIndex = RightToeBase = 5
jointIndex = RightHandThumb2 = 36
jointIndex = RightFoot = 4
jointIndex = polySurface8 = 66
jointIndex = LeftHandPinky2 = 44
jointIndex = RightHandThumb1 = 35
jointIndex = LeftHandThumb4 = 62
jointIndex = RightHandMiddle1 = 27
jointIndex = RightToe_End = 6
jointIndex = Head = 64
jointIndex = Spine = 12
jointIndex = RightHandIndex4 = 34
jointIndex = polySurface9 = 0
jointIndex = HeadTop_End = 65
jointIndex = RightHandRing2 = 24
jointIndex = LeftHandIndex4 = 58
jointIndex = Spine1 = 13
jointIndex = LeftHand = 42
jointIndex = RightHandThumb4 = 38
jointIndex = RightArm = 16
jointIndex = LeftForeArm = 41
jointIndex = LeftToeBase = 10
jointIndex = RightHandMiddle4 = 30
jointIndex = RightShoulder = 15
jointIndex = RightHandIndex3 = 33
jointIndex = LeftHandIndex2 = 56
jointIndex = RightHandRing3 = 25
jointIndex = RightHandIndex2 = 32
jointIndex = RightHandPinky4 = 22
jointIndex = RightHandIndex1 = 31
