name = LeonaProject_Avatar
type = body+head
scale = 1
filename = LeonaProject_Avatar/LeonaProject_Avatar.fbx
texdir = LeonaProject_Avatar/textures
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandMiddle1 = 26
jointIndex = LeftHandIndex2 = 51
jointIndex = RightArm = 15
jointIndex = LeftHandMiddle1 = 46
jointIndex = LeftHandMiddle3 = 48
jointIndex = LeftHandRing4 = 45
jointIndex = LeftHandMiddle2 = 47
jointIndex = LeftUpLeg = 6
jointIndex = RightHandRing1 = 22
jointIndex = RightForeArm = 16
jointIndex = RightHandPinky4 = 21
jointIndex = RightToeBase = 4
jointIndex = LeftHandThumb1 = 54
jointIndex = Spine = 11
jointIndex = RightHandIndex3 = 32
jointIndex = LeftShoulder = 34
jointIndex = LeftHandPinky4 = 41
jointIndex = Spine2 = 13
jointIndex = RightFoot = 3
jointIndex = LeftForeArm = 36
jointIndex = LeftHandThumb4 = 57
jointIndex = LeftHandPinky1 = 38
jointIndex = LeftHandRing1 = 42
jointIndex = LeftHandIndex3 = 52
jointIndex = RightUpLeg = 1
jointIndex = LeftHandIndex1 = 50
jointIndex = RightHandPinky2 = 19
jointIndex = RightHandMiddle3 = 28
jointIndex = RightHandRing4 = 25
jointIndex = LeftHandThumb2 = 55
jointIndex = RightHandMiddle4 = 29
jointIndex = RightLeg = 2
jointIndex = LeftFoot = 8
jointIndex = Hips = 0
jointIndex = Head = 59
jointIndex = RightHandPinky3 = 20
jointIndex = LeftHandMiddle4 = 49
jointIndex = Spine1 = 12
jointIndex = LeftHandThumb3 = 56
jointIndex = LeftHandRing3 = 44
jointIndex = HeadTop_End = 60
jointIndex = Model = 61
jointIndex = RightHandRing2 = 23
jointIndex = RightToe_End = 5
jointIndex = LeftHand = 37
jointIndex = LeftHandPinky2 = 39
jointIndex = RightHand = 17
jointIndex = LeftToe_End = 10
jointIndex = RightHandMiddle2 = 27
jointIndex = Neck = 58
jointIndex = RightHandRing3 = 24
jointIndex = RightHandIndex2 = 31
jointIndex = LeftArm = 35
jointIndex = LeftLeg = 7
jointIndex = transform1 = 64
jointIndex = RightShoulder = 14
jointIndex = LeftToeBase = 9
jointIndex = LeftHandPinky3 = 40
jointIndex = polySurface9 = 63
jointIndex = RightHandPinky1 = 18
jointIndex = RightHandIndex4 = 33
jointIndex = RightHandIndex1 = 30
jointIndex = LeftHandIndex4 = 53
jointIndex = LeftHandRing2 = 43
jointIndex = polySurface8 = 62
