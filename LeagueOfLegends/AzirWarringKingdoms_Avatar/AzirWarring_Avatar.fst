name = AzirWarring_Avatar
type = body+head
scale = 0.9
filename = AzirWarring_Avatar/AzirWarring_Avatar.fbx
texdir = AzirWarring_Avatar/textures
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointLeftHand = LeftHand
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLean = Spine
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandPinky1 = 19
jointIndex = LeftHandPinky3 = 45
jointIndex = RightFoot = 4
jointIndex = LeftHandThumb1 = 59
jointIndex = polySurface3 = 73
jointIndex = LeftHandIndex4 = 58
jointIndex = RightHandMiddle3 = 29
jointIndex = polySurface2 = 75
jointIndex = LeftHandPinky2 = 44
jointIndex = LeftForeArm = 41
jointIndex = polySurface7 = 67
jointIndex = LeftHandMiddle1 = 51
jointIndex = RightHandIndex2 = 32
jointIndex = RightHand = 18
jointIndex = Spine = 12
jointIndex = default0012 = 66
jointIndex = RightHandPinky3 = 21
jointIndex = LeftLeg = 8
jointIndex = LeftHandPinky4 = 46
jointIndex = LeftFoot = 9
jointIndex = LeftHandIndex2 = 56
jointIndex = RightHandMiddle1 = 27
jointIndex = LeftHand = 42
jointIndex = Neck = 63
jointIndex = transform3 = 76
jointIndex = transform1 = 70
jointIndex = RightHandPinky2 = 20
jointIndex = polySurface4 = 71
jointIndex = LeftHandIndex1 = 55
jointIndex = LeftArm = 40
jointIndex = LeftHandThumb2 = 60
jointIndex = RightHandMiddle2 = 28
jointIndex = transform5 = 74
jointIndex = RightHandMiddle4 = 30
jointIndex = RightHandIndex3 = 33
jointIndex = transform2 = 78
jointIndex = LeftHandMiddle4 = 54
jointIndex = LeftHandThumb4 = 62
jointIndex = RightUpLeg = 2
jointIndex = LeftHandIndex3 = 57
jointIndex = RightLeg = 3
jointIndex = LeftHandThumb3 = 61
jointIndex = RightHandRing2 = 24
jointIndex = LeftHandPinky1 = 43
jointIndex = LeftShoulder = 39
jointIndex = RightToeBase = 5
jointIndex = RightShoulder = 15
jointIndex = RightToe_End = 6
jointIndex = RightHandThumb4 = 38
jointIndex = polySurface5 = 68
jointIndex = RightHandThumb1 = 35
jointIndex = polySurface6 = 69
jointIndex = transform4 = 72
jointIndex = Head = 64
jointIndex = RightHandThumb2 = 36
jointIndex = LeftHandRing3 = 49
jointIndex = LeftHandMiddle2 = 52
jointIndex = LeftHandMiddle3 = 53
jointIndex = RightHandRing1 = 23
jointIndex = LeftToe_End = 11
jointIndex = RightHandIndex1 = 31
jointIndex = LeftHandRing4 = 50
jointIndex = RightForeArm = 17
jointIndex = HeadTop_End = 65
jointIndex = LeftToeBase = 10
jointIndex = LeftHandRing2 = 48
jointIndex = RightArm = 16
jointIndex = RightHandThumb3 = 37
jointIndex = LeftHandRing1 = 47
jointIndex = LeftUpLeg = 7
jointIndex = Spine2 = 14
jointIndex = RightHandIndex4 = 34
jointIndex = polySurface1 = 77
jointIndex = Hips = 1
jointIndex = RightHandRing4 = 26
jointIndex = RightHandRing3 = 25
jointIndex = Spine1 = 13
jointIndex = RightHandPinky4 = 22
