name = EvelynnShadow_Avatar1
type = body+head
scale = .8
filename = EvelynnShadow_Avatar1/EvelynnShadow_Avatar1.fbx
texdir = EvelynnShadow_Avatar1/textures
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointRoot = Hips
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = polySurface3 = 69
jointIndex = RightToe_End = 6
jointIndex = LeftHandMiddle3 = 53
jointIndex = RightHandIndex1 = 31
jointIndex = default0012 = 74
jointIndex = RightHandIndex2 = 32
jointIndex = RightHandRing4 = 26
jointIndex = RightShoulder = 15
jointIndex = transform1 = 77
jointIndex = RightHandPinky1 = 19
jointIndex = RightHandPinky2 = 20
jointIndex = LeftHandMiddle4 = 54
jointIndex = LeftHandIndex1 = 55
jointIndex = LeftHandIndex2 = 56
jointIndex = transform3 = 75
jointIndex = LeftFoot = 9
jointIndex = LeftForeArm = 41
jointIndex = LeftHandRing3 = 49
jointIndex = LeftHandThumb3 = 61
jointIndex = LeftHandThumb1 = 59
jointIndex = RightHandThumb4 = 38
jointIndex = polySurface1 = 72
jointIndex = RightHandMiddle4 = 30
jointIndex = Neck = 63
jointIndex = RightToeBase = 5
jointIndex = LeftHandPinky2 = 44
jointIndex = LeftHandRing2 = 48
jointIndex = default1 = 78
jointIndex = Head = 64
jointIndex = Spine1 = 13
jointIndex = RightHandIndex4 = 34
jointIndex = RightHandThumb2 = 36
jointIndex = LeftHandMiddle2 = 52
jointIndex = RightHandPinky3 = 21
jointIndex = LeftLeg = 8
jointIndex = RightUpLeg = 2
jointIndex = Spine = 12
jointIndex = RightForeArm = 17
jointIndex = RightHandMiddle1 = 27
jointIndex = LeftUpLeg = 7
jointIndex = LeftHandIndex3 = 57
jointIndex = LeftHandPinky4 = 46
jointIndex = LeftHandIndex4 = 58
jointIndex = RightFoot = 4
jointIndex = RightHandRing1 = 23
jointIndex = LeftHandThumb4 = 62
jointIndex = HeadTop_End = 65
jointIndex = LeftHandPinky1 = 43
jointIndex = RightArm = 16
jointIndex = LeftHandMiddle1 = 51
jointIndex = LeftToe_End = 11
jointIndex = RightHandMiddle2 = 28
jointIndex = RightHandRing3 = 25
jointIndex = RightHand = 18
jointIndex = LeftHandRing1 = 47
jointIndex = RightHandThumb3 = 37
jointIndex = LeftArm = 40
jointIndex = polySurface2 = 70
jointIndex = LeftHand = 42
jointIndex = transform2 = 79
jointIndex = default0011 = 76
jointIndex = RightHandRing2 = 24
jointIndex = LeftHandThumb2 = 60
jointIndex = LeftShoulder = 39
jointIndex = RightLeg = 3
jointIndex = LeftHandRing4 = 50
jointIndex = RightHandIndex3 = 33
jointIndex = RightHandPinky4 = 22
jointIndex = Spine2 = 14
jointIndex = RightHandMiddle3 = 29
jointIndex = LeftHandPinky3 = 45
jointIndex = Hips = 1
jointIndex = LeftToeBase = 10
jointIndex = RightHandThumb1 = 35
jointIndex = default003 = 66
