name = EvelynnTango_Avatar
type = body+head
scale = .8
filename = EvelynnTango_Avatar/EvelynnTango_Avatar.fbx
texdir = EvelynnTango_Avatar/textures
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointRoot = Hips
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = polySurface3 = 69
jointIndex = RightToe_End = 5
jointIndex = LeftHandMiddle3 = 52
jointIndex = RightHandIndex1 = 30
jointIndex = default0012 = 73
jointIndex = RightHandIndex2 = 31
jointIndex = RightHandRing4 = 25
jointIndex = RightShoulder = 14
jointIndex = transform1 = 76
jointIndex = RightHandPinky1 = 18
jointIndex = RightHandPinky2 = 19
jointIndex = LeftHandMiddle4 = 53
jointIndex = LeftHandIndex1 = 54
jointIndex = LeftHandIndex2 = 55
jointIndex = transform3 = 74
jointIndex = LeftFoot = 8
jointIndex = LeftForeArm = 40
jointIndex = LeftHandRing3 = 48
jointIndex = LeftHandThumb3 = 60
jointIndex = LeftHandThumb1 = 58
jointIndex = RightHandThumb4 = 37
jointIndex = polySurface1 = 71
jointIndex = RightHandMiddle4 = 29
jointIndex = Neck = 62
jointIndex = RightToeBase = 4
jointIndex = LeftHandPinky2 = 43
jointIndex = default1 = 77
jointIndex = LeftHandRing2 = 47
jointIndex = Head = 63
jointIndex = Spine1 = 12
jointIndex = RightHandIndex4 = 33
jointIndex = RightHandThumb2 = 35
jointIndex = LeftHandMiddle2 = 51
jointIndex = RightHandPinky3 = 20
jointIndex = LeftLeg = 7
jointIndex = RightUpLeg = 1
jointIndex = Spine = 11
jointIndex = RightForeArm = 16
jointIndex = RightHandMiddle1 = 26
jointIndex = LeftUpLeg = 6
jointIndex = LeftHandIndex3 = 56
jointIndex = LeftHandPinky4 = 45
jointIndex = LeftHandIndex4 = 57
jointIndex = RightFoot = 3
jointIndex = RightHandRing1 = 22
jointIndex = LeftHandThumb4 = 61
jointIndex = HeadTop_End = 64
jointIndex = LeftHandPinky1 = 42
jointIndex = RightArm = 15
jointIndex = LeftHandMiddle1 = 50
jointIndex = LeftToe_End = 10
jointIndex = RightHandMiddle2 = 27
jointIndex = RightHandRing3 = 24
jointIndex = RightHand = 17
jointIndex = LeftHandRing1 = 46
jointIndex = RightHandThumb3 = 36
jointIndex = LeftArm = 39
jointIndex = LeftHand = 41
jointIndex = transform2 = 78
jointIndex = default0011 = 75
jointIndex = RightHandRing2 = 23
jointIndex = LeftHandThumb2 = 59
jointIndex = LeftShoulder = 38
jointIndex = RightLeg = 2
jointIndex = LeftHandRing4 = 49
jointIndex = RightHandIndex3 = 32
jointIndex = RightHandPinky4 = 21
jointIndex = Spine2 = 13
jointIndex = RightHandMiddle3 = 28
jointIndex = LeftHandPinky3 = 44
jointIndex = Hips = 0
jointIndex = LeftToeBase = 9
jointIndex = RightHandThumb1 = 34
jointIndex = default003 = 66
