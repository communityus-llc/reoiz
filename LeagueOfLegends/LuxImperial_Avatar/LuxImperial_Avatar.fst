name = LuxImperial_Avatar
type = body+head
scale = 1
filename = LuxImperial_Avatar/LuxImperial_Avatar.fbx
texdir = LuxImperial_Avatar/textures
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandMiddle1 = 26
jointIndex = LeftHandIndex2 = 55
jointIndex = RightArm = 15
jointIndex = LeftHandMiddle1 = 50
jointIndex = LeftHandMiddle3 = 52
jointIndex = LeftHandRing4 = 49
jointIndex = LeftHandMiddle2 = 51
jointIndex = LeftUpLeg = 6
jointIndex = RightHandRing1 = 22
jointIndex = RightForeArm = 16
jointIndex = RightHandPinky4 = 21
jointIndex = RightHandThumb3 = 36
jointIndex = RightToeBase = 4
jointIndex = LeftHandThumb1 = 58
jointIndex = Spine = 11
jointIndex = RightHandIndex3 = 32
jointIndex = LeftShoulder = 38
jointIndex = LeftHandPinky4 = 45
jointIndex = polySurface24 = 66
jointIndex = Spine2 = 13
jointIndex = RightFoot = 3
jointIndex = LeftForeArm = 40
jointIndex = LeftHandThumb4 = 61
jointIndex = LeftHandPinky1 = 42
jointIndex = LeftHandRing1 = 46
jointIndex = LeftHandIndex3 = 56
jointIndex = polySurface23 = 65
jointIndex = RightUpLeg = 1
jointIndex = LeftHandIndex1 = 54
jointIndex = RightHandThumb1 = 34
jointIndex = RightHandPinky2 = 19
jointIndex = RightHandMiddle3 = 28
jointIndex = RightHandRing4 = 25
jointIndex = LeftHandThumb2 = 59
jointIndex = RightHandMiddle4 = 29
jointIndex = RightLeg = 2
jointIndex = LeftFoot = 8
jointIndex = Hips = 0
jointIndex = Head = 63
jointIndex = RightHandPinky3 = 20
jointIndex = LeftHandMiddle4 = 53
jointIndex = RightHandThumb2 = 35
jointIndex = Spine1 = 12
jointIndex = LeftHandThumb3 = 60
jointIndex = LeftHandRing3 = 48
jointIndex = RightHandThumb4 = 37
jointIndex = HeadTop_End = 64
jointIndex = RightHandRing2 = 23
jointIndex = RightToe_End = 5
jointIndex = LeftHand = 41
jointIndex = LeftHandPinky2 = 43
jointIndex = RightHand = 17
jointIndex = LeftToe_End = 10
jointIndex = RightHandMiddle2 = 27
jointIndex = Neck = 62
jointIndex = RightHandRing3 = 24
jointIndex = RightHandIndex2 = 31
jointIndex = LeftArm = 39
jointIndex = LeftLeg = 7
jointIndex = RightShoulder = 14
jointIndex = LeftToeBase = 9
jointIndex = LeftHandPinky3 = 44
jointIndex = RightHandPinky1 = 18
jointIndex = RightHandIndex4 = 33
jointIndex = RightHandIndex1 = 30
jointIndex = LeftHandIndex4 = 57
jointIndex = LeftHandRing2 = 47
