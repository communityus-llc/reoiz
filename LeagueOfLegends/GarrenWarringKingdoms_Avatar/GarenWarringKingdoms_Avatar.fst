name = GarenWarringKingdoms_Avatar
type = body+head
scale = 1
filename = GarenWarringKingdoms_Avatar/GarenWarringKingdoms_Avatar.fbx
texdir = GarenWarringKingdoms_Avatar/textures
joint = jointRoot = Hips
joint = jointLean = Spine
joint = jointHead = Head
joint = jointNeck = Neck
joint = jointRightHand = RightHand
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftHandMiddle3 = 53
jointIndex = RightHandPinky4 = 22
jointIndex = RightHandRing3 = 25
jointIndex = LeftHandIndex1 = 55
jointIndex = LeftForeArm = 41
jointIndex = RightHandThumb3 = 37
jointIndex = LeftHand = 42
jointIndex = RightHandIndex3 = 33
jointIndex = LeftUpLeg = 7
jointIndex = RightLeg = 3
jointIndex = LeftToeBase = 10
jointIndex = RightHandMiddle4 = 30
jointIndex = LeftHandMiddle4 = 54
jointIndex = LeftHandMiddle2 = 52
jointIndex = RightHand = 18
jointIndex = RightHandMiddle1 = 27
jointIndex = Head = 64
jointIndex = RightHandThumb1 = 35
jointIndex = RightArm = 16
jointIndex = LeftHandThumb1 = 59
jointIndex = RightHandThumb4 = 38
jointIndex = Spine2 = 14
jointIndex = RightFoot = 4
jointIndex = RightToe_End = 6
jointIndex = transform1 = 67
jointIndex = RightHandMiddle3 = 29
jointIndex = RightHandPinky2 = 20
jointIndex = RightHandIndex1 = 31
jointIndex = LeftFoot = 9
jointIndex = LeftHandThumb3 = 61
jointIndex = RightHandMiddle2 = 28
jointIndex = LeftHandRing2 = 48
jointIndex = LeftHandIndex3 = 57
jointIndex = Spine = 12
jointIndex = LeftHandThumb4 = 62
jointIndex = polySurface3 = 68
jointIndex = LeftHandPinky3 = 45
jointIndex = RightHandThumb2 = 36
jointIndex = LeftLeg = 8
jointIndex = default1 = 66
jointIndex = polySurface1 = 0
jointIndex = Neck = 63
jointIndex = LeftHandIndex2 = 56
jointIndex = Hips = 1
jointIndex = LeftArm = 40
jointIndex = LeftHandRing4 = 50
jointIndex = LeftHandRing1 = 47
jointIndex = RightToeBase = 5
jointIndex = LeftHandThumb2 = 60
jointIndex = LeftHandPinky1 = 43
jointIndex = RightUpLeg = 2
jointIndex = RightHandRing4 = 26
jointIndex = LeftHandPinky2 = 44
jointIndex = LeftToe_End = 11
jointIndex = RightHandPinky1 = 19
jointIndex = RightHandIndex4 = 34
jointIndex = RightHandIndex2 = 32
jointIndex = LeftHandIndex4 = 58
jointIndex = LeftHandMiddle1 = 51
jointIndex = RightHandPinky3 = 21
jointIndex = Spine1 = 13
jointIndex = RightShoulder = 15
jointIndex = LeftHandRing3 = 49
jointIndex = LeftShoulder = 39
jointIndex = RightHandRing2 = 24
jointIndex = RightForeArm = 17
jointIndex = HeadTop_End = 65
jointIndex = RightHandRing1 = 23
jointIndex = LeftHandPinky4 = 46
