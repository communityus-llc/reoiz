name = LuluPoolParty_Avatar
type = body+head
scale = 1
filename = LuluPoolParty_Avatar/LuluPoolParty_Avatar.fbx
texdir = LuluPoolParty_Avatar/textures
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Spine = 12
jointIndex = LeftHandThumb3 = 53
jointIndex = LeftHandThumb1 = 51
jointIndex = RightHandThumb3 = 33
jointIndex = LeftHandMiddle2 = 44
jointIndex = RightHandThumb1 = 31
jointIndex = LeftHandRing3 = 41
jointIndex = polySurface19 = 63
jointIndex = polySurface15 = 70
jointIndex = RightForeArm = 17
jointIndex = RightLeg = 3
jointIndex = RightToe_End = 6
jointIndex = LeftHand = 38
jointIndex = RightHandIndex2 = 28
jointIndex = transform2 = 77
jointIndex = LeftHandMiddle4 = 46
jointIndex = polySurface12 = 74
jointIndex = RightHandThumb4 = 34
jointIndex = LeftArm = 36
jointIndex = LeftHandIndex2 = 48
jointIndex = LeftToeBase = 10
jointIndex = RightHandThumb2 = 32
jointIndex = LeftHandIndex1 = 47
jointIndex = LeftHandRing1 = 39
jointIndex = polySurface18 = 68
jointIndex = RightFoot = 4
jointIndex = RightHandMiddle3 = 25
jointIndex = transform8 = 65
jointIndex = LeftHandRing2 = 40
jointIndex = RightUpLeg = 2
jointIndex = RightArm = 16
jointIndex = RightHandRing3 = 21
jointIndex = LeftLeg = 8
jointIndex = Neck = 55
jointIndex = default1 = 61
jointIndex = RightToeBase = 5
jointIndex = polySurface13 = 64
jointIndex = LeftHandThumb4 = 54
jointIndex = RightHandRing2 = 20
jointIndex = RightHand = 18
jointIndex = RightHandMiddle2 = 24
jointIndex = transform3 = 73
jointIndex = RightHandMiddle1 = 23
jointIndex = polySurface17 = 1
jointIndex = LeftUpLeg = 7
jointIndex = polySurface20 = 62
jointIndex = LeftHandThumb2 = 52
jointIndex = transform4 = 79
jointIndex = LeftForeArm = 37
jointIndex = RightHandMiddle4 = 26
jointIndex = transform6 = 75
jointIndex = Spine1 = 13
jointIndex = polySurface10 = 78
jointIndex = polySurface16 = 67
jointIndex = LeftHandIndex4 = 50
jointIndex = LeftShoulder = 35
jointIndex = RightHandIndex1 = 27
jointIndex = LeftHandMiddle1 = 43
jointIndex = RightHandIndex4 = 30
jointIndex = transform9 = 60
jointIndex = RightHandRing4 = 22
jointIndex = Head = 56
jointIndex = transform5 = 71
jointIndex = transform1 = 66
jointIndex = LeftToe_End = 11
jointIndex = RightShoulder = 15
jointIndex = LeftFoot = 9
jointIndex = LeftHandIndex3 = 49
jointIndex = transform7 = 69
jointIndex = polySurface11 = 76
jointIndex = Spine2 = 14
jointIndex = RightHandIndex3 = 29
jointIndex = RightHandRing1 = 19
jointIndex = LeftHandRing4 = 42
jointIndex = HeadTop_End = 57
jointIndex = polySurface14 = 72
jointIndex = LeftHandMiddle3 = 45
jointIndex = Hips = 0
