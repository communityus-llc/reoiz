name = AhriShadowfox_Avatar
type = body+head
scale = 1
filename = AhriShadowfox_Avatar/AhriShadowfox_Avatar.fbx
texdir = AhriShadowfox_Avatar/textures
joint = jointRoot = Hips
joint = jointRightHand = RightHand
joint = jointHead = Head
joint = jointLeftHand = LeftHand
joint = jointNeck = Neck
joint = jointLean = Spine
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftHandIndex4 = 57
jointIndex = LeftHandPinky1 = 42
jointIndex = mesh0 = 65
jointIndex = LeftHandIndex1 = 54
jointIndex = Spine2 = 13
jointIndex = RightHandIndex2 = 31
jointIndex = LeftHandMiddle4 = 53
jointIndex = RightHandThumb2 = 35
jointIndex = Spine = 11
jointIndex = RightHandIndex1 = 30
jointIndex = RightHandPinky1 = 18
jointIndex = LeftHandIndex3 = 56
jointIndex = RightForeArm = 16
jointIndex = LeftShoulder = 38
jointIndex = LeftHandMiddle1 = 50
jointIndex = RightHandMiddle4 = 29
jointIndex = RightToe_End = 5
jointIndex = RightHandPinky2 = 19
jointIndex = LeftToeBase = 9
jointIndex = LeftHandPinky2 = 43
jointIndex = RightHandMiddle1 = 26
jointIndex = LeftHandMiddle2 = 51
jointIndex = LeftFoot = 8
jointIndex = RightHandPinky3 = 20
jointIndex = LeftForeArm = 40
jointIndex = RightHandThumb4 = 37
jointIndex = RightHandThumb1 = 34
jointIndex = LeftHand = 41
jointIndex = RightHandMiddle2 = 27
jointIndex = LeftHandThumb3 = 60
jointIndex = LeftToe_End = 10
jointIndex = RightArm = 15
jointIndex = RightHandRing2 = 23
jointIndex = RightUpLeg = 1
jointIndex = RightHandThumb3 = 36
jointIndex = LeftHandRing2 = 47
jointIndex = RightHandRing4 = 25
jointIndex = RightHandPinky4 = 21
jointIndex = RightHand = 17
jointIndex = Head = 63
jointIndex = LeftArm = 39
jointIndex = RightShoulder = 14
jointIndex = RightHandIndex3 = 32
jointIndex = LeftHandRing1 = 46
jointIndex = LeftUpLeg = 6
jointIndex = RightFoot = 3
jointIndex = LeftLeg = 7
jointIndex = RightToeBase = 4
jointIndex = RightHandMiddle3 = 28
jointIndex = RightHandIndex4 = 33
jointIndex = LeftHandPinky3 = 44
jointIndex = Spine1 = 12
jointIndex = LeftHandRing4 = 49
jointIndex = LeftHandThumb1 = 58
jointIndex = HeadTop_End = 64
jointIndex = LeftHandThumb2 = 59
jointIndex = LeftHandPinky4 = 45
jointIndex = Neck = 62
jointIndex = LeftHandIndex2 = 55
jointIndex = RightHandRing3 = 24
jointIndex = RightHandRing1 = 22
jointIndex = LeftHandRing3 = 48
jointIndex = LeftHandMiddle3 = 52
jointIndex = LeftHandThumb4 = 61
jointIndex = Hips = 0
jointIndex = RightLeg = 2
