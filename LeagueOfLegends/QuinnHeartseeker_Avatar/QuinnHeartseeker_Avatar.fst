name = QuinnHeartseeker_Avatar
type = body+head
scale = 1
filename = QuinnHeartseeker_Avatar/QuinnHeartseeker_Avatar.fbx
texdir = QuinnHeartseeker_Avatar/textures
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Spine = 26
jointIndex = LeftHandPinky4 = 60
jointIndex = RightHandPinky2 = 34
jointIndex = LeftHandThumb3 = 75
jointIndex = LeftHandThumb1 = 73
jointIndex = RightHandThumb3 = 51
jointIndex = LeftHandMiddle2 = 66
jointIndex = LeftHandPinky2 = 58
jointIndex = polySurface42 = 13
jointIndex = RightHandThumb1 = 49
jointIndex = LeftHandRing3 = 63
jointIndex = polySurface43 = 11
jointIndex = RightHandPinky3 = 35
jointIndex = RightForeArm = 31
jointIndex = RightLeg = 17
jointIndex = RightToe_End = 20
jointIndex = RightHandPinky4 = 36
jointIndex = LeftHand = 56
jointIndex = RightHandIndex2 = 46
jointIndex = RightHandPinky1 = 33
jointIndex = transform2 = 10
jointIndex = LeftHandMiddle4 = 68
jointIndex = RightHandThumb4 = 52
jointIndex = LeftArm = 54
jointIndex = LeftToeBase = 24
jointIndex = LeftHandIndex2 = 70
jointIndex = RightHandThumb2 = 50
jointIndex = LeftHandIndex1 = 69
jointIndex = LeftHandPinky1 = 57
jointIndex = LeftHandRing1 = 61
jointIndex = RightFoot = 18
jointIndex = RightHandMiddle3 = 43
jointIndex = LeftHandRing2 = 62
jointIndex = RightUpLeg = 16
jointIndex = RightArm = 30
jointIndex = polySurface46 = 5
jointIndex = RightHandRing3 = 39
jointIndex = LeftLeg = 22
jointIndex = Neck = 77
jointIndex = default1 = 1
jointIndex = RightToeBase = 19
jointIndex = LeftHandThumb4 = 76
jointIndex = RightHandRing2 = 38
jointIndex = polySurface47 = 3
jointIndex = transform3 = 12
jointIndex = RightHand = 32
jointIndex = RightHandMiddle2 = 42
jointIndex = RightHandMiddle1 = 41
jointIndex = LeftUpLeg = 21
jointIndex = LeftHandThumb2 = 74
jointIndex = transform4 = 14
jointIndex = LeftForeArm = 55
jointIndex = RightHandMiddle4 = 44
jointIndex = transform6 = 6
jointIndex = Spine1 = 27
jointIndex = LeftHandIndex4 = 72
jointIndex = LeftShoulder = 53
jointIndex = LeftHandPinky3 = 59
jointIndex = RightHandIndex1 = 45
jointIndex = polySurface44 = 9
jointIndex = LeftHandMiddle1 = 65
jointIndex = RightHandIndex4 = 48
jointIndex = RightHandRing4 = 40
jointIndex = Head = 78
jointIndex = transform5 = 8
jointIndex = transform1 = 2
jointIndex = LeftToe_End = 25
jointIndex = polySurface48 = 80
jointIndex = RightShoulder = 29
jointIndex = LeftFoot = 23
jointIndex = LeftHandIndex3 = 71
jointIndex = transform7 = 4
jointIndex = Spine2 = 28
jointIndex = RightHandIndex3 = 47
jointIndex = RightHandRing1 = 37
jointIndex = LeftHandRing4 = 64
jointIndex = HeadTop_End = 79
jointIndex = polySurface45 = 7
jointIndex = LeftHandMiddle3 = 67
jointIndex = Hips = 15
