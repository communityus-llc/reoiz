name = ZiggsBattleBoss_Avatar
type = body+head
scale = 1
filename = ZiggsBattleBoss_Avatar/ZiggsBattleBoss_Avatar.fbx
texdir = ZiggsBattleBoss_Avatar/textures
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointHead = Head
joint = jointNeck = Neck
joint = jointRoot = Hips
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftShoulder = 30
jointIndex = LeftLeg = 7
jointIndex = LeftToe_End = 10
jointIndex = LeftFoot = 8
jointIndex = LeftHandRing2 = 35
jointIndex = LeftUpLeg = 6
jointIndex = RightUpLeg = 1
jointIndex = RightLeg = 2
jointIndex = LeftHandRing3 = 36
jointIndex = LeftHandIndex3 = 40
jointIndex = RightForeArm = 16
jointIndex = LeftHandThumb1 = 42
jointIndex = LeftHandRing1 = 34
jointIndex = Neck = 46
jointIndex = LeftHandThumb3 = 44
jointIndex = RightHandThumb3 = 28
jointIndex = LeftHandIndex1 = 38
jointIndex = RightHandRing1 = 18
jointIndex = Spine2 = 13
jointIndex = RightHandRing4 = 21
jointIndex = RightHand = 17
jointIndex = LeftHandThumb2 = 43
jointIndex = LeftHandRing4 = 37
jointIndex = LeftArm = 31
jointIndex = Hips = 0
jointIndex = RightToeBase = 4
jointIndex = RightHandThumb2 = 27
jointIndex = RightFoot = 3
jointIndex = RightHandThumb1 = 26
jointIndex = LeftHandThumb4 = 45
jointIndex = RightToe_End = 5
jointIndex = Head = 47
jointIndex = Spine = 11
jointIndex = RightHandIndex4 = 25
jointIndex = HeadTop_End = 48
jointIndex = RightHandRing2 = 19
jointIndex = LeftHandIndex4 = 41
jointIndex = LeftHand = 33
jointIndex = Spine1 = 12
jointIndex = RightHandThumb4 = 29
jointIndex = LeftForeArm = 32
jointIndex = RightArm = 15
jointIndex = LeftToeBase = 9
jointIndex = RightShoulder = 14
jointIndex = RightHandIndex3 = 24
jointIndex = LeftHandIndex2 = 39
jointIndex = default1 = 49
jointIndex = RightHandRing3 = 20
jointIndex = RightHandIndex2 = 23
jointIndex = RightHandIndex1 = 22
