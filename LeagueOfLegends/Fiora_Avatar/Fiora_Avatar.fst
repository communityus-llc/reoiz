name = Fiora_Avatar
type = body+head
scale = 1
filename = Fiora_Avatar/Fiora_Avatar.fbx
texdir = Fiora_Avatar/textures
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointRoot = Hips
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = polySurface3 = 6
jointIndex = RightToe_End = 12
jointIndex = LeftHandMiddle3 = 59
jointIndex = RightHandIndex1 = 37
jointIndex = RightHandIndex2 = 38
jointIndex = RightHandRing4 = 32
jointIndex = RightShoulder = 21
jointIndex = transform1 = 1
jointIndex = Model = 0
jointIndex = RightHandPinky1 = 25
jointIndex = RightHandPinky2 = 26
jointIndex = LeftHandMiddle4 = 60
jointIndex = LeftHandIndex1 = 61
jointIndex = LeftHandIndex2 = 62
jointIndex = LeftFoot = 15
jointIndex = LeftForeArm = 47
jointIndex = LeftHandRing3 = 55
jointIndex = LeftHandThumb3 = 67
jointIndex = LeftHandThumb1 = 65
jointIndex = RightHandThumb4 = 44
jointIndex = polySurface1 = 2
jointIndex = RightHandMiddle4 = 36
jointIndex = Neck = 69
jointIndex = polySurface5 = 72
jointIndex = RightToeBase = 11
jointIndex = LeftHandPinky2 = 50
jointIndex = LeftHandRing2 = 54
jointIndex = Head = 70
jointIndex = polySurface4 = 5
jointIndex = Spine1 = 19
jointIndex = RightHandIndex4 = 40
jointIndex = RightHandThumb2 = 42
jointIndex = LeftHandMiddle2 = 58
jointIndex = RightHandPinky3 = 27
jointIndex = LeftLeg = 14
jointIndex = RightUpLeg = 8
jointIndex = Spine = 18
jointIndex = RightForeArm = 23
jointIndex = RightHandMiddle1 = 33
jointIndex = LeftUpLeg = 13
jointIndex = LeftHandIndex3 = 63
jointIndex = LeftHandPinky4 = 52
jointIndex = LeftHandIndex4 = 64
jointIndex = RightFoot = 10
jointIndex = RightHandRing1 = 29
jointIndex = LeftHandThumb4 = 68
jointIndex = HeadTop_End = 71
jointIndex = LeftHandPinky1 = 49
jointIndex = RightArm = 22
jointIndex = LeftHandMiddle1 = 57
jointIndex = LeftToe_End = 17
jointIndex = RightHandMiddle2 = 34
jointIndex = RightHandRing3 = 31
jointIndex = RightHand = 24
jointIndex = LeftHandRing1 = 53
jointIndex = RightHandThumb3 = 43
jointIndex = LeftArm = 46
jointIndex = polySurface2 = 3
jointIndex = transform2 = 4
jointIndex = LeftHand = 48
jointIndex = RightHandRing2 = 30
jointIndex = LeftHandThumb2 = 66
jointIndex = LeftShoulder = 45
jointIndex = RightLeg = 9
jointIndex = LeftHandRing4 = 56
jointIndex = RightHandIndex3 = 39
jointIndex = RightHandPinky4 = 28
jointIndex = Spine2 = 20
jointIndex = RightHandMiddle3 = 35
jointIndex = LeftHandPinky3 = 51
jointIndex = Hips = 7
jointIndex = LeftToeBase = 16
jointIndex = RightHandThumb1 = 41
