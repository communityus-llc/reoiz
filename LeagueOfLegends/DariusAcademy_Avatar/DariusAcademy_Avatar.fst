name = DariusAcademy_Avatar
type = body+head
scale = 1
filename = DariusAcademy_Avatar/DariusAcademy_Avatar.fbx
texdir = DariusAcademy_Avatar/textures
joint = jointRightHand = RightHand
joint = jointRoot = Hips
joint = jointLeftHand = LeftHand
joint = jointLean = Spine
joint = jointNeck = Neck
joint = jointHead = Head
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandThumb3 = 36
jointIndex = Spine1 = 12
jointIndex = RightHandMiddle2 = 27
jointIndex = RightHandThumb2 = 35
jointIndex = Hips = 0
jointIndex = RightHandRing4 = 25
jointIndex = LeftHandPinky4 = 45
jointIndex = Head = 63
jointIndex = LeftHandThumb4 = 61
jointIndex = RightHandPinky1 = 18
jointIndex = LeftHandPinky2 = 43
jointIndex = LeftForeArm = 40
jointIndex = RightHandIndex3 = 32
jointIndex = LeftHandThumb3 = 60
jointIndex = RightLeg = 2
jointIndex = RightHandThumb4 = 37
jointIndex = RightHandPinky2 = 19
jointIndex = LeftHandIndex2 = 55
jointIndex = RightShoulder = 14
jointIndex = RightToe_End = 5
jointIndex = RightHandThumb1 = 34
jointIndex = RightHandPinky4 = 21
jointIndex = Spine = 11
jointIndex = RightHand = 17
jointIndex = RightHandRing3 = 24
jointIndex = LeftHandIndex1 = 54
jointIndex = RightHandMiddle3 = 28
jointIndex = RightHandRing2 = 23
jointIndex = LeftHandRing4 = 49
jointIndex = LeftHandThumb2 = 59
jointIndex = LeftHandThumb1 = 58
jointIndex = RightFoot = 3
jointIndex = Spine2 = 13
jointIndex = RightHandPinky3 = 20
jointIndex = LeftHandMiddle4 = 53
jointIndex = RightHandMiddle1 = 26
jointIndex = RightHandIndex2 = 31
jointIndex = LeftToe_End = 10
jointIndex = RightToeBase = 4
jointIndex = LeftHandMiddle3 = 52
jointIndex = LeftHandRing2 = 47
jointIndex = LeftLeg = 7
jointIndex = LeftArm = 39
jointIndex = RightHandMiddle4 = 29
jointIndex = LeftFoot = 8
jointIndex = LeftUpLeg = 6
jointIndex = LeftHandRing1 = 46
jointIndex = LeftHandIndex3 = 56
jointIndex = HeadTop_End = 64
jointIndex = RightForeArm = 16
jointIndex = RightHandIndex4 = 33
jointIndex = LeftShoulder = 38
jointIndex = RightUpLeg = 1
jointIndex = LeftToeBase = 9
jointIndex = RightHandRing1 = 22
jointIndex = Neck = 62
jointIndex = LeftHandMiddle2 = 51
jointIndex = LeftHandPinky3 = 44
jointIndex = LeftHandRing3 = 48
jointIndex = LeftHandMiddle1 = 50
jointIndex = RightHandIndex1 = 30
jointIndex = RightArm = 15
jointIndex = LeftHandPinky1 = 42
jointIndex = default1 = 65
jointIndex = LeftHandIndex4 = 57
jointIndex = LeftHand = 41
