name = VeigarFinalBoss_Avatar
type = body+head
scale = 1
filename = VeigarFinalBoss_Avatar/VeigarFinalBoss_Avatar.fbx
texdir = VeigarFinalBoss_Avatar/textures
joint = jointRightHand = RightHand
joint = jointHead = Head
joint = jointRoot = Hips
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftToe_End = 13
jointIndex = LeftArm = 3
jointIndex = LeftToeBase = 12
jointIndex = default1 = 19
jointIndex = LeftUpLeg = 9
jointIndex = Head = 1
jointIndex = RightLeg = 15
jointIndex = RightToe_End = 18
jointIndex = LeftFoot = 11
jointIndex = Hips = 0
jointIndex = RightToeBase = 17
jointIndex = LeftHand = 5
jointIndex = LeftLeg = 10
jointIndex = LeftForeArm = 4
jointIndex = RightFoot = 16
jointIndex = RightUpLeg = 14
jointIndex = HeadTop_End = 2
jointIndex = RightForeArm = 7
jointIndex = RightArm = 6
jointIndex = RightHand = 8
