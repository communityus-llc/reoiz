name = LeeSinPoolParty_Avatar1
type = body+head
scale = 1.45
filename = LeeSinPoolParty_Avatar1/LeeSinPoolParty_Avatar1.fbx
texdir = LeeSinPoolParty_Avatar1/textures
joint = jointNeck = Neck
joint = jointRoot = Hips
joint = jointRightHand = RightHand
joint = jointHead = Head
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Spine1 = 12
jointIndex = LeftShoulder = 38
jointIndex = LeftHandPinky3 = 44
jointIndex = RightHandIndex1 = 30
jointIndex = LeftHandThumb4 = 61
jointIndex = LeftHandThumb1 = 58
jointIndex = RightHandMiddle2 = 27
jointIndex = Spine = 11
jointIndex = LeftUpLeg = 6
jointIndex = RightArm = 15
jointIndex = RightHandPinky3 = 20
jointIndex = LeftArm = 39
jointIndex = RightToeBase = 4
jointIndex = RightHandPinky2 = 19
jointIndex = LeftHandMiddle2 = 51
jointIndex = LeftHandIndex1 = 54
jointIndex = HeadTop_End = 64
jointIndex = RightShoulder = 14
jointIndex = LeftHandPinky4 = 45
jointIndex = RightHandThumb3 = 36
jointIndex = RightFoot = 3
jointIndex = LeftFoot = 8
jointIndex = RightHandThumb4 = 37
jointIndex = RightHandIndex2 = 31
jointIndex = RightHandMiddle4 = 29
jointIndex = RightHandThumb2 = 35
jointIndex = RightLeg = 2
jointIndex = LeftHandThumb2 = 59
jointIndex = RightHandThumb1 = 34
jointIndex = LeftHandRing2 = 47
jointIndex = Head = 63
jointIndex = LeftLeg = 7
jointIndex = LeftForeArm = 40
jointIndex = LeftToeBase = 9
jointIndex = RightForeArm = 16
jointIndex = LeftHandIndex3 = 56
jointIndex = LeftHandIndex2 = 55
jointIndex = RightHandIndex4 = 33
jointIndex = LeftHandThumb3 = 60
jointIndex = RightHand = 17
jointIndex = LeftHandMiddle3 = 52
jointIndex = Neck = 62
jointIndex = RightHandRing1 = 22
jointIndex = LeftHandRing3 = 48
jointIndex = LeftHandRing4 = 49
jointIndex = LeftHandIndex4 = 57
jointIndex = default1 = 65
jointIndex = RightHandPinky4 = 21
jointIndex = RightHandMiddle1 = 26
jointIndex = Spine2 = 13
jointIndex = LeftHandPinky2 = 43
jointIndex = RightHandRing2 = 23
jointIndex = RightHandRing4 = 25
jointIndex = LeftHand = 41
jointIndex = RightHandRing3 = 24
jointIndex = LeftHandMiddle4 = 53
jointIndex = RightHandMiddle3 = 28
jointIndex = LeftHandPinky1 = 42
jointIndex = LeftHandMiddle1 = 50
jointIndex = LeftToe_End = 10
jointIndex = LeftHandRing1 = 46
jointIndex = Hips = 0
jointIndex = RightToe_End = 5
jointIndex = RightHandIndex3 = 32
jointIndex = RightUpLeg = 1
jointIndex = RightHandPinky1 = 18
