name = PoppyStarGuardian_Avatar
type = body+head
scale = .7
filename = PoppyStarGuardian_Avatar/PoppyStarGuardian_Avatar.fbx
texdir = PoppyStarGuardian_Avatar/textures
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Spine = 14
jointIndex = transform13 = 60
jointIndex = LeftHandThumb3 = 47
jointIndex = transform12 = 2
jointIndex = LeftHandThumb1 = 45
jointIndex = RightHandThumb3 = 31
jointIndex = polySurface31 = 75
jointIndex = polySurface42 = 53
jointIndex = polySurface36 = 65
jointIndex = RightHandThumb1 = 29
jointIndex = LeftHandRing3 = 39
jointIndex = polySurface43 = 56
jointIndex = polySurface37 = 63
jointIndex = RightForeArm = 19
jointIndex = RightLeg = 5
jointIndex = RightToe_End = 8
jointIndex = transform10 = 62
jointIndex = polySurface30 = 77
jointIndex = LeftHand = 36
jointIndex = RightHandIndex2 = 26
jointIndex = transform2 = 74
jointIndex = RightHandThumb4 = 32
jointIndex = LeftArm = 34
jointIndex = LeftHandIndex2 = 42
jointIndex = LeftToeBase = 12
jointIndex = RightHandThumb2 = 30
jointIndex = LeftHandIndex1 = 41
jointIndex = LeftHandRing1 = 37
jointIndex = RightFoot = 6
jointIndex = transform8 = 68
jointIndex = LeftHandRing2 = 38
jointIndex = RightUpLeg = 4
jointIndex = RightArm = 18
jointIndex = RightHandRing3 = 23
jointIndex = LeftLeg = 10
jointIndex = polySurface32 = 73
jointIndex = Neck = 49
jointIndex = polySurface35 = 67
jointIndex = polySurface33 = 71
jointIndex = RightToeBase = 7
jointIndex = LeftHandThumb4 = 48
jointIndex = polySurface38 = 61
jointIndex = RightHandRing2 = 22
jointIndex = RightHand = 20
jointIndex = transform3 = 78
jointIndex = LeftUpLeg = 9
jointIndex = LeftHandThumb2 = 46
jointIndex = transform11 = 57
jointIndex = transform4 = 76
jointIndex = LeftForeArm = 35
jointIndex = polySurface34 = 69
jointIndex = transform6 = 70
jointIndex = Spine1 = 15
jointIndex = LeftHandIndex4 = 44
jointIndex = LeftShoulder = 33
jointIndex = polySurface40 = 59
jointIndex = RightHandIndex1 = 25
jointIndex = RightHandIndex4 = 28
jointIndex = transform9 = 64
jointIndex = RightHandRing4 = 24
jointIndex = Head = 50
jointIndex = polySurface39 = 55
jointIndex = transform5 = 72
jointIndex = transform1 = 54
jointIndex = LeftToe_End = 13
jointIndex = RightShoulder = 17
jointIndex = LeftFoot = 11
jointIndex = LeftHandIndex3 = 43
jointIndex = Model001_5_Mesh_0_1_16_16_014 = 52
jointIndex = transform7 = 66
jointIndex = Spine2 = 16
jointIndex = RightHandIndex3 = 27
jointIndex = RightHandRing1 = 21
jointIndex = LeftHandRing4 = 40
jointIndex = HeadTop_End = 51
jointIndex = polySurface41 = 58
jointIndex = Hips = 3
