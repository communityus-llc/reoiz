name = SorakaStarGuardian_Avatar1
type = body+head
scale = 1
filename = SorakaStarGuardian_Avatar1/SorakaStarGuardian_Avatar1.fbx
texdir = SorakaStarGuardian_Avatar1/textures
joint = jointNeck = Neck
joint = jointRoot = Hips
joint = jointRightHand = RightHand
joint = jointHead = Head
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftHandIndex4 = 57
jointIndex = LeftHandMiddle1 = 50
jointIndex = RightHandRing3 = 24
jointIndex = RightHandPinky3 = 20
jointIndex = Spine = 11
jointIndex = LeftShoulder = 38
jointIndex = LeftHandPinky4 = 45
jointIndex = RightForeArm = 16
jointIndex = polySurface55 = 67
jointIndex = RightFoot = 3
jointIndex = RightHandMiddle1 = 26
jointIndex = Spine1 = 12
jointIndex = LeftHandIndex3 = 56
jointIndex = transform1 = 70
jointIndex = RightHandThumb4 = 37
jointIndex = LeftHandMiddle3 = 52
jointIndex = LeftHandPinky1 = 42
jointIndex = RightLeg = 2
jointIndex = LeftForeArm = 40
jointIndex = RightHandMiddle2 = 27
jointIndex = LeftHandRing3 = 48
jointIndex = RightHandPinky2 = 19
jointIndex = LeftHandThumb1 = 58
jointIndex = RightHand = 17
jointIndex = RightHandMiddle3 = 28
jointIndex = transform2 = 77
jointIndex = polySurface51 = 72
jointIndex = LeftHandIndex1 = 54
jointIndex = Hips = 0
jointIndex = LeftFoot = 8
jointIndex = LeftHandThumb4 = 61
jointIndex = LeftHandRing2 = 47
jointIndex = polySurface49 = 76
jointIndex = LeftHandIndex2 = 55
jointIndex = RightArm = 15
jointIndex = LeftArm = 39
jointIndex = RightHandMiddle4 = 29
jointIndex = LeftHandPinky3 = 44
jointIndex = LeftToeBase = 9
jointIndex = LeftHandPinky2 = 43
jointIndex = polySurface50 = 74
jointIndex = LeftHandThumb3 = 60
jointIndex = LeftHandRing1 = 46
jointIndex = RightHandRing1 = 22
jointIndex = RightHandIndex2 = 31
jointIndex = LeftHandThumb2 = 59
jointIndex = RightHandRing4 = 25
jointIndex = polySurface53 = 69
jointIndex = RightHandPinky1 = 18
jointIndex = RightHandThumb3 = 36
jointIndex = LeftHandMiddle4 = 53
jointIndex = RightHandIndex1 = 30
jointIndex = LeftToe_End = 10
jointIndex = polySurface52 = 71
jointIndex = default1 = 66
jointIndex = RightUpLeg = 1
jointIndex = LeftUpLeg = 6
jointIndex = HeadTop_End = 64
jointIndex = LeftHandRing4 = 49
jointIndex = LeftHandMiddle2 = 51
jointIndex = RightHandRing2 = 23
jointIndex = LeftHand = 41
jointIndex = transform3 = 75
jointIndex = Head = 63
jointIndex = RightHandPinky4 = 21
jointIndex = Neck = 62
jointIndex = RightShoulder = 14
jointIndex = RightHandIndex4 = 33
jointIndex = RightToeBase = 4
jointIndex = RightHandThumb2 = 35
jointIndex = Spine2 = 13
jointIndex = transform4 = 73
jointIndex = RightToe_End = 5
jointIndex = RightHandThumb1 = 34
jointIndex = LeftLeg = 7
jointIndex = polySurface54 = 68
jointIndex = RightHandIndex3 = 32
