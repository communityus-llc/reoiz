name = EliseSuperGalaxy_Avatar
type = body+head
scale = 1
filename = EliseSuperGalaxy_Avatar/EliseSuperGalaxy_Avatar.fbx
texdir = EliseSuperGalaxy_Avatar/textures
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointRoot = Hips
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Neck = 56
jointIndex = RightForeArm = 10
jointIndex = LeftHandMiddle2 = 45
jointIndex = LeftHandThumb1 = 52
jointIndex = LeftHandPinky1 = 36
jointIndex = RightHandIndex3 = 26
jointIndex = RightHandThumb2 = 29
jointIndex = RightHandPinky4 = 15
jointIndex = LeftHandMiddle3 = 46
jointIndex = Spine2 = 7
jointIndex = RightHandIndex2 = 25
jointIndex = RightHandThumb1 = 28
jointIndex = RightHandThumb4 = 31
jointIndex = RightArm = 9
jointIndex = LeftArm = 33
jointIndex = RightHandPinky3 = 14
jointIndex = LeftHandThumb2 = 53
jointIndex = LeftLeg = 4
jointIndex = Spine = 5
jointIndex = RightHandRing1 = 16
jointIndex = LeftHandThumb3 = 54
jointIndex = LeftHandMiddle1 = 44
jointIndex = Head = 57
jointIndex = RightHandThumb3 = 30
jointIndex = RightHandRing2 = 17
jointIndex = default1 = 59
jointIndex = LeftHandIndex3 = 50
jointIndex = RightHandRing3 = 18
jointIndex = LeftHandThumb4 = 55
jointIndex = LeftForeArm = 34
jointIndex = LeftHandIndex2 = 49
jointIndex = RightHandIndex4 = 27
jointIndex = Hips = 0
jointIndex = RightHandMiddle4 = 23
jointIndex = LeftHandMiddle4 = 47
jointIndex = LeftHandIndex1 = 48
jointIndex = LeftHandRing2 = 41
jointIndex = RightHand = 11
jointIndex = RightHandRing4 = 19
jointIndex = LeftHandPinky2 = 37
jointIndex = RightLeg = 2
jointIndex = LeftHandPinky3 = 38
jointIndex = RightHandPinky2 = 13
jointIndex = LeftHandPinky4 = 39
jointIndex = RightShoulder = 8
jointIndex = LeftShoulder = 32
jointIndex = LeftHandRing3 = 42
jointIndex = LeftHand = 35
jointIndex = RightUpLeg = 1
jointIndex = LeftUpLeg = 3
jointIndex = HeadTop_End = 58
jointIndex = LeftHandIndex4 = 51
jointIndex = Spine1 = 6
jointIndex = RightHandMiddle2 = 21
jointIndex = RightHandMiddle1 = 20
jointIndex = RightHandMiddle3 = 22
jointIndex = LeftHandRing1 = 40
jointIndex = RightHandPinky1 = 12
jointIndex = RightHandIndex1 = 24
jointIndex = LeftHandRing4 = 43
