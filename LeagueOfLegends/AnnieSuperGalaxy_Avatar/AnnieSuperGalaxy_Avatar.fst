name = AnnieSuperGalaxy_Avatar
type = body+head
scale = .68
filename = AnnieSuperGalaxy_Avatar/AnnieSuperGalaxy_Avatar.fbx
texdir = AnnieSuperGalaxy_Avatar/textures
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointRoot = Hips
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Neck = 22
jointIndex = RightForeArm = 16
jointIndex = LeftToeBase = 9
jointIndex = Object017 = 30
jointIndex = Object019 = 26
jointIndex = Object003 = 56
jointIndex = Object007 = 48
jointIndex = Object018 = 28
jointIndex = Spine2 = 13
jointIndex = Object013 = 36
jointIndex = RightArm = 15
jointIndex = LeftArm = 19
jointIndex = transform7 = 29
jointIndex = Object002 = 58
jointIndex = Object005 = 52
jointIndex = LeftLeg = 7
jointIndex = RightFoot = 3
jointIndex = Spine = 11
jointIndex = transform10 = 49
jointIndex = transform5 = 33
jointIndex = transform18 = 43
jointIndex = Head = 23
jointIndex = transform6 = 31
jointIndex = Object020 = 25
jointIndex = transform15 = 39
jointIndex = RightToeBase = 4
jointIndex = Object010 = 42
jointIndex = Object009 = 44
jointIndex = transform2 = 35
jointIndex = transform8 = 27
jointIndex = Object001 = 60
jointIndex = Object016 = 32
jointIndex = LeftForeArm = 20
jointIndex = RightToe_End = 5
jointIndex = transform12 = 61
jointIndex = Hips = 0
jointIndex = LeftToe_End = 10
jointIndex = transform3 = 55
jointIndex = transform17 = 45
jointIndex = RightHand = 17
jointIndex = transform4 = 57
jointIndex = RightLeg = 2
jointIndex = transform16 = 37
jointIndex = transform9 = 47
jointIndex = transform14 = 41
jointIndex = RightShoulder = 14
jointIndex = LeftFoot = 8
jointIndex = LeftShoulder = 18
jointIndex = transform13 = 59
jointIndex = LeftHand = 21
jointIndex = RightUpLeg = 1
jointIndex = Object011 = 40
jointIndex = HeadTop_End = 24
jointIndex = LeftUpLeg = 6
jointIndex = Object006 = 50
jointIndex = transform1 = 53
jointIndex = Object004 = 54
jointIndex = Object015 = 34
jointIndex = Spine1 = 12
jointIndex = Object008 = 46
jointIndex = Object012 = 38
jointIndex = transform11 = 51
