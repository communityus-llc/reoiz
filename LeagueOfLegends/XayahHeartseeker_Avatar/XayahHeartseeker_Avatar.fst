name = XayahHeartseeker_Avatar
type = body+head
scale = 1
filename = XayahHeartseeker_Avatar/XayahHeartseeker_Avatar.fbx
texdir = XayahHeartseeker_Avatar/textures
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointHead = Head
joint = jointNeck = Neck
joint = jointRoot = Hips
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftShoulder = 38
jointIndex = LeftLeg = 7
jointIndex = LeftToe_End = 10
jointIndex = LeftFoot = 8
jointIndex = LeftHandRing2 = 47
jointIndex = LeftUpLeg = 6
jointIndex = RightUpLeg = 1
jointIndex = RightLeg = 2
jointIndex = LeftHandRing3 = 48
jointIndex = LeftHandIndex3 = 56
jointIndex = RightForeArm = 16
jointIndex = LeftHandThumb1 = 58
jointIndex = RightHandMiddle3 = 28
jointIndex = LeftHandRing1 = 46
jointIndex = Neck = 62
jointIndex = RightHandPinky3 = 20
jointIndex = LeftHandThumb3 = 60
jointIndex = RightHandThumb3 = 36
jointIndex = LeftHandIndex1 = 54
jointIndex = LeftHandMiddle2 = 51
jointIndex = RightHandPinky2 = 19
jointIndex = LeftHandPinky3 = 44
jointIndex = LeftHandPinky1 = 42
jointIndex = LeftHandPinky4 = 45
jointIndex = RightHandPinky1 = 18
jointIndex = RightHandRing1 = 22
jointIndex = Spine2 = 13
jointIndex = RightHandRing4 = 25
jointIndex = RightHand = 17
jointIndex = LeftHandThumb2 = 59
jointIndex = LeftHandRing4 = 49
jointIndex = LeftHandMiddle1 = 50
jointIndex = RightHandMiddle2 = 27
jointIndex = LeftHandMiddle3 = 52
jointIndex = LeftHandMiddle4 = 53
jointIndex = Hips = 0
jointIndex = LeftArm = 39
jointIndex = RightToeBase = 4
jointIndex = RightHandThumb2 = 35
jointIndex = RightFoot = 3
jointIndex = LeftHandPinky2 = 43
jointIndex = RightHandThumb1 = 34
jointIndex = LeftHandThumb4 = 61
jointIndex = RightHandMiddle1 = 26
jointIndex = RightToe_End = 5
jointIndex = Head = 63
jointIndex = Spine = 11
jointIndex = RightHandIndex4 = 33
jointIndex = HeadTop_End = 64
jointIndex = RightHandRing2 = 23
jointIndex = LeftHandIndex4 = 57
jointIndex = Spine1 = 12
jointIndex = LeftHand = 41
jointIndex = RightHandThumb4 = 37
jointIndex = RightArm = 15
jointIndex = LeftForeArm = 40
jointIndex = LeftToeBase = 9
jointIndex = RightHandMiddle4 = 29
jointIndex = RightShoulder = 14
jointIndex = RightHandIndex3 = 32
jointIndex = LeftHandIndex2 = 55
jointIndex = RightHandRing3 = 24
jointIndex = default007 = 65
jointIndex = RightHandIndex2 = 31
jointIndex = RightHandPinky4 = 21
jointIndex = RightHandIndex1 = 30
