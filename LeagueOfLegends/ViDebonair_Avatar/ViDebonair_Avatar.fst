name = ViDebonair_Avatar
type = body+head
scale = 1
filename = ViDebonair_Avatar/ViDebonair_Avatar.fbx
texdir = ViDebonair_Avatar/textures
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointHead = Head
joint = jointNeck = Neck
joint = jointRoot = Hips
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightArm = 15
jointIndex = RightHandThumb3 = 36
jointIndex = LeftHandPinky2 = 43
jointIndex = LeftHandThumb3 = 60
jointIndex = LeftHandThumb4 = 61
jointIndex = RightHandPinky3 = 20
jointIndex = RightHand = 17
jointIndex = RightHandIndex1 = 30
jointIndex = LeftHandIndex3 = 56
jointIndex = RightHandMiddle2 = 27
jointIndex = RightToeBase = 4
jointIndex = RightHandRing2 = 23
jointIndex = LeftToeBase = 9
jointIndex = LeftHandRing2 = 47
jointIndex = RightUpLeg = 1
jointIndex = LeftToe_End = 10
jointIndex = polySurface29 = 66
jointIndex = RightHandPinky2 = 19
jointIndex = RightHandThumb2 = 35
jointIndex = polySurface60 = 65
jointIndex = RightHandRing1 = 22
jointIndex = LeftLeg = 7
jointIndex = LeftForeArm = 40
jointIndex = Head = 63
jointIndex = RightLeg = 2
jointIndex = RightShoulder = 14
jointIndex = LeftHandPinky4 = 45
jointIndex = LeftHandIndex4 = 57
jointIndex = polySurface61 = 67
jointIndex = Spine2 = 13
jointIndex = Spine = 11
jointIndex = LeftHandRing4 = 49
jointIndex = RightHandThumb1 = 34
jointIndex = LeftHandMiddle3 = 52
jointIndex = LeftHandIndex2 = 55
jointIndex = RightHandPinky4 = 21
jointIndex = RightHandThumb4 = 37
jointIndex = LeftUpLeg = 6
jointIndex = RightToe_End = 5
jointIndex = LeftHandPinky3 = 44
jointIndex = LeftHand = 41
jointIndex = LeftHandThumb2 = 59
jointIndex = LeftShoulder = 38
jointIndex = LeftHandIndex1 = 54
jointIndex = RightForeArm = 16
jointIndex = LeftHandMiddle2 = 51
jointIndex = LeftHandThumb1 = 58
jointIndex = LeftArm = 39
jointIndex = Spine1 = 12
jointIndex = LeftHandMiddle4 = 53
jointIndex = RightHandRing4 = 25
jointIndex = RightHandMiddle1 = 26
jointIndex = Hips = 0
jointIndex = Neck = 62
jointIndex = LeftHandRing3 = 48
jointIndex = RightHandRing3 = 24
jointIndex = RightHandIndex2 = 31
jointIndex = RightHandIndex3 = 32
jointIndex = RightHandMiddle3 = 28
jointIndex = LeftFoot = 8
jointIndex = RightHandPinky1 = 18
jointIndex = RightFoot = 3
jointIndex = HeadTop_End = 64
jointIndex = LeftHandPinky1 = 42
jointIndex = RightHandMiddle4 = 29
jointIndex = RightHandIndex4 = 33
jointIndex = LeftHandRing1 = 46
jointIndex = LeftHandMiddle1 = 50
