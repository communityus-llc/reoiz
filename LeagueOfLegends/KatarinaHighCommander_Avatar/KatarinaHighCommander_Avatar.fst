name = KatarinaHighCommander_Avatar
type = body+head
scale = 1
filename = KatarinaHighCommander_Avatar/KatarinaHighCommander_Avatar.fbx
texdir = KatarinaHighCommander_Avatar/textures
joint = jointLeftHand = LeftHand
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointHead = Head
joint = jointLean = Spine
joint = jointRoot = Hips
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftHandThumb4 = 61
jointIndex = RightLeg = 2
jointIndex = LeftHandMiddle1 = 50
jointIndex = RightHandPinky4 = 21
jointIndex = LeftHandIndex4 = 57
jointIndex = RightHandIndex1 = 30
jointIndex = LeftHandThumb2 = 59
jointIndex = LeftLeg = 7
jointIndex = RightToeBase = 4
jointIndex = LeftHandRing3 = 48
jointIndex = Spine2 = 13
jointIndex = LeftForeArm = 40
jointIndex = RightHandPinky3 = 20
jointIndex = HeadTop_End = 64
jointIndex = RightHandIndex3 = 32
jointIndex = LeftToe_End = 10
jointIndex = RightHandThumb3 = 36
jointIndex = RightHandRing4 = 25
jointIndex = RightArm = 15
jointIndex = RightHandPinky1 = 18
jointIndex = Spine = 11
jointIndex = LeftHandThumb1 = 58
jointIndex = LeftHandPinky4 = 45
jointIndex = Neck = 62
jointIndex = LeftHandPinky3 = 44
jointIndex = LeftFoot = 8
jointIndex = RightHandMiddle1 = 26
jointIndex = LeftHandMiddle2 = 51
jointIndex = RightUpLeg = 1
jointIndex = polySurface9 = 66
jointIndex = Head = 63
jointIndex = LeftShoulder = 38
jointIndex = LeftHandPinky2 = 43
jointIndex = LeftHandRing4 = 49
jointIndex = RightHand = 17
jointIndex = polySurface10 = 67
jointIndex = RightHandIndex2 = 31
jointIndex = LeftHandIndex1 = 54
jointIndex = RightHandPinky2 = 19
jointIndex = RightHandRing3 = 24
jointIndex = LeftUpLeg = 6
jointIndex = LeftHandMiddle4 = 53
jointIndex = LeftHandIndex3 = 56
jointIndex = LeftHandPinky1 = 42
jointIndex = LeftHandRing1 = 46
jointIndex = RightForeArm = 16
jointIndex = Spine1 = 12
jointIndex = LeftHandThumb3 = 60
jointIndex = RightHandMiddle2 = 27
jointIndex = RightHandIndex4 = 33
jointIndex = LeftHandRing2 = 47
jointIndex = RightToe_End = 5
jointIndex = transform1 = 68
jointIndex = RightHandThumb1 = 34
jointIndex = LeftHandIndex2 = 55
jointIndex = LeftToeBase = 9
jointIndex = Hips = 0
jointIndex = RightFoot = 3
jointIndex = RightShoulder = 14
jointIndex = LeftHandMiddle3 = 52
jointIndex = RightHandThumb4 = 37
jointIndex = RightHandMiddle4 = 29
jointIndex = LeftArm = 39
jointIndex = RightHandRing1 = 22
jointIndex = RightHandThumb2 = 35
jointIndex = RightHandMiddle3 = 28
jointIndex = RightHandRing2 = 23
jointIndex = Model = 65
jointIndex = LeftHand = 41
