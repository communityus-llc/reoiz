name = JinxStarGuardian_Avatar1
type = body+head
scale = 0.8
filename = JinxStarGuardian_Avatar1/JinxStarGuardian_Avatar1.fbx
texdir = JinxStarGuardian_Avatar1/textures
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
joint = jointRoot = Hips
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointHead = Head
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = HeadTop_End = 65
jointIndex = RightHandRing3 = 25
jointIndex = RightHandThumb4 = 38
jointIndex = RightHandThumb3 = 37
jointIndex = LeftHandPinky2 = 44
jointIndex = LeftHandThumb1 = 59
jointIndex = LeftUpLeg = 7
jointIndex = RightShoulder = 15
jointIndex = RightHandRing1 = 23
jointIndex = RightFoot = 4
jointIndex = LeftHand = 42
jointIndex = RightArm = 16
jointIndex = LeftHandPinky4 = 46
jointIndex = RightHandIndex4 = 34
jointIndex = RightToeBase = 5
jointIndex = polySurface6 = 0
jointIndex = LeftHandIndex3 = 57
jointIndex = Spine = 12
jointIndex = LeftToe_End = 11
jointIndex = Head = 64
jointIndex = RightHand = 18
jointIndex = RightHandPinky1 = 19
jointIndex = LeftHandIndex4 = 58
jointIndex = polySurface8 = 66
jointIndex = Spine1 = 13
jointIndex = RightUpLeg = 2
jointIndex = RightHandRing2 = 24
jointIndex = LeftHandMiddle4 = 54
jointIndex = LeftForeArm = 41
jointIndex = LeftHandIndex1 = 55
jointIndex = RightHandThumb2 = 36
jointIndex = RightHandIndex2 = 32
jointIndex = RightHandPinky4 = 22
jointIndex = RightForeArm = 17
jointIndex = RightHandPinky3 = 21
jointIndex = LeftHandMiddle2 = 52
jointIndex = LeftHandThumb3 = 61
jointIndex = Neck = 63
jointIndex = RightHandMiddle2 = 28
jointIndex = LeftHandThumb2 = 60
jointIndex = RightLeg = 3
jointIndex = LeftLeg = 8
jointIndex = RightHandRing4 = 26
jointIndex = LeftHandRing3 = 49
jointIndex = RightToe_End = 6
jointIndex = Spine2 = 14
jointIndex = RightHandIndex1 = 31
jointIndex = LeftHandMiddle1 = 51
jointIndex = LeftHandRing1 = 47
jointIndex = LeftHandThumb4 = 62
jointIndex = LeftToeBase = 10
jointIndex = RightHandMiddle4 = 30
jointIndex = RightHandPinky2 = 20
jointIndex = LeftHandRing2 = 48
jointIndex = RightHandMiddle3 = 29
jointIndex = LeftShoulder = 39
jointIndex = LeftHandRing4 = 50
jointIndex = RightHandThumb1 = 35
jointIndex = RightHandMiddle1 = 27
jointIndex = LeftFoot = 9
jointIndex = Hips = 1
jointIndex = LeftHandPinky3 = 45
jointIndex = LeftArm = 40
jointIndex = RightHandIndex3 = 33
jointIndex = LeftHandPinky1 = 43
jointIndex = LeftHandMiddle3 = 53
jointIndex = LeftHandIndex2 = 56
