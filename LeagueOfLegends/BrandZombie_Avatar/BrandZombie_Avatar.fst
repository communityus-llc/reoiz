name = BrandZombie_Avatar
type = body+head
scale = 1
filename = BrandZombie_Avatar/BrandZombie_Avatar.fbx
texdir = BrandZombie_Avatar/textures
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointLeftHand = LeftHand
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLean = Spine
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandPinky1 = 18
jointIndex = LeftHandPinky3 = 44
jointIndex = RightFoot = 3
jointIndex = LeftHandThumb1 = 58
jointIndex = LeftHandIndex4 = 57
jointIndex = RightHandMiddle3 = 28
jointIndex = polySurface2 = 68
jointIndex = LeftHandPinky2 = 43
jointIndex = LeftForeArm = 40
jointIndex = LeftHandMiddle1 = 50
jointIndex = RightHandIndex2 = 31
jointIndex = RightHand = 17
jointIndex = Spine = 11
jointIndex = RightHandPinky3 = 20
jointIndex = LeftLeg = 7
jointIndex = LeftHandPinky4 = 45
jointIndex = LeftFoot = 8
jointIndex = LeftHandIndex2 = 55
jointIndex = RightHandMiddle1 = 26
jointIndex = LeftHand = 41
jointIndex = Neck = 62
jointIndex = transform3 = 69
jointIndex = transform1 = 67
jointIndex = RightHandPinky2 = 19
jointIndex = LeftHandIndex1 = 54
jointIndex = LeftArm = 39
jointIndex = LeftHandThumb2 = 59
jointIndex = RightHandMiddle2 = 27
jointIndex = RightHandMiddle4 = 29
jointIndex = RightHandIndex3 = 32
jointIndex = transform2 = 71
jointIndex = LeftHandMiddle4 = 53
jointIndex = LeftHandThumb4 = 61
jointIndex = RightUpLeg = 1
jointIndex = LeftHandIndex3 = 56
jointIndex = RightLeg = 2
jointIndex = LeftHandThumb3 = 60
jointIndex = RightHandRing2 = 23
jointIndex = LeftHandPinky1 = 42
jointIndex = LeftShoulder = 38
jointIndex = RightToeBase = 4
jointIndex = RightShoulder = 14
jointIndex = RightToe_End = 5
jointIndex = RightHandThumb4 = 37
jointIndex = RightHandThumb1 = 34
jointIndex = Head = 63
jointIndex = RightHandThumb2 = 35
jointIndex = LeftHandRing3 = 48
jointIndex = LeftHandMiddle2 = 51
jointIndex = transformBrand_zombie1 = 66
jointIndex = LeftHandMiddle3 = 52
jointIndex = RightHandRing1 = 22
jointIndex = LeftToe_End = 10
jointIndex = RightHandIndex1 = 30
jointIndex = LeftHandRing4 = 49
jointIndex = RightForeArm = 16
jointIndex = polySurface21 = 65
jointIndex = HeadTop_End = 64
jointIndex = LeftToeBase = 9
jointIndex = LeftHandRing2 = 47
jointIndex = RightArm = 15
jointIndex = RightHandThumb3 = 36
jointIndex = LeftHandRing1 = 46
jointIndex = LeftUpLeg = 6
jointIndex = Spine2 = 13
jointIndex = RightHandIndex4 = 33
jointIndex = polySurface1 = 70
jointIndex = Hips = 0
jointIndex = RightHandRing4 = 25
jointIndex = RightHandRing3 = 24
jointIndex = Spine1 = 12
jointIndex = RightHandPinky4 = 21
