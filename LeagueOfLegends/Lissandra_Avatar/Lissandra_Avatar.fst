name = Lissandra_Avatar
type = body+head
scale = 1
filename = Lissandra_Avatar/Lissandra_Avatar.fbx
texdir = Lissandra_Avatar/textures
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Neck = 20
jointIndex = RightHand = 7
jointIndex = LeftHandIndex4 = 19
jointIndex = LeftHandIndex1 = 16
jointIndex = RightForeArm = 6
jointIndex = RightHandIndex2 = 9
jointIndex = LeftHandIndex3 = 18
jointIndex = RightArm = 5
jointIndex = Hips = 0
jointIndex = LeftForeArm = 14
jointIndex = LeftArm = 13
jointIndex = RightShoulder = 4
jointIndex = Spine = 1
jointIndex = LeftHandIndex2 = 17
jointIndex = HeadTop_End = 22
jointIndex = Spine2 = 3
jointIndex = RightHandIndex4 = 11
jointIndex = LeftHand = 15
jointIndex = Head = 21
jointIndex = RightHandIndex3 = 10
jointIndex = LeftShoulder = 12
jointIndex = Model = 23
jointIndex = Spine1 = 2
jointIndex = RightHandIndex1 = 8
