name = TaricPoolParty_Avatar
type = body+head
scale = .85
filename = TaricPoolParty_Avatar/TaricPoolParty_Avatar.fbx
texdir = TaricPoolParty_Avatar/textures
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
joint = jointHead = Head
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandRing3 = 24
jointIndex = LeftToe_End = 10
jointIndex = RightHandMiddle3 = 28
jointIndex = LeftHandMiddle4 = 53
jointIndex = RightHandMiddle4 = 29
jointIndex = Head = 63
jointIndex = RightForeArm = 16
jointIndex = RightHandThumb1 = 34
jointIndex = RightHandIndex2 = 31
jointIndex = LeftHandIndex3 = 56
jointIndex = LeftShoulder = 38
jointIndex = LeftHandRing2 = 47
jointIndex = LeftHand = 41
jointIndex = RightHandPinky1 = 18
jointIndex = RightUpLeg = 1
jointIndex = LeftHandThumb4 = 61
jointIndex = LeftHandRing3 = 48
jointIndex = LeftHandPinky2 = 43
jointIndex = Spine = 11
jointIndex = LeftArm = 39
jointIndex = RightHandRing2 = 23
jointIndex = RightToe_End = 5
jointIndex = LeftHandMiddle3 = 52
jointIndex = LeftHandThumb2 = 59
jointIndex = RightShoulder = 14
jointIndex = LeftHandPinky3 = 44
jointIndex = LeftHandThumb3 = 60
jointIndex = Hips = 0
jointIndex = default1 = 65
jointIndex = RightHandThumb2 = 35
jointIndex = RightHandMiddle1 = 26
jointIndex = RightHandIndex4 = 33
jointIndex = LeftHandRing1 = 46
jointIndex = LeftHandIndex1 = 54
jointIndex = RightHandPinky4 = 21
jointIndex = LeftLeg = 7
jointIndex = RightHandThumb4 = 37
jointIndex = LeftHandIndex2 = 55
jointIndex = RightHandMiddle2 = 27
jointIndex = LeftHandThumb1 = 58
jointIndex = RightHandIndex3 = 32
jointIndex = LeftHandPinky4 = 45
jointIndex = RightArm = 15
jointIndex = RightLeg = 2
jointIndex = Spine1 = 12
jointIndex = LeftToeBase = 9
jointIndex = LeftHandIndex4 = 57
jointIndex = RightHandRing1 = 22
jointIndex = RightFoot = 3
jointIndex = RightToeBase = 4
jointIndex = RightHandThumb3 = 36
jointIndex = Neck = 62
jointIndex = RightHandRing4 = 25
jointIndex = LeftFoot = 8
jointIndex = LeftUpLeg = 6
jointIndex = LeftHandMiddle2 = 51
jointIndex = RightHand = 17
jointIndex = RightHandIndex1 = 30
jointIndex = RightHandPinky2 = 19
jointIndex = LeftForeArm = 40
jointIndex = Spine2 = 13
jointIndex = LeftHandRing4 = 49
jointIndex = RightHandPinky3 = 20
jointIndex = LeftHandPinky1 = 42
jointIndex = LeftHandMiddle1 = 50
jointIndex = HeadTop_End = 64
