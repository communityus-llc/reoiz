name = Sivir_Avatar
type = body+head
scale = 1
filename = Sivir_Avatar/Sivir_Avatar.fbx
texdir = Sivir_Avatar/textures
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointNeck = Neck
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandMiddle1 = 26
jointIndex = RightArm = 15
jointIndex = polySurface54 = 45
jointIndex = LeftUpLeg = 6
jointIndex = RightHandRing1 = 22
jointIndex = RightForeArm = 16
jointIndex = RightHandPinky4 = 21
jointIndex = RightHandThumb3 = 36
jointIndex = RightToeBase = 4
jointIndex = Spine = 11
jointIndex = RightHandIndex3 = 32
jointIndex = LeftShoulder = 38
jointIndex = Spine2 = 13
jointIndex = RightFoot = 3
jointIndex = LeftForeArm = 40
jointIndex = RightUpLeg = 1
jointIndex = RightHandThumb1 = 34
jointIndex = RightHandPinky2 = 19
jointIndex = RightHandMiddle3 = 28
jointIndex = RightHandRing4 = 25
jointIndex = RightHandMiddle4 = 29
jointIndex = RightLeg = 2
jointIndex = LeftFoot = 8
jointIndex = Hips = 0
jointIndex = Head = 42
jointIndex = polySurface53 = 46
jointIndex = RightHandPinky3 = 20
jointIndex = RightHandThumb2 = 35
jointIndex = Spine1 = 12
jointIndex = RightHandThumb4 = 37
jointIndex = HeadTop_End = 43
jointIndex = Model = 44
jointIndex = RightHandRing2 = 23
jointIndex = RightToe_End = 5
jointIndex = RightHand = 17
jointIndex = LeftToe_End = 10
jointIndex = RightHandMiddle2 = 27
jointIndex = Neck = 41
jointIndex = RightHandRing3 = 24
jointIndex = RightHandIndex2 = 31
jointIndex = LeftArm = 39
jointIndex = LeftLeg = 7
jointIndex = transform1 = 47
jointIndex = RightShoulder = 14
jointIndex = LeftToeBase = 9
jointIndex = RightHandPinky1 = 18
jointIndex = RightHandIndex4 = 33
jointIndex = RightHandIndex1 = 30
