name = Gnar_Avatar1
type = body+head
scale = 1
filename = Gnar_Avatar1/Gnar_Avatar1.fbx
texdir = Gnar_Avatar1/textures
joint = jointRoot = Hips
joint = jointLean = Spine
joint = jointHead = Head
joint = jointNeck = Neck
joint = jointRightHand = RightHand
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandIndex4 = 25
jointIndex = LeftHandRing1 = 34
jointIndex = Head = 47
jointIndex = LeftHandThumb1 = 42
jointIndex = RightHandRing1 = 18
jointIndex = LeftLeg = 7
jointIndex = RightUpLeg = 1
jointIndex = RightHandIndex2 = 23
jointIndex = RightLeg = 2
jointIndex = RightHandRing2 = 19
jointIndex = Spine2 = 13
jointIndex = RightForeArm = 16
jointIndex = HeadTop_End = 48
jointIndex = RightToeBase = 4
jointIndex = LeftHandIndex1 = 38
jointIndex = RightHandThumb1 = 26
jointIndex = polySurface1 = 50
jointIndex = RightHandThumb4 = 29
jointIndex = LeftHandRing3 = 36
jointIndex = RightToe_End = 5
jointIndex = RightHandRing4 = 21
jointIndex = RightShoulder = 14
jointIndex = RightHandRing3 = 20
jointIndex = LeftHandIndex3 = 40
jointIndex = LeftShoulder = 30
jointIndex = LeftArm = 31
jointIndex = RightHandIndex3 = 24
jointIndex = LeftHandThumb4 = 45
jointIndex = LeftFoot = 8
jointIndex = LeftHandRing2 = 35
jointIndex = LeftHandThumb2 = 43
jointIndex = RightHandThumb2 = 27
jointIndex = LeftHandThumb3 = 44
jointIndex = RightArm = 15
jointIndex = polySurface2 = 51
jointIndex = RightHandThumb3 = 28
jointIndex = RightHandIndex1 = 22
jointIndex = Spine1 = 12
jointIndex = LeftForeArm = 32
jointIndex = RightHand = 17
jointIndex = LeftUpLeg = 6
jointIndex = LeftHandIndex2 = 39
jointIndex = Neck = 46
jointIndex = Hips = 0
jointIndex = RightFoot = 3
jointIndex = transform1 = 52
jointIndex = LeftToeBase = 9
jointIndex = Spine = 11
jointIndex = LeftHand = 33
jointIndex = LeftHandRing4 = 37
jointIndex = LeftHandIndex4 = 41
jointIndex = LeftToe_End = 10
