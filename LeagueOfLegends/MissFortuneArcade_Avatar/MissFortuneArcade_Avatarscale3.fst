name = MissFortuneArcade_Avatarscale3
type = body+head
scale = 2.6
filename = MissFortuneArcade_Avatarscale3/MissFortuneArcade_Avatarscale3.fbx
texdir = MissFortuneArcade_Avatarscale3/textures
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Spine = 12
jointIndex = LeftHandPinky4 = 46
jointIndex = RightHandPinky2 = 20
jointIndex = LeftHandThumb3 = 61
jointIndex = polySurface7 = 69
jointIndex = polySurface3 = 76
jointIndex = LeftHandThumb1 = 59
jointIndex = RightHandThumb3 = 37
jointIndex = LeftHandMiddle2 = 52
jointIndex = LeftHandPinky2 = 44
jointIndex = RightHandThumb1 = 35
jointIndex = LeftHandRing3 = 49
jointIndex = polySurface8 = 67
jointIndex = RightHandPinky3 = 21
jointIndex = RightForeArm = 17
jointIndex = RightLeg = 3
jointIndex = RightToe_End = 6
jointIndex = RightHandPinky4 = 22
jointIndex = LeftHand = 42
jointIndex = RightHandIndex2 = 32
jointIndex = RightHandPinky1 = 19
jointIndex = polySurface1 = 80
jointIndex = transform2 = 73
jointIndex = LeftHandMiddle4 = 54
jointIndex = RightHandThumb4 = 38
jointIndex = LeftArm = 40
jointIndex = polySurface5 = 72
jointIndex = LeftHandIndex2 = 56
jointIndex = LeftToeBase = 10
jointIndex = RightHandThumb2 = 36
jointIndex = LeftHandIndex1 = 55
jointIndex = LeftHandPinky1 = 43
jointIndex = LeftHandRing1 = 47
jointIndex = RightFoot = 4
jointIndex = RightHandMiddle3 = 29
jointIndex = LeftHandRing2 = 48
jointIndex = RightUpLeg = 2
jointIndex = RightArm = 16
jointIndex = RightHandRing3 = 25
jointIndex = LeftLeg = 8
jointIndex = Neck = 63
jointIndex = RightToeBase = 5
jointIndex = LeftHandThumb4 = 62
jointIndex = RightHandRing2 = 24
jointIndex = polySurface6 = 70
jointIndex = RightHand = 18
jointIndex = RightHandMiddle2 = 28
jointIndex = transform3 = 81
jointIndex = RightHandMiddle1 = 27
jointIndex = LeftUpLeg = 7
jointIndex = LeftHandThumb2 = 60
jointIndex = transform4 = 77
jointIndex = LeftForeArm = 41
jointIndex = RightHandMiddle4 = 30
jointIndex = transform6 = 71
jointIndex = Spine1 = 13
jointIndex = LeftHandIndex4 = 58
jointIndex = LeftShoulder = 39
jointIndex = LeftHandPinky3 = 45
jointIndex = RightHandIndex1 = 31
jointIndex = LeftHandMiddle1 = 51
jointIndex = RightHandIndex4 = 34
jointIndex = RightHandRing4 = 26
jointIndex = Head = 64
jointIndex = transform5 = 79
jointIndex = transform1 = 68
jointIndex = polySurface2 = 78
jointIndex = Arcade_Miss_Fortune_B = 66
jointIndex = LeftToe_End = 11
jointIndex = RightShoulder = 15
jointIndex = polySurface4 = 74
jointIndex = LeftFoot = 9
jointIndex = LeftHandIndex3 = 57
jointIndex = transform7 = 75
jointIndex = Spine2 = 14
jointIndex = RightHandIndex3 = 33
jointIndex = RightHandRing1 = 23
jointIndex = LeftHandRing4 = 50
jointIndex = HeadTop_End = 65
jointIndex = LeftHandMiddle3 = 53
jointIndex = Hips = 1
