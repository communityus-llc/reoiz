name = LeBlancRavenborn_Avatar1
type = body+head
scale = 0.9
filename = LeBlancRavenborn_Avatar1/LeBlancRavenborn_Avatar1.fbx
texdir = LeBlancRavenborn_Avatar1/textures
joint = jointLeftHand = LeftHand
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointHead = Head
joint = jointLean = Spine
joint = jointRoot = Hips
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandRing2 = 23
jointIndex = HeadTop_End = 64
jointIndex = LeftLeg = 7
jointIndex = LeftArm = 39
jointIndex = LeftHand = 41
jointIndex = LeftHandRing3 = 48
jointIndex = Neck = 62
jointIndex = RightHandPinky3 = 20
jointIndex = LeftHandThumb1 = 58
jointIndex = LeftToe_End = 10
jointIndex = RightUpLeg = 1
jointIndex = RightLeg = 2
jointIndex = LeftHandMiddle4 = 53
jointIndex = LeftHandIndex1 = 54
jointIndex = RightHandPinky1 = 18
jointIndex = LeftHandRing4 = 49
jointIndex = RightHand = 17
jointIndex = RightHandRing3 = 24
jointIndex = LeftHandPinky4 = 45
jointIndex = LeftHandPinky3 = 44
jointIndex = LeftHandThumb4 = 61
jointIndex = Head = 63
jointIndex = LeftFoot = 8
jointIndex = LeftUpLeg = 6
jointIndex = LeftHandMiddle3 = 52
jointIndex = RightHandMiddle1 = 26
jointIndex = RightHandRing4 = 25
jointIndex = RightShoulder = 14
jointIndex = LeftHandMiddle1 = 50
jointIndex = LeftHandIndex4 = 57
jointIndex = RightHandThumb1 = 34
jointIndex = RightHandIndex1 = 30
jointIndex = RightHandIndex2 = 31
jointIndex = RightHandThumb3 = 36
jointIndex = LeftHandIndex3 = 56
jointIndex = RightToeBase = 4
jointIndex = RightToe_End = 5
jointIndex = LeftHandPinky1 = 42
jointIndex = LeftHandIndex2 = 55
jointIndex = LeftForeArm = 40
jointIndex = RightFoot = 3
jointIndex = RightHandPinky2 = 19
jointIndex = RightHandMiddle2 = 27
jointIndex = Spine2 = 13
jointIndex = RightHandThumb4 = 37
jointIndex = Spine = 11
jointIndex = RightHandThumb2 = 35
jointIndex = LeftShoulder = 38
jointIndex = polySurface7 = 66
jointIndex = polySurface8 = 65
jointIndex = RightHandPinky4 = 21
jointIndex = RightHandIndex4 = 33
jointIndex = LeftHandPinky2 = 43
jointIndex = LeftHandRing1 = 46
jointIndex = LeftHandThumb3 = 60
jointIndex = LeftToeBase = 9
jointIndex = LeftHandRing2 = 47
jointIndex = RightHandMiddle3 = 28
jointIndex = LeftHandMiddle2 = 51
jointIndex = Hips = 0
jointIndex = RightForeArm = 16
jointIndex = RightHandRing1 = 22
jointIndex = RightHandMiddle4 = 29
jointIndex = RightHandIndex3 = 32
jointIndex = LeftHandThumb2 = 59
jointIndex = Spine1 = 12
jointIndex = RightArm = 15
