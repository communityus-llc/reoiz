name = MalzaharBattleboss_Avatar
type = body+head
scale = 1
filename = MalzaharBattleboss_Avatar/MalzaharBattleboss_Avatar.fbx
texdir = MalzaharBattleboss_Avatar/textures
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Spine = 11
jointIndex = LeftHandPinky4 = 45
jointIndex = polySurface26 = 67
jointIndex = RightHandPinky2 = 19
jointIndex = LeftHandThumb3 = 60
jointIndex = LeftHandThumb1 = 58
jointIndex = RightHandThumb3 = 36
jointIndex = LeftHandMiddle2 = 51
jointIndex = LeftHandPinky2 = 43
jointIndex = RightHandThumb1 = 34
jointIndex = LeftHandRing3 = 48
jointIndex = RightHandPinky3 = 20
jointIndex = RightForeArm = 16
jointIndex = RightLeg = 2
jointIndex = RightToe_End = 5
jointIndex = RightHandPinky4 = 21
jointIndex = LeftHand = 41
jointIndex = RightHandIndex2 = 31
jointIndex = RightHandPinky1 = 18
jointIndex = transform2 = 70
jointIndex = LeftHandMiddle4 = 53
jointIndex = RightHandThumb4 = 37
jointIndex = LeftArm = 39
jointIndex = LeftHandIndex2 = 55
jointIndex = LeftToeBase = 9
jointIndex = RightHandThumb2 = 35
jointIndex = LeftHandIndex1 = 54
jointIndex = LeftHandPinky1 = 42
jointIndex = LeftHandRing1 = 46
jointIndex = RightFoot = 3
jointIndex = RightHandMiddle3 = 28
jointIndex = LeftHandRing2 = 47
jointIndex = RightUpLeg = 1
jointIndex = RightArm = 15
jointIndex = RightHandRing3 = 24
jointIndex = LeftLeg = 7
jointIndex = default1 = 65
jointIndex = RightToeBase = 4
jointIndex = LeftHandThumb4 = 61
jointIndex = RightHandRing2 = 23
jointIndex = RightHand = 17
jointIndex = RightHandMiddle2 = 27
jointIndex = transform3 = 68
jointIndex = RightHandMiddle1 = 26
jointIndex = LeftUpLeg = 6
jointIndex = LeftHandThumb2 = 59
jointIndex = polySurface261 = 64
jointIndex = polySurface25 = 69
jointIndex = LeftForeArm = 40
jointIndex = RightHandMiddle4 = 29
jointIndex = Spine1 = 12
jointIndex = LeftHandIndex4 = 57
jointIndex = LeftShoulder = 38
jointIndex = LeftHandPinky3 = 44
jointIndex = RightHandIndex1 = 30
jointIndex = LeftHandMiddle1 = 50
jointIndex = RightHandIndex4 = 33
jointIndex = RightHandRing4 = 25
jointIndex = Head = 62
jointIndex = transform1 = 66
jointIndex = LeftToe_End = 10
jointIndex = RightShoulder = 14
jointIndex = LeftFoot = 8
jointIndex = LeftHandIndex3 = 56
jointIndex = Spine2 = 13
jointIndex = RightHandIndex3 = 32
jointIndex = RightHandRing1 = 22
jointIndex = LeftHandRing4 = 49
jointIndex = HeadTop_End = 63
jointIndex = LeftHandMiddle3 = 52
jointIndex = Hips = 0
