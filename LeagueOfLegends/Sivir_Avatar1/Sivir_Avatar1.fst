name = Sivir_Avatar1
type = body+head
scale = .87
filename = Sivir_Avatar1/Sivir_Avatar1.fbx
texdir = Sivir_Avatar1/textures
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointHead = Head
joint = jointNeck = Neck
joint = jointRoot = Hips
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftShoulder = 38
jointIndex = LeftLeg = 7
jointIndex = LeftToe_End = 10
jointIndex = LeftFoot = 8
jointIndex = LeftUpLeg = 6
jointIndex = RightUpLeg = 1
jointIndex = RightLeg = 2
jointIndex = RightForeArm = 16
jointIndex = RightHandMiddle3 = 28
jointIndex = Neck = 41
jointIndex = RightHandPinky3 = 20
jointIndex = RightHandThumb3 = 36
jointIndex = polySurface54 = 45
jointIndex = RightHandPinky2 = 19
jointIndex = RightHandPinky1 = 18
jointIndex = RightHandRing1 = 22
jointIndex = Spine2 = 13
jointIndex = RightHandRing4 = 25
jointIndex = RightHand = 17
jointIndex = RightHandMiddle2 = 27
jointIndex = Hips = 0
jointIndex = LeftArm = 39
jointIndex = RightToeBase = 4
jointIndex = RightHandThumb2 = 35
jointIndex = RightFoot = 3
jointIndex = RightHandThumb1 = 34
jointIndex = RightHandMiddle1 = 26
jointIndex = RightToe_End = 5
jointIndex = Head = 42
jointIndex = Spine = 11
jointIndex = RightHandIndex4 = 33
jointIndex = HeadTop_End = 43
jointIndex = RightHandRing2 = 23
jointIndex = Spine1 = 12
jointIndex = RightHandThumb4 = 37
jointIndex = RightArm = 15
jointIndex = LeftForeArm = 40
jointIndex = polySurface53 = 44
jointIndex = LeftToeBase = 9
jointIndex = RightHandMiddle4 = 29
jointIndex = RightShoulder = 14
jointIndex = RightHandIndex3 = 32
jointIndex = RightHandRing3 = 24
jointIndex = RightHandIndex2 = 31
jointIndex = RightHandPinky4 = 21
jointIndex = RightHandIndex1 = 30
