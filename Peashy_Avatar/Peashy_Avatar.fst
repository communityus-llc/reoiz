name = Peashy_Avatar
type = body+head
scale = 1.5
filename = Peashy_Avatar/Peashy_Avatar.fbx
texdir = Peashy_Avatar/textures
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
joint = jointHead = Head
joint = jointRoot = Hips
joint = jointRightHand = RightHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = SMD_root_1 = 28
jointIndex = Hips = 0
jointIndex = LeftUpLeg = 6
jointIndex = Neck = 22
jointIndex = RightHand = 17
jointIndex = LeftFoot = 8
jointIndex = RightToeBase = 4
jointIndex = SMD_root_2 = 25
jointIndex = RightForeArm = 16
jointIndex = SMD_root_ = 27
jointIndex = LeftToe_End = 10
jointIndex = LeftShoulder = 18
jointIndex = LeftToeBase = 9
jointIndex = RightArm = 15
jointIndex = RightShoulder = 14
jointIndex = Spine = 11
jointIndex = LeftArm = 19
jointIndex = LeftHand = 21
jointIndex = RightUpLeg = 1
jointIndex = LeftForeArm = 20
jointIndex = Head = 23
jointIndex = SMD_root_3 = 26
jointIndex = LeftLeg = 7
jointIndex = Spine1 = 12
jointIndex = RightLeg = 2
jointIndex = RightFoot = 3
jointIndex = HeadTop_End = 24
jointIndex = Spine2 = 13
jointIndex = RightToe_End = 5
