name = IF_Avatar
type = body+head
scale = 1.25
filename = IF_Avatar/IF_Avatar.fbx
texdir = IF_Avatar/textures
joint = jointLeftHand = LeftHand
joint = jointRightHand = RightHand
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointNeck = Neck
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightShoulder = 14
jointIndex = RightHandThumb3 = 37
jointIndex = RightToe_End = 5
jointIndex = RightHandThumb2 = 36
jointIndex = LeftHandRing1 = 47
jointIndex = LeftHandPinky2 = 44
jointIndex = RightHandRing3 = 25
jointIndex = LeftHandIndex2 = 56
jointIndex = LeftHandThumb2 = 60
jointIndex = RightHandMiddle1 = 27
jointIndex = RightHandIndex2 = 32
jointIndex = RightHandRing1 = 23
jointIndex = RightUpLeg = 1
jointIndex = LeftToeBase = 9
jointIndex = RightHandIndex3 = 33
jointIndex = LeftHandThumb3 = 61
jointIndex = LeftHandMiddle1 = 51
jointIndex = polySurface4 = 71
jointIndex = RightHandPinky2 = 20
jointIndex = RightHandIndex1 = 31
jointIndex = RightHandIndex4 = 34
jointIndex = RightHandPinky3 = 21
jointIndex = LeftHandThumb1 = 59
jointIndex = Neck = 63
jointIndex = LeftHandPinky1 = 43
jointIndex = RightHandMiddle4 = 30
jointIndex = LeftHandMiddle4 = 54
jointIndex = LeftHandIndex4 = 58
jointIndex = RightHand = 18
jointIndex = RightHandRing4 = 26
jointIndex = LeftHandMiddle3 = 53
jointIndex = LeftHandIndex3 = 57
jointIndex = RightHandMiddle3 = 29
jointIndex = polySurface1 = 66
jointIndex = RightToeBase = 4
jointIndex = LeftFoot = 8
jointIndex = LeftForeArm = 41
jointIndex = RightFoot = 3
jointIndex = RightHandThumb4 = 38
jointIndex = LeftHandPinky3 = 45
jointIndex = polySurface3 = 68
jointIndex = LeftHandMiddle2 = 52
jointIndex = RightLeg = 2
jointIndex = LeftHandRing4 = 50
jointIndex = SMD_root_2 = 67
jointIndex = LeftHandRing2 = 48
jointIndex = polySurface2 = 70
jointIndex = Spine = 11
jointIndex = HeadTop_End = 65
jointIndex = LeftHandPinky4 = 46
jointIndex = joint1 = 16
jointIndex = RightForeArm = 17
jointIndex = RightHandPinky4 = 22
jointIndex = RightHandPinky1 = 19
jointIndex = LeftUpLeg = 6
jointIndex = RightHandThumb1 = 35
jointIndex = RightHandMiddle2 = 28
jointIndex = LeftArm = 40
jointIndex = Head = 64
jointIndex = LeftToe_End = 10
jointIndex = RightHandRing2 = 24
jointIndex = Hips = 0
jointIndex = LeftHand = 42
jointIndex = LeftHandIndex1 = 55
jointIndex = Spine1 = 12
jointIndex = RightArm = 15
jointIndex = LeftShoulder = 39
jointIndex = LeftHandRing3 = 49
jointIndex = Spine2 = 13
jointIndex = SMD_root_1 = 69
jointIndex = LeftLeg = 7
jointIndex = LeftHandThumb4 = 62
