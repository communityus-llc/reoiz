name = Metaknight_Avatar
type = body+head
scale = 2.1
filename = Metaknight_Avatar/Metaknight_Avatar.fbx
texdir = Metaknight_Avatar/textures
joint = jointLean = head
joint = jointLeftHand = l_hand
joint = jointHead = head
joint = jointRoot = hips
joint = jointRightHand = r_hand
jointIndex = hips = 0
jointIndex = polySurface3 = 22
jointIndex = r_arm = 2
jointIndex = polySurface11 = 17
jointIndex = polySurface20 = 28
jointIndex = l_foot = 13
jointIndex = Wing_VIS_O_OBJ_ = 20
jointIndex = polySurface2 = 24
jointIndex = r_foot = 10
jointIndex = l_hand = 8
jointIndex = transform2 = 25
jointIndex = arms = 26
jointIndex = polySurface1 = 18
jointIndex = l_toe_end = 15
jointIndex = l_toe = 14
jointIndex = r_elbow = 3
jointIndex = r_toe_end = 12
jointIndex = r_hand_end = 5
jointIndex = Metaknight_eye_VIS_O_OBJ_ = 21
jointIndex = transform1 = 27
jointIndex = l_elbow = 7
jointIndex = polySurface7 = 29
jointIndex = head = 1
jointIndex = l_arm = 6
jointIndex = polySurface4 = 16
jointIndex = r_hand = 4
jointIndex = l_hand_end = 9
jointIndex = r_toe = 11
