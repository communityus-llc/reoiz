name = sandyCheeks_rexport_f
type = body+head
scale = 1
filename = sandyCheeks_rexport_f/sandyCheeks_rexport_f.fbx
texdir = sandyCheeks_rexport_f/textures
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftFoot = 8
jointIndex = Spine1 = 12
jointIndex = RightHandIndex2 = 19
jointIndex = LeftHandThumb3 = 36
jointIndex = RightHandIndex4 = 21
jointIndex = RightShoulder = 14
jointIndex = RightHandThumb1 = 22
jointIndex = RightFoot = 3
jointIndex = RightHand = 17
jointIndex = LeftToe_End = 10
jointIndex = Spine = 11
jointIndex = RightToeBase = 4
jointIndex = LeftHandIndex2 = 31
jointIndex = RightHandThumb3 = 24
jointIndex = RightLeg = 2
jointIndex = RightUpLeg = 1
jointIndex = RightHandThumb4 = 25
jointIndex = Head = 39
jointIndex = HeadTop_End = 40
jointIndex = RightHandIndex3 = 20
jointIndex = RightHandIndex1 = 18
jointIndex = LeftHandThumb1 = 34
jointIndex = LeftArm = 27
jointIndex = RightHandThumb2 = 23
jointIndex = LeftToeBase = 9
jointIndex = LeftShoulder = 26
jointIndex = LeftHand = 29
jointIndex = LeftForeArm = 28
jointIndex = Neck = 38
jointIndex = Mesh = 41
jointIndex = LeftHandThumb2 = 35
jointIndex = LeftHandIndex1 = 30
jointIndex = LeftUpLeg = 6
jointIndex = RightArm = 15
jointIndex = Spine2 = 13
jointIndex = LeftHandThumb4 = 37
jointIndex = RightForeArm = 16
jointIndex = RightToe_End = 5
jointIndex = LeftHandIndex3 = 32
jointIndex = LeftLeg = 7
jointIndex = LeftHandIndex4 = 33
jointIndex = Hips = 0
