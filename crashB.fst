name = crashB
type = body+head
scale = 1
filename = crashB/crashB.fbx
texdir = crashB/textures
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
joint = jointHead = Head
joint = jointRoot = Hips
joint = jointLean = Spine
joint = jointEyeRight = RightEye
joint = jointEyeLeft = LeftEye
joint = jointRightHand = RightHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
bs = BrowsU_L = BrowUp = 1
bs = JawOpen = MouthOpen = 1
jointIndex = LeftHandMiddle2 = 43
jointIndex = LeftHandMiddle4 = 45
jointIndex = RightHandThumb2 = 19
jointIndex = Spine = 11
jointIndex = Spine2 = 13
jointIndex = RightFoot = 3
jointIndex = LeftUpLeg = 6
jointIndex = RightHandIndex2 = 31
jointIndex = RightHandIndex1 = 30
jointIndex = RightHandThumb3 = 20
jointIndex = LeftForeArm = 36
jointIndex = RightHandRing3 = 28
jointIndex = Head = 55
jointIndex = RightLeg = 2
jointIndex = Hips = 0
jointIndex = LeftHand = 37
jointIndex = LeftHandMiddle1 = 42
jointIndex = RightHandMiddle3 = 24
jointIndex = RightHandRing2 = 27
jointIndex = RightUpLeg = 1
jointIndex = LeftArm = 35
jointIndex = LeftFoot = 8
jointIndex = LeftToeBase = 9
jointIndex = HeadTop_End = 56
jointIndex = RightShoulder = 14
jointIndex = LeftHandIndex3 = 52
jointIndex = LeftShoulder = 34
jointIndex = RightHandThumb4 = 21
jointIndex = LeftHandRing2 = 39
jointIndex = Crash = 59
jointIndex = RightForeArm = 16
jointIndex = LeftHandIndex2 = 51
jointIndex = Crash1 = 60
jointIndex = RightHandMiddle4 = 25
jointIndex = RightHandRing1 = 26
jointIndex = RightToeBase = 4
jointIndex = LeftHandThumb4 = 49
jointIndex = RightArm = 15
jointIndex = RightHandThumb1 = 18
jointIndex = LeftHandRing3 = 40
jointIndex = LeftHandIndex1 = 50
jointIndex = RightHandRing4 = 29
jointIndex = RightHand = 17
jointIndex = LeftEye = 57
jointIndex = RightHandIndex4 = 33
jointIndex = RightHandIndex3 = 32
jointIndex = LeftHandRing4 = 41
jointIndex = RightEye = 58
jointIndex = LeftHandThumb2 = 47
jointIndex = LeftHandMiddle3 = 44
jointIndex = RightHandMiddle2 = 23
jointIndex = LeftLeg = 7
jointIndex = LeftHandThumb3 = 48
jointIndex = LeftHandIndex4 = 53
jointIndex = Neck = 54
jointIndex = RightToe_End = 5
jointIndex = LeftHandRing1 = 38
jointIndex = RightHandMiddle1 = 22
jointIndex = Spine1 = 12
jointIndex = LeftHandThumb1 = 46
jointIndex = LeftToe_End = 10
