name = robot_medic_hifi_blue
type = body+head
scale = 1
filename = robot_medic_hifi_blue/robot_medic_hifi_blue.fbx
texdir = robot_medic_hifi_blue/textures
joint = jointRoot = Hips
joint = jointHead = HeadTop_End
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointNeck = Head
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandThumb1 = 34
jointIndex = RightHandThumb2 = 35
jointIndex = LeftShoulder = 38
jointIndex = RightHandThumb3 = 36
jointIndex = RightHandThumb4 = 37
jointIndex = Neck = 62
jointIndex = Head = 63
jointIndex = RightUpLeg = 1
jointIndex = LeftHandRing1 = 46
jointIndex = LeftHandRing2 = 47
jointIndex = LeftHandRing3 = 48
jointIndex = LeftHandRing4 = 49
jointIndex = RightHandPinky1 = 18
jointIndex = RightHandPinky2 = 19
jointIndex = LeftHand = 41
jointIndex = RightHandPinky3 = 20
jointIndex = RightHandPinky4 = 21
jointIndex = RightToe_End = 5
jointIndex = LeftLeg = 7
jointIndex = RightHandRing1 = 22
jointIndex = RightHandRing2 = 23
jointIndex = RightHandRing3 = 24
jointIndex = RightHandRing4 = 25
jointIndex = LeftToe_End = 10
jointIndex = RightLeg = 2
jointIndex = RightHandIndex1 = 30
jointIndex = RightHandIndex2 = 31
jointIndex = HeadTop_End = 64
jointIndex = RightHandIndex3 = 32
jointIndex = LeftFoot = 8
jointIndex = RightHandIndex4 = 33
jointIndex = LeftHandThumb1 = 58
jointIndex = RightHand = 17
jointIndex = LeftHandIndex1 = 54
jointIndex = LeftHandThumb2 = 59
jointIndex = RightForeArm = 16
jointIndex = LeftHandIndex2 = 55
jointIndex = LeftHandThumb3 = 60
jointIndex = LeftHandIndex3 = 56
jointIndex = LeftHandThumb4 = 61
jointIndex = LeftHandIndex4 = 57
jointIndex = Robot = 65
jointIndex = LeftUpLeg = 6
jointIndex = Hips = 0
jointIndex = RightToeBase = 4
jointIndex = LeftForeArm = 40
jointIndex = LeftArm = 39
jointIndex = LeftHandMiddle1 = 50
jointIndex = LeftHandMiddle2 = 51
jointIndex = LeftHandMiddle3 = 52
jointIndex = LeftHandMiddle4 = 53
jointIndex = LeftToeBase = 9
jointIndex = RightHandMiddle1 = 26
jointIndex = RightFoot = 3
jointIndex = RightArm = 15
jointIndex = RightHandMiddle2 = 27
jointIndex = RightHandMiddle3 = 28
jointIndex = RightHandMiddle4 = 29
jointIndex = Spine1 = 12
jointIndex = LeftHandPinky1 = 42
jointIndex = Spine2 = 13
jointIndex = LeftHandPinky2 = 43
jointIndex = LeftHandPinky3 = 44
jointIndex = LeftHandPinky4 = 45
jointIndex = Spine = 11
jointIndex = RightShoulder = 14
