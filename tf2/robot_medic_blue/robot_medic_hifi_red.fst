name = robot_medic_hifi_red
type = body+head
scale = 1
filename = robot_medic_hifi_red/robot_medic_hifi_red.fbx
texdir = robot_medic_hifi_red/textures
joint = jointEyeLeft = LeftEye
joint = jointLeftHand = LeftHand
joint = jointRoot = Hips
joint = jointHead = HeadTop_End
joint = jointRightHand = RightHand
joint = jointLean = Spine
joint = jointEyeRight = RightEye
joint = jointNeck = Head
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightHandMiddle2 = 35
jointIndex = RightHandMiddle3 = 36
jointIndex = LeftEye = 64
jointIndex = RightHandMiddle4 = 37
jointIndex = LeftHandIndex1 = 50
jointIndex = LeftHandIndex2 = 51
jointIndex = LeftArm = 39
jointIndex = LeftHandIndex3 = 52
jointIndex = LeftHandIndex4 = 53
jointIndex = Spine1 = 12
jointIndex = Spine2 = 13
jointIndex = LeftUpLeg = 6
jointIndex = LeftHandRing1 = 46
jointIndex = LeftHandRing2 = 47
jointIndex = RightForeArm = 16
jointIndex = LeftHandRing3 = 48
jointIndex = LeftHandRing4 = 49
jointIndex = LeftForeArm = 40
jointIndex = RightEye = 65
jointIndex = RightHand = 17
jointIndex = RightArm = 15
jointIndex = LeftHandThumb1 = 54
jointIndex = RightUpLeg = 1
jointIndex = LeftHandThumb2 = 55
jointIndex = LeftHandThumb3 = 56
jointIndex = LeftHandThumb4 = 57
jointIndex = RightHandIndex1 = 22
jointIndex = RightHandIndex2 = 23
jointIndex = RightHandIndex3 = 24
jointIndex = RightHandIndex4 = 25
jointIndex = RightToeBase = 4
jointIndex = LeftHandMiddle1 = 58
jointIndex = LeftToeBase = 9
jointIndex = LeftHandPinky1 = 42
jointIndex = LeftHandMiddle2 = 59
jointIndex = Head = 63
jointIndex = LeftHandPinky2 = 43
jointIndex = LeftHandMiddle3 = 60
jointIndex = Neck = 62
jointIndex = LeftHandPinky3 = 44
jointIndex = LeftHandMiddle4 = 61
jointIndex = Hips = 0
jointIndex = LeftHandPinky4 = 45
jointIndex = RightFoot = 3
jointIndex = HeadTop_End = 66
jointIndex = Robot = 67
jointIndex = LeftLeg = 7
jointIndex = LeftHand = 41
jointIndex = RightShoulder = 14
jointIndex = RightHandThumb1 = 18
jointIndex = Spine = 11
jointIndex = RightHandThumb2 = 19
jointIndex = RightHandThumb3 = 20
jointIndex = RightHandThumb4 = 21
jointIndex = LeftShoulder = 38
jointIndex = RightToe_End = 5
jointIndex = LeftToe_End = 10
jointIndex = RightHandPinky1 = 30
jointIndex = RightHandRing1 = 26
jointIndex = RightHandPinky2 = 31
jointIndex = RightHandRing2 = 27
jointIndex = RightHandPinky3 = 32
jointIndex = RightHandRing3 = 28
jointIndex = RightHandPinky4 = 33
jointIndex = RightHandRing4 = 29
jointIndex = LeftFoot = 8
jointIndex = RightLeg = 2
jointIndex = RightHandMiddle1 = 34
