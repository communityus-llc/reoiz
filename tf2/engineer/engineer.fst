name = engineer
type = body+head
scale = 1
filename = engineer/engineer.fbx
texdir = engineer/textures
joint = jointEyeLeft = LeftEye
joint = jointLeftHand = LeftHand
joint = jointNeck = Head
joint = jointRightHand = RightHand
joint = jointRoot = Hips
joint = jointHead = HeadTop_End
joint = jointEyeRight = RightEye
joint = jointLean = Spine
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftForeArm = 40
jointIndex = LeftUpLeg = 6
jointIndex = RightHandMiddle1 = 34
jointIndex = RightHandMiddle2 = 35
jointIndex = RightHandMiddle3 = 36
jointIndex = RightHandMiddle4 = 37
jointIndex = LeftHand = 41
jointIndex = LeftHandThumb1 = 54
jointIndex = LeftHandThumb2 = 55
jointIndex = Neck = 62
jointIndex = Head = 63
jointIndex = LeftHandThumb3 = 56
jointIndex = Hips = 0
jointIndex = LeftHandThumb4 = 57
jointIndex = LeftShoulder = 38
jointIndex = LeftHandPinky1 = 42
jointIndex = Spine = 11
jointIndex = RightHand = 17
jointIndex = LeftHandPinky2 = 43
jointIndex = LeftHandPinky3 = 44
jointIndex = LeftHandPinky4 = 45
jointIndex = HeadTop_End = 66
jointIndex = RightToe_End = 5
jointIndex = LeftFoot = 8
jointIndex = LeftToeBase = 9
jointIndex = RightLeg = 2
jointIndex = RightShoulder = 14
jointIndex = RightHandIndex1 = 22
jointIndex = RightHandIndex2 = 23
jointIndex = RightHandIndex3 = 24
jointIndex = RightHandIndex4 = 25
jointIndex = engineer = 67
jointIndex = LeftHandMiddle1 = 58
jointIndex = LeftLeg = 7
jointIndex = RightFoot = 3
jointIndex = LeftHandMiddle2 = 59
jointIndex = LeftHandMiddle3 = 60
jointIndex = LeftHandMiddle4 = 61
jointIndex = RightHandRing1 = 26
jointIndex = RightHandRing2 = 27
jointIndex = RightHandRing3 = 28
jointIndex = RightHandRing4 = 29
jointIndex = RightForeArm = 16
jointIndex = Spine1 = 12
jointIndex = LeftToe_End = 10
jointIndex = Spine2 = 13
jointIndex = RightEye = 65
jointIndex = RightHandThumb1 = 18
jointIndex = RightHandThumb2 = 19
jointIndex = LeftHandRing1 = 46
jointIndex = RightHandThumb3 = 20
jointIndex = LeftHandRing2 = 47
jointIndex = RightHandThumb4 = 21
jointIndex = LeftHandRing3 = 48
jointIndex = LeftHandRing4 = 49
jointIndex = RightArm = 15
jointIndex = LeftHandIndex1 = 50
jointIndex = LeftHandIndex2 = 51
jointIndex = LeftHandIndex3 = 52
jointIndex = LeftHandIndex4 = 53
jointIndex = LeftEye = 64
jointIndex = RightHandPinky1 = 30
jointIndex = RightHandPinky2 = 31
jointIndex = RightHandPinky3 = 32
jointIndex = RightHandPinky4 = 33
jointIndex = RightUpLeg = 1
jointIndex = LeftArm = 39
jointIndex = RightToeBase = 4
