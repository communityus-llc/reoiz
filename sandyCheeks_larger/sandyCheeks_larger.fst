name = sandyCheeks_larger
type = body+head
scale = 1
filename = sandyCheeks_larger/sandyCheeks_larger.fbx
texdir = sandyCheeks_larger/textures
joint = jointHead = Head
joint = jointRightHand = RightHand
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftLeg = 7
jointIndex = RightHand = 17
jointIndex = RightToeBase = 4
jointIndex = RightHandIndex1 = 18
jointIndex = Head = 31
jointIndex = Spine2 = 13
jointIndex = RightToe_End = 5
jointIndex = RightShoulder = 14
jointIndex = RightHandIndex3 = 20
jointIndex = Spine1 = 12
jointIndex = Hips = 0
jointIndex = RightArm = 15
jointIndex = RightHandIndex4 = 21
jointIndex = LeftArm = 23
jointIndex = LeftHand = 25
jointIndex = RightFoot = 3
jointIndex = LeftToeBase = 9
jointIndex = LeftHandIndex2 = 27
jointIndex = Neck = 30
jointIndex = RightForeArm = 16
jointIndex = LeftFoot = 8
jointIndex = LeftShoulder = 22
jointIndex = Spine = 11
jointIndex = Mesh = 33
jointIndex = RightLeg = 2
jointIndex = RightUpLeg = 1
jointIndex = LeftHandIndex1 = 26
jointIndex = LeftHandIndex3 = 28
jointIndex = LeftUpLeg = 6
jointIndex = LeftToe_End = 10
jointIndex = HeadTop_End = 32
jointIndex = RightHandIndex2 = 19
jointIndex = LeftHandIndex4 = 29
jointIndex = LeftForeArm = 24
