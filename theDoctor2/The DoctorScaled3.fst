name = The Doctor2
type = body+head
scale = 0.33
filename = The Doctor2/The Doctor2.fbx
texdir = The Doctor2/textures
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
joint = jointRightHand = RightHand
joint = jointHead = HeadTop_End
joint = jointNeck = Neck
joint = jointRoot = Hips
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
bs = JawOpen = MouthOpen = 0.7
bs = LipsUpperOpen = UpperLipOut = 1
bs = BrowsD_L = BrowsDown_Left = 1
bs = BrowsU_R = BrowsUp_Right = 1
bs = ChinUpperRaise = UpperLipUp_Right = 0.5
bs = ChinUpperRaise = UpperLipUp_Left = 0.5
bs = BrowsD_R = BrowsDown_Right = 1
bs = MouthSmile_R = Smile_Right = 1
bs = Sneer = Squint_Right = 0.5
bs = Sneer = Squint_Left = 0.5
bs = Sneer = NoseScrunch_Right = 0.75
bs = Sneer = NoseScrunch_Left = 0.75
bs = EyeOpen_L = EyesWide_Left = 1
bs = LipsUpperUp = UpperLipUp_Right = 0.7
bs = LipsUpperUp = UpperLipUp_Left = 0.7
bs = MouthDimple_L = Smile_Left = 0.25
bs = MouthFrown_L = Frown_Left = 1
bs = MouthRight = Midmouth_Right = 1
bs = EyeBlink_L = Blink_Left = 1
bs = BrowsU_C = BrowsUp_Right = 1
bs = BrowsU_C = BrowsUp_Left = 1
bs = ChinLowerRaise = Jaw_Up = 1
bs = EyeSquint_R = Squint_Right = 1
bs = JawLeft = JawRotateY_Left = 0.5
bs = EyeOpen_R = EyesWide_Right = 1
bs = MouthLeft = Midmouth_Left = 1
bs = EyeBlink_R = Blink_Right = 1
bs = BrowsU_L = BrowsUp_Left = 1
bs = MouthDimple_R = Smile_Right = 0.25
bs = LipsLowerClose = LowerLipIn = 1
bs = JawFwd = JawForeward = 1
bs = LipsLowerOpen = LowerLipOut = 1
bs = LipsLowerDown = LowerLipDown_Right = 0.7
bs = LipsLowerDown = LowerLipDown_Left = 0.7
bs = LipsUpperClose = UpperLipIn = 1
bs = JawRight = Jaw_Right = 1
bs = Puff = CheekPuff_Right = 1
bs = Puff = CheekPuff_Left = 1
bs = MouthSmile_L = Smile_Left = 1
bs = LipsPucker = MouthNarrow_Right = 1
bs = LipsPucker = MouthNarrow_Left = 1
bs = MouthFrown_R = Frown_Right = 1
bs = LipsFunnel = TongueUp = 1
bs = LipsFunnel = MouthWhistle_NarrowAdjust_Right = 0.5
bs = LipsFunnel = MouthWhistle_NarrowAdjust_Left = 0.5
bs = LipsFunnel = MouthNarrow_Right = 1
bs = LipsFunnel = MouthNarrow_Left = 1
bs = LipsFunnel = Jaw_Down = 0.36
bs = LipsFunnel = JawForeward = 0.39
bs = EyeSquint_L = Squint_Left = 1
jointIndex = LeftHandMiddle3 = 53
jointIndex = RightArm = 16
jointIndex = LeftHandIndex4 = 58
jointIndex = LeftHandThumb1 = 59
jointIndex = RightHandMiddle1 = 27
jointIndex = RightHandPinky3 = 21
jointIndex = RightUpLeg = 2
jointIndex = RightHandThumb4 = 38
jointIndex = RightHandRing4 = 26
jointIndex = Neck = 63
jointIndex = LeftHandPinky2 = 44
jointIndex = Head = 64
jointIndex = Spine1 = 13
jointIndex = LeftForeArm = 41
jointIndex = RightLeg = 3
jointIndex = RightFoot = 4
jointIndex = Spine = 12
jointIndex = LeftHandPinky3 = 45
jointIndex = LeftUpLeg = 7
jointIndex = LeftHandThumb2 = 60
jointIndex = DrawCall_0919.2 = 1
jointIndex = RightHandMiddle3 = 29
jointIndex = HeadTop_End = 65
jointIndex = RightForeArm = 17
jointIndex = LeftHand = 42
jointIndex = RightHandPinky2 = 20
jointIndex = LeftHandThumb3 = 61
jointIndex = RightHandMiddle4 = 30
jointIndex = RightHandIndex3 = 33
jointIndex = LeftHandRing1 = 47
jointIndex = LeftHandRing4 = 50
jointIndex = LeftLeg = 8
jointIndex = LeftHandPinky1 = 43
jointIndex = LeftHandIndex1 = 55
jointIndex = LeftHandThumb4 = 62
jointIndex = RightHandIndex1 = 31
jointIndex = LeftHandMiddle1 = 51
jointIndex = RightHandThumb1 = 35
jointIndex = RightHandPinky4 = 22
jointIndex = LeftToe_End = 11
jointIndex = RightHandIndex4 = 34
jointIndex = RightHandRing3 = 25
jointIndex = RightHandMiddle2 = 28
jointIndex = RightHandPinky1 = 19
jointIndex = RightToeBase = 5
jointIndex = RightHand = 18
jointIndex = LeftHandPinky4 = 46
jointIndex = LeftHandRing2 = 48
jointIndex = LeftHandIndex3 = 57
jointIndex = Hips = 0
jointIndex = LeftArm = 40
jointIndex = Spine2 = 14
jointIndex = RightHandRing2 = 24
jointIndex = RightShoulder = 15
jointIndex = LeftToeBase = 10
jointIndex = LeftHandMiddle2 = 52
jointIndex = LeftShoulder = 39
jointIndex = LeftHandRing3 = 49
jointIndex = RightHandThumb2 = 36
jointIndex = RightHandRing1 = 23
jointIndex = RightHandThumb3 = 37
jointIndex = LeftHandMiddle4 = 54
jointIndex = LeftHandIndex2 = 56
jointIndex = LeftFoot = 9
jointIndex = RightToe_End = 6
jointIndex = RightHandIndex2 = 32
