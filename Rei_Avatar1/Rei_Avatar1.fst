name = Rei_Avatar1
type = body+head
scale = 1.05
filename = Rei_Avatar1/Rei_Avatar1.fbx
texdir = Rei_Avatar1/textures
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointLeftHand = LeftHand
joint = jointNeck = Neck
joint = jointHead = Head
joint = jointRightHand = RightHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightToeBase = 4
jointIndex = LeftHand = 25
jointIndex = SMD_root_1 = 35
jointIndex = Hips = 0
jointIndex = RightHandIndex3 = 20
jointIndex = LeftHandIndex2 = 27
jointIndex = LeftHandIndex3 = 28
jointIndex = LeftHandIndex1 = 26
jointIndex = polySurface3 = 34
jointIndex = RightHand = 17
jointIndex = Spine2 = 13
jointIndex = LeftArm = 23
jointIndex = Spine1 = 12
jointIndex = RightHandIndex2 = 19
jointIndex = LeftLeg = 7
jointIndex = RightForeArm = 16
jointIndex = LeftShoulder = 22
jointIndex = RightToe_End = 5
jointIndex = Spine = 11
jointIndex = RightShoulder = 14
jointIndex = RightUpLeg = 1
jointIndex = HeadTop_End = 32
jointIndex = RightFoot = 3
jointIndex = Head = 31
jointIndex = RightHandIndex4 = 21
jointIndex = LeftForeArm = 24
jointIndex = hair = 33
jointIndex = RightArm = 15
jointIndex = RightHandIndex1 = 18
jointIndex = LeftHandIndex4 = 29
jointIndex = LeftFoot = 8
jointIndex = LeftUpLeg = 6
jointIndex = RightLeg = 2
jointIndex = Neck = 30
jointIndex = LeftToe_End = 10
jointIndex = LeftToeBase = 9
