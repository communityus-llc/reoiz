name = Nepgear_avatar_hf
type = body+head
scale = 0.85
filename = Nepgear_avatar_hf/Nepgear_avatar_hf.fbx
texdir = Nepgear_avatar_hf/textures
joint = jointLeftHand = LeftHand
joint = jointRoot = Hips
joint = jointHead = Head
joint = jointNeck = Neck
joint = jointRightHand = RightHand
joint = jointLean = Spine
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftHandThumb4 = 61
jointIndex = RightFoot = 3
jointIndex = RightHandIndex4 = 33
jointIndex = LeftFoot = 8
jointIndex = RightHandMiddle2 = 27
jointIndex = RightToe_End = 5
jointIndex = RightHandIndex2 = 31
jointIndex = Hips = 0
jointIndex = RightHandRing2 = 23
jointIndex = RightForeArm = 16
jointIndex = RightHandMiddle4 = 29
jointIndex = RightHandThumb2 = 35
jointIndex = SMD_root_3 = 69
jointIndex = SMD_root_2 = 68
jointIndex = LeftHandIndex1 = 54
jointIndex = RightHandThumb3 = 36
jointIndex = LeftHandMiddle4 = 53
jointIndex = LeftHandRing4 = 49
jointIndex = SMD_root_ = 66
jointIndex = RightHandPinky3 = 20
jointIndex = RightHandPinky2 = 19
jointIndex = LeftHandRing1 = 46
jointIndex = SMD_root_1 = 67
jointIndex = RightUpLeg = 1
jointIndex = RightLeg = 2
jointIndex = RightHandIndex1 = 30
jointIndex = Spine = 11
jointIndex = LeftHandIndex4 = 57
jointIndex = LeftToeBase = 9
jointIndex = RightHandMiddle3 = 28
jointIndex = RightToeBase = 4
jointIndex = RightArm = 15
jointIndex = LeftHandThumb1 = 58
jointIndex = LeftHandPinky2 = 43
jointIndex = LeftHandMiddle2 = 51
jointIndex = HeadTop_End = 64
jointIndex = RightHandThumb1 = 34
jointIndex = Spine1 = 12
jointIndex = LeftHandRing2 = 47
jointIndex = RightHandIndex3 = 32
jointIndex = LeftForeArm = 40
jointIndex = LeftHandPinky4 = 45
jointIndex = LeftHandPinky3 = 44
jointIndex = LeftHandThumb3 = 60
jointIndex = RightHandThumb4 = 37
jointIndex = LeftHandThumb2 = 59
jointIndex = LeftLeg = 7
jointIndex = LeftHand = 41
jointIndex = Head = 63
jointIndex = LeftToe_End = 10
jointIndex = RightHandRing3 = 24
jointIndex = LeftArm = 39
jointIndex = LeftHandIndex2 = 55
jointIndex = LeftHandIndex3 = 56
jointIndex = Spine2 = 13
jointIndex = Neck = 62
jointIndex = LeftShoulder = 38
jointIndex = RightHandPinky1 = 18
jointIndex = RightShoulder = 14
jointIndex = RightHandRing4 = 25
jointIndex = LeftHandMiddle1 = 50
jointIndex = RightHandPinky4 = 21
jointIndex = LeftUpLeg = 6
jointIndex = RightHandMiddle1 = 26
jointIndex = LeftHandRing3 = 48
jointIndex = LeftHandMiddle3 = 52
jointIndex = RightHandRing1 = 22
jointIndex = LeftHandPinky1 = 42
jointIndex = RightHand = 17
