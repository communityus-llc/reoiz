name = MegaDrive
type = body+head
scale = 1
filename = MegaDrive/MegaDrive.fbx
texdir = MegaDrive/textures
joint = jointRoot = Hips
joint = jointLean = Spine
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
joint = jointRightHand = RightHand
joint = jointHead = HeadTop_End
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
bs = MouthDimple_R = Smile_Right = 0.25
bs = BrowsU_R = BrowsUp_Right = 1
bs = EyeOpen_R = EyesWide_Right = 1
bs = ChinLowerRaise = Jaw_Up = 1
bs = EyeSquint_L = Squint_Left = 1
bs = LipsUpperUp = UpperLipUp_Right = 0.7
bs = LipsUpperUp = UpperLipUp_Left = 0.7
bs = LipsLowerOpen = LowerLipOut = 1
bs = LipsFunnel = TongueUp = 1
bs = LipsFunnel = MouthWhistle_NarrowAdjust_Right = 0.5
bs = LipsFunnel = MouthWhistle_NarrowAdjust_Left = 0.5
bs = LipsFunnel = MouthNarrow_Right = 1
bs = LipsFunnel = MouthNarrow_Left = 1
bs = LipsFunnel = Jaw_Down = 0.36
bs = LipsFunnel = JawForeward = 0.39
bs = BrowsU_L = BrowsUp_Left = 1
bs = LipsUpperOpen = UpperLipOut = 1
bs = LipsLowerDown = LowerLipDown_Right = 0.7
bs = LipsLowerDown = LowerLipDown_Left = 0.7
bs = BrowsD_L = BrowsDown_Left = 1
bs = BrowsD_R = BrowsDown_Right = 1
bs = JawFwd = JawForeward = 1
bs = MouthSmile_R = Smile_Right = 1
bs = JawOpen = MouthOpen = 0.7
bs = MouthFrown_R = Frown_Right = 1
bs = MouthLeft = Midmouth_Left = 1
bs = Sneer = Squint_Right = 0.5
bs = Sneer = Squint_Left = 0.5
bs = Sneer = NoseScrunch_Right = 0.75
bs = Sneer = NoseScrunch_Left = 0.75
bs = Puff = CheekPuff_Right = 1
bs = Puff = CheekPuff_Left = 1
bs = LipsUpperClose = UpperLipIn = 1
bs = MouthRight = Midmouth_Right = 1
bs = EyeSquint_R = Squint_Right = 1
bs = EyeBlink_L = Blink_Left = 1
bs = JawRight = Jaw_Right = 1
bs = ChinUpperRaise = UpperLipUp_Right = 0.5
bs = ChinUpperRaise = UpperLipUp_Left = 0.5
bs = BrowsU_C = BrowsUp_Right = 1
bs = BrowsU_C = BrowsUp_Left = 1
bs = LipsPucker = MouthNarrow_Right = 1
bs = LipsPucker = MouthNarrow_Left = 1
bs = MouthFrown_L = Frown_Left = 1
bs = MouthSmile_L = Smile_Left = 1
bs = MouthDimple_L = Smile_Left = 0.25
bs = EyeOpen_L = EyesWide_Left = 1
bs = LipsLowerClose = LowerLipIn = 1
bs = EyeBlink_R = Blink_Right = 1
bs = JawLeft = JawRotateY_Left = 0.5
jointIndex = RightHandRing2 = 24
jointIndex = RightUpLeg = 2
jointIndex = LeftLeg = 8
jointIndex = RightToeBase = 5
jointIndex = Hips = 0
jointIndex = LeftHandRing4 = 50
jointIndex = LeftHand = 42
jointIndex = RightHandIndex3 = 33
jointIndex = RightShoulder = 15
jointIndex = Spine1 = 13
jointIndex = Spine2 = 14
jointIndex = RightHandRing1 = 23
jointIndex = RightToe_End = 6
jointIndex = LeftHandMiddle4 = 54
jointIndex = RightHandRing4 = 26
jointIndex = LeftHandThumb4 = 62
jointIndex = HeadTop_End = 65
jointIndex = Spine = 12
jointIndex = RightFoot = 4
jointIndex = LeftHandThumb2 = 60
jointIndex = LeftToeBase = 10
jointIndex = LeftHandMiddle2 = 52
jointIndex = RightHandMiddle4 = 30
jointIndex = LeftHandPinky2 = 44
jointIndex = RightHandMiddle1 = 27
jointIndex = RightHandThumb3 = 37
jointIndex = LeftHandPinky4 = 46
jointIndex = RightHandPinky1 = 19
jointIndex = LeftHandIndex4 = 58
jointIndex = RightHandThumb4 = 38
jointIndex = RightHandIndex1 = 31
jointIndex = RightHandThumb2 = 36
jointIndex = MegaDrive1 = 1
jointIndex = LeftToe_End = 11
jointIndex = LeftHandThumb1 = 59
jointIndex = LeftShoulder = 39
jointIndex = RightHandThumb1 = 35
jointIndex = LeftUpLeg = 7
jointIndex = RightHand = 18
jointIndex = RightArm = 16
jointIndex = RightHandRing3 = 25
jointIndex = RightHandPinky2 = 20
jointIndex = LeftHandMiddle1 = 51
jointIndex = LeftHandRing1 = 47
jointIndex = LeftHandRing3 = 49
jointIndex = RightHandIndex4 = 34
jointIndex = LeftHandIndex2 = 56
jointIndex = LeftHandIndex1 = 55
jointIndex = RightHandMiddle3 = 29
jointIndex = LeftHandThumb3 = 61
jointIndex = LeftFoot = 9
jointIndex = RightLeg = 3
jointIndex = RightHandMiddle2 = 28
jointIndex = Head = 64
jointIndex = LeftArm = 40
jointIndex = LeftHandPinky1 = 43
jointIndex = LeftHandMiddle3 = 53
jointIndex = LeftForeArm = 41
jointIndex = LeftHandIndex3 = 57
jointIndex = RightHandIndex2 = 32
jointIndex = LeftHandRing2 = 48
jointIndex = RightHandPinky4 = 22
jointIndex = RightHandPinky3 = 21
jointIndex = LeftHandPinky3 = 45
jointIndex = Neck = 63
jointIndex = RightForeArm = 17
