name = Moana_Avatar
type = body+head
scale = 1.0
filename = Moana_Avatar/Moana_Avatar.fbx
texdir = Moana_Avatar/textures
joint = jointHead = Head
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointRoot = Hips
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightForeArm = 17
jointIndex = RightShoulder = 15
jointIndex = RightHandThumb4 = 26
jointIndex = RightFoot = 4
jointIndex = Hips = 1
jointIndex = Spine2 = 14
jointIndex = RightHand = 18
jointIndex = Head = 40
jointIndex = LeftToeBase = 10
jointIndex = RightHandThumb2 = 24
jointIndex = RightUpLeg = 2
jointIndex = LeftHandIndex1 = 31
jointIndex = LeftUpLeg = 7
jointIndex = RightLeg = 3
jointIndex = Spine1 = 13
jointIndex = Moana_Avatar = 0
jointIndex = LeftToe_End = 11
jointIndex = LeftArm = 28
jointIndex = LeftHandThumb2 = 36
jointIndex = LeftFoot = 9
jointIndex = RightHandIndex2 = 20
jointIndex = Neck = 39
jointIndex = LeftHandThumb4 = 38
jointIndex = LeftHandIndex2 = 32
jointIndex = LeftHandIndex3 = 33
jointIndex = RightHandThumb3 = 25
jointIndex = RightArm = 16
jointIndex = RightToeBase = 5
jointIndex = LeftHandThumb1 = 35
jointIndex = HeadTop_End = 41
jointIndex = RightHandIndex4 = 22
jointIndex = RightHandThumb1 = 23
jointIndex = Spine = 12
jointIndex = LeftForeArm = 29
jointIndex = LeftHandIndex4 = 34
jointIndex = LeftHand = 30
jointIndex = RightHandIndex1 = 19
jointIndex = RightToe_End = 6
jointIndex = RightHandIndex3 = 21
jointIndex = LeftLeg = 8
jointIndex = LeftShoulder = 27
jointIndex = LeftHandThumb3 = 37
