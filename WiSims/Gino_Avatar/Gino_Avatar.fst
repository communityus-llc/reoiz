name = Gino_Avatar
type = body+head
scale = 1
filename = Gino_Avatar/Gino_Avatar.fbx
texdir = Gino_Avatar/textures
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
joint = jointHead = Head
joint = jointRoot = Hips
joint = jointRightHand = RightHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Spine2 = 13
jointIndex = HeadTop_End = 24
jointIndex = RightShoulder = 14
jointIndex = Object003_001 = 26
jointIndex = RightHand = 17
jointIndex = RightUpLeg = 1
jointIndex = RightArm = 15
jointIndex = RightForeArm = 16
jointIndex = LeftLeg = 7
jointIndex = Hips = 0
jointIndex = LeftArm = 19
jointIndex = LeftForeArm = 20
jointIndex = RightLeg = 2
jointIndex = RightFoot = 3
jointIndex = LeftFoot = 8
jointIndex = Spine1 = 12
jointIndex = LeftToe_End = 10
jointIndex = LeftToeBase = 9
jointIndex = Spine = 11
jointIndex = RightToe_End = 5
jointIndex = Neck = 22
jointIndex = Object002_001 = 27
jointIndex = Head = 23
jointIndex = LeftHand = 21
jointIndex = LeftShoulder = 18
jointIndex = RightToeBase = 4
jointIndex = LeftUpLeg = 6
jointIndex = Object001_001 = 25
