name = Walker_Avatar
type = body+head
scale = 1
filename = Walker_Avatar/Walker_Avatar.fbx
texdir = Walker_Avatar/textures
joint = jointNeck = Neck
joint = jointRoot = Hips
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
joint = jointRightHand = RightHand
joint = jointHead = Head
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftToeBase = 9
jointIndex = Neck = 22
jointIndex = RightHand = 17
jointIndex = Spine2 = 13
jointIndex = Spine1 = 12
jointIndex = RightUpLeg = 1
jointIndex = LeftFoot = 8
jointIndex = RightShoulder = 14
jointIndex = Spine = 11
jointIndex = RightLeg = 2
jointIndex = RightArm = 15
jointIndex = LeftArm = 19
jointIndex = LeftShoulder = 18
jointIndex = RightFoot = 3
jointIndex = RightToe_End = 5
jointIndex = Head = 23
jointIndex = Mesh = 25
jointIndex = RightForeArm = 16
jointIndex = LeftLeg = 7
jointIndex = LeftUpLeg = 6
jointIndex = RightToeBase = 4
jointIndex = HeadTop_End = 24
jointIndex = LeftToe_End = 10
jointIndex = Hips = 0
jointIndex = LeftForeArm = 20
jointIndex = LeftHand = 21
