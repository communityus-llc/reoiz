name = Rosalyn_Avatar
type = body+head
scale = 1
filename = Rosalyn_Avatar/Rosalyn_Avatar.fbx
texdir = Rosalyn_Avatar/textures
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
joint = jointHead = Head
joint = jointRoot = Hips
joint = jointRightHand = RightHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Spine2 = 9
jointIndex = HeadTop_End = 28
jointIndex = RightShoulder = 10
jointIndex = RightHandIndex3 = 16
jointIndex = RightHand = 13
jointIndex = RightHandIndex2 = 15
jointIndex = RightUpLeg = 1
jointIndex = RightArm = 11
jointIndex = RightForeArm = 12
jointIndex = LeftLeg = 5
jointIndex = RightHandIndex1 = 14
jointIndex = Hips = 0
jointIndex = LeftArm = 19
jointIndex = LeftForeArm = 20
jointIndex = RightLeg = 2
jointIndex = LeftHandIndex4 = 25
jointIndex = RightFoot = 3
jointIndex = LeftFoot = 6
jointIndex = Spine1 = 8
jointIndex = Object004 = 29
jointIndex = Object002 = 32
jointIndex = LeftHandIndex1 = 22
jointIndex = Spine = 7
jointIndex = Neck = 26
jointIndex = LeftHandIndex2 = 23
jointIndex = Head = 27
jointIndex = LeftHand = 21
jointIndex = LeftShoulder = 18
jointIndex = Object001 = 30
jointIndex = LeftHandIndex3 = 24
jointIndex = Object003 = 31
jointIndex = RightHandIndex4 = 17
jointIndex = LeftUpLeg = 4
