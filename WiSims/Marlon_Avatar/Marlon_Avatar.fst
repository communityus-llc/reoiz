name = Marlon_Avatar
type = body+head
scale = 1
filename = Marlon_Avatar/Marlon_Avatar.fbx
texdir = Marlon_Avatar/textures
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
joint = jointHead = Head
joint = jointRoot = Hips
joint = jointRightHand = RightHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Spine2 = 4
jointIndex = HeadTop_End = 15
jointIndex = RightShoulder = 5
jointIndex = Object003_001 = 0
jointIndex = RightHand = 8
jointIndex = RightArm = 6
jointIndex = RightForeArm = 7
jointIndex = LeftForeArm = 11
jointIndex = Hips = 1
jointIndex = LeftArm = 10
jointIndex = Spine1 = 3
jointIndex = Spine = 2
jointIndex = Neck = 13
jointIndex = Object002_001 = 16
jointIndex = Head = 14
jointIndex = LeftHand = 12
jointIndex = LeftShoulder = 9
jointIndex = Object001_001 = 17
