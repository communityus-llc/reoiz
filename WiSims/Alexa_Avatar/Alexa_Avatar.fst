name = Alexa_Avatar
type = body+head
scale = 1
filename = Alexa_Avatar/Alexa_Avatar.fbx
texdir = Alexa_Avatar/textures
joint = jointLeftHand = LeftHand
joint = jointRoot = Hips
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointHead = Head
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightUpLeg = 1
jointIndex = LeftLeg = 7
jointIndex = Head = 23
jointIndex = Spine1 = 12
jointIndex = RightFoot = 3
jointIndex = LeftUpLeg = 6
jointIndex = LeftShoulder = 18
jointIndex = Object002_001 = 25
jointIndex = LeftToe_End = 10
jointIndex = Spine2 = 13
jointIndex = LeftFoot = 8
jointIndex = RightToe_End = 5
jointIndex = polySurface11 = 27
jointIndex = LeftToeBase = 9
jointIndex = RightHand = 17
jointIndex = RightToeBase = 4
jointIndex = LeftArm = 19
jointIndex = RightForeArm = 16
jointIndex = RightArm = 15
jointIndex = polySurface10 = 28
jointIndex = LeftForeArm = 20
jointIndex = Hips = 0
jointIndex = RightLeg = 2
jointIndex = Spine = 11
jointIndex = hair = 26
jointIndex = LeftHand = 21
jointIndex = RightShoulder = 14
jointIndex = Neck = 22
jointIndex = HeadTop_End = 24
