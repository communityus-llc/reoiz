name = Nicole_Avatar
type = body+head
scale = 1
filename = Nicole_Avatar/Nicole_Avatar.fbx
texdir = Nicole_Avatar/textures
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
joint = jointHead = Head
joint = jointRoot = Hips
joint = jointRightHand = RightHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Spine2 = 3
jointIndex = HeadTop_End = 14
jointIndex = RightShoulder = 4
jointIndex = RightHand = 7
jointIndex = RightArm = 5
jointIndex = RightForeArm = 6
jointIndex = LeftForeArm = 10
jointIndex = Hips = 0
jointIndex = LeftArm = 9
jointIndex = Spine1 = 2
jointIndex = Object002 = 16
jointIndex = Spine = 1
jointIndex = Neck = 12
jointIndex = Head = 13
jointIndex = LeftHand = 11
jointIndex = LeftShoulder = 8
jointIndex = Object001 = 17
jointIndex = Object003 = 15
