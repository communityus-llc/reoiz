name = Chaz_Avatar1
type = body+head
scale = 1
filename = Chaz_Avatar1/Chaz_Avatar1.fbx
texdir = Chaz_Avatar1/textures
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
joint = jointHead = Head
joint = jointRoot = Hips
joint = jointRightHand = RightHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Spine2 = 13
jointIndex = HeadTop_End = 24
jointIndex = RightShoulder = 14
jointIndex = Object003_001 = 26
jointIndex = RightHand = 17
jointIndex = RightUpLeg = 1
jointIndex = RightArm = 15
jointIndex = RightForeArm = 16
jointIndex = LeftLeg = 7
jointIndex = Hips = 0
jointIndex = LeftArm = 19
jointIndex = LeftForeArm = 20
jointIndex = RightLeg = 2
jointIndex = RightFoot = 3
jointIndex = LeftFoot = 8
jointIndex = Spine1 = 12
jointIndex = Object004 = 25
jointIndex = LeftToe_End = 10
jointIndex = LeftToeBase = 9
jointIndex = Spine = 11
jointIndex = RightToe_End = 5
jointIndex = Neck = 22
jointIndex = Object002_001 = 27
jointIndex = Head = 23
jointIndex = LeftHand = 21
jointIndex = LeftShoulder = 18
jointIndex = RightToeBase = 4
jointIndex = LeftUpLeg = 6
jointIndex = Object001_001 = 28
