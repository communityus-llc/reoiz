name = Trevor_Avatar
type = body+head
scale = 1
filename = Trevor_Avatar/Trevor_Avatar.fbx
texdir = Trevor_Avatar/textures
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
joint = jointHead = Head
joint = jointRoot = Hips
joint = jointRightHand = RightHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = polySurface102 = 0
jointIndex = Spine2 = 10
jointIndex = HeadTop_End = 21
jointIndex = RightShoulder = 11
jointIndex = RightHand = 14
jointIndex = RightUpLeg = 2
jointIndex = RightArm = 12
jointIndex = RightForeArm = 13
jointIndex = LeftLeg = 6
jointIndex = Hips = 1
jointIndex = LeftArm = 16
jointIndex = LeftForeArm = 17
jointIndex = RightLeg = 3
jointIndex = RightFoot = 4
jointIndex = polySurface61 = 22
jointIndex = LeftFoot = 7
jointIndex = Spine1 = 9
jointIndex = Spine = 8
jointIndex = Neck = 19
jointIndex = Object002_001 = 23
jointIndex = Head = 20
jointIndex = LeftHand = 18
jointIndex = LeftShoulder = 15
jointIndex = LeftUpLeg = 5
