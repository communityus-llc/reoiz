name = Justice_Avatar
type = body+head
scale = 1
filename = Justice_Avatar/Justice_Avatar.fbx
texdir = Justice_Avatar/textures
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
joint = jointHead = Head
joint = jointRoot = Hips
joint = jointRightHand = RightHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Spine2 = 9
jointIndex = HeadTop_End = 20
jointIndex = RightShoulder = 10
jointIndex = Object003_001 = 22
jointIndex = RightHand = 13
jointIndex = RightUpLeg = 1
jointIndex = RightArm = 11
jointIndex = RightForeArm = 12
jointIndex = LeftLeg = 5
jointIndex = LeftForeArm = 16
jointIndex = Hips = 0
jointIndex = LeftArm = 15
jointIndex = RightLeg = 2
jointIndex = RightFoot = 3
jointIndex = LeftFoot = 6
jointIndex = Spine1 = 8
jointIndex = Object004 = 21
jointIndex = Spine = 7
jointIndex = Neck = 18
jointIndex = Object002_001 = 23
jointIndex = Head = 19
jointIndex = LeftHand = 17
jointIndex = LeftShoulder = 14
jointIndex = LeftUpLeg = 4
jointIndex = Object001_001 = 24
