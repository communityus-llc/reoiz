name = Beebee_Avatar
type = body+head
scale = 1
filename = Beebee_Avatar/Beebee_Avatar.fbx
texdir = Beebee_Avatar/textures
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointHead = Head
joint = jointNeck = Neck
joint = jointRoot = Hips
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftToe_End = 10
jointIndex = LeftArm = 19
jointIndex = LeftToeBase = 9
jointIndex = polySurface2 = 26
jointIndex = LeftShoulder = 18
jointIndex = LeftUpLeg = 6
jointIndex = RightToe_End = 5
jointIndex = RightLeg = 2
jointIndex = Head = 23
jointIndex = LeftFoot = 8
jointIndex = Neck = 22
jointIndex = Hips = 0
jointIndex = Spine = 11
jointIndex = RightShoulder = 14
jointIndex = RightToeBase = 4
jointIndex = LeftHand = 21
jointIndex = Object001_003 = 27
jointIndex = Object002_003 = 25
jointIndex = Spine2 = 13
jointIndex = LeftLeg = 7
jointIndex = LeftForeArm = 20
jointIndex = RightFoot = 3
jointIndex = Spine1 = 12
jointIndex = RightUpLeg = 1
jointIndex = RightForeArm = 16
jointIndex = HeadTop_End = 24
jointIndex = RightArm = 15
jointIndex = RightHand = 17
