name = Spencer_Avatar
type = body+head
scale = 1
filename = Spencer_Avatar/Spencer_Avatar.fbx
texdir = Spencer_Avatar/textures
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
joint = jointHead = Head
joint = jointRoot = Hips
joint = jointRightHand = RightHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Spine2 = 13
jointIndex = HeadTop_End = 24
jointIndex = RightShoulder = 14
jointIndex = RightHand = 17
jointIndex = polySurface6 = 30
jointIndex = RightUpLeg = 1
jointIndex = RightArm = 15
jointIndex = RightForeArm = 16
jointIndex = LeftLeg = 7
jointIndex = Hips = 0
jointIndex = LeftArm = 19
jointIndex = LeftForeArm = 20
jointIndex = polySurface8 = 28
jointIndex = RightLeg = 2
jointIndex = polySurface9 = 27
jointIndex = RightFoot = 3
jointIndex = polySurface4 = 29
jointIndex = LeftFoot = 8
jointIndex = Spine1 = 12
jointIndex = Object002 = 25
jointIndex = LeftToe_End = 10
jointIndex = polySurface10 = 26
jointIndex = LeftToeBase = 9
jointIndex = Spine = 11
jointIndex = RightToe_End = 5
jointIndex = Neck = 22
jointIndex = Head = 23
jointIndex = LeftHand = 21
jointIndex = LeftShoulder = 18
jointIndex = Object001 = 31
jointIndex = RightToeBase = 4
jointIndex = LeftUpLeg = 6
