name = Megaman_Avatar
type = body+head
scale = 2.25
filename = Megaman_Avatar/Megaman_Avatar.fbx
texdir = Megaman_Avatar/textures
joint = jointRightHand = RightHand
joint = jointLean = Spine
joint = jointHead = Head
joint = jointNeck = Neck
joint = jointRoot = Hips
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightFoot = 3
jointIndex = HeadTop_End = 40
jointIndex = RightHandIndex3 = 20
jointIndex = RightUpLeg = 1
jointIndex = MegaMan_RArm_Hand1 = 42
jointIndex = Head = 39
jointIndex = RightHandIndex2 = 19
jointIndex = RightForeArm = 16
jointIndex = RightHandThumb1 = 22
jointIndex = RightLeg = 2
jointIndex = LeftHandIndex2 = 31
jointIndex = Spine2 = 13
jointIndex = LeftHandIndex3 = 32
jointIndex = polySurface17 = 43
jointIndex = LeftToeBase = 9
jointIndex = LeftHandThumb3 = 36
jointIndex = LeftHandThumb2 = 35
jointIndex = Spine = 11
jointIndex = RightHandThumb4 = 25
jointIndex = Hips = 0
jointIndex = RightHand = 17
jointIndex = polySurface9 = 44
jointIndex = LeftLeg = 7
jointIndex = RightHandIndex4 = 21
jointIndex = RightHandIndex1 = 18
jointIndex = LeftToe_End = 10
jointIndex = RightShoulder = 14
jointIndex = RightToeBase = 4
jointIndex = LeftHandIndex4 = 33
jointIndex = polySurface16 = 46
jointIndex = LeftArm = 27
jointIndex = LeftHand = 29
jointIndex = Neck = 38
jointIndex = polySurface18 = 45
jointIndex = RightHandThumb3 = 24
jointIndex = LeftHandThumb4 = 37
jointIndex = polySurface19 = 41
jointIndex = RightArm = 15
jointIndex = RightHandThumb2 = 23
jointIndex = Spine1 = 12
jointIndex = LeftUpLeg = 6
jointIndex = LeftShoulder = 26
jointIndex = LeftFoot = 8
jointIndex = LeftHandIndex1 = 30
jointIndex = LeftForeArm = 28
jointIndex = RightToe_End = 5
jointIndex = LeftHandThumb1 = 34
