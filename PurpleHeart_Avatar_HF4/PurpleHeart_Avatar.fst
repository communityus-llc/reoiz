name = PurpleHeart_Avatar_HF4
type = body+head
scale = 1.35
filename = PurpleHeart_Avatar_HF4/PurpleHeart_Avatar_HF4.fbx
texdir = PurpleHeart_Avatar_HF4/textures
joint = jointNeck = Neck
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointHead = Head
joint = jointLeftHand = LeftHand
joint = jointRoot = Hips
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftHand = 41
jointIndex = RightHandPinky4 = 21
jointIndex = RightUpLeg = 1
jointIndex = LeftHandThumb1 = 58
jointIndex = RightHandPinky2 = 19
jointIndex = RightHandRing4 = 25
jointIndex = LeftHandPinky2 = 43
jointIndex = RightHandRing3 = 24
jointIndex = LeftArm = 39
jointIndex = RightHandThumb4 = 37
jointIndex = LeftHandPinky4 = 45
jointIndex = Spine2 = 13
jointIndex = RightHandIndex2 = 31
jointIndex = LeftHandMiddle3 = 52
jointIndex = LeftHandThumb3 = 60
jointIndex = RightHandMiddle2 = 27
jointIndex = LeftHandRing4 = 49
jointIndex = hair = 65
jointIndex = RightHandMiddle1 = 26
jointIndex = RightHandThumb3 = 36
jointIndex = RightHandIndex4 = 33
jointIndex = RightArm = 15
jointIndex = Head = 63
jointIndex = Spine1 = 12
jointIndex = body = 66
jointIndex = LeftHandPinky1 = 42
jointIndex = Spine = 11
jointIndex = RightHandIndex3 = 32
jointIndex = LeftHandIndex3 = 56
jointIndex = RightShoulder = 14
jointIndex = Neck = 62
jointIndex = LeftHandThumb2 = 59
jointIndex = LeftHandIndex2 = 55
jointIndex = LeftHandIndex1 = 54
jointIndex = RightHandMiddle3 = 28
jointIndex = RightFoot = 3
jointIndex = face = 67
jointIndex = LeftHandThumb4 = 61
jointIndex = RightHandPinky1 = 18
jointIndex = acc = 68
jointIndex = LeftHandMiddle4 = 53
jointIndex = LeftForeArm = 40
jointIndex = LeftUpLeg = 6
jointIndex = LeftHandRing1 = 46
jointIndex = LeftFoot = 8
jointIndex = RightHandMiddle4 = 29
jointIndex = LeftHandRing2 = 47
jointIndex = RightHandIndex1 = 30
jointIndex = LeftLeg = 7
jointIndex = RightHandThumb2 = 35
jointIndex = LeftToe_End = 10
jointIndex = HeadTop_End = 64
jointIndex = RightHandRing1 = 22
jointIndex = RightToe_End = 5
jointIndex = RightHand = 17
jointIndex = LeftHandIndex4 = 57
jointIndex = LeftHandPinky3 = 44
jointIndex = RightHandThumb1 = 34
jointIndex = RightLeg = 2
jointIndex = Hips = 0
jointIndex = LeftToeBase = 9
jointIndex = RightHandRing2 = 23
jointIndex = RightToeBase = 4
jointIndex = LeftHandRing3 = 48
jointIndex = LeftShoulder = 38
jointIndex = RightForeArm = 16
jointIndex = LeftHandMiddle2 = 51
jointIndex = RightHandPinky3 = 20
jointIndex = LeftHandMiddle1 = 50
