name = Samus_Avatar
type = body+head
scale = 2
filename = Samus_Avatar/Samus_Avatar.fbx
texdir = Samus_Avatar/textures
joint = jointLean = Spine
joint = jointLeftHand = LeftHand
joint = jointNeck = Neck
joint = jointHead = Head
joint = jointRoot = Hips
joint = jointRightHand = RightHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = rknee = 58
jointIndex = HeadTop_End = 44
jointIndex = LeftUpLeg = 6
jointIndex = RightToe_End = 5
jointIndex = RightUpLeg = 1
jointIndex = neck = 57
jointIndex = arm_cannon4_VIS_O_OBJ_ = 45
jointIndex = LeftHandRing3 = 28
jointIndex = lthigh = 65
jointIndex = RightToeBase = 4
jointIndex = rleg = 63
jointIndex = LeftHandThumb2 = 39
jointIndex = lshou = 52
jointIndex = rarm = 56
jointIndex = Spine1 = 12
jointIndex = LeftHandPinky4 = 25
jointIndex = rthigh = 64
jointIndex = Head = 43
jointIndex = LeftHandRing4 = 29
jointIndex = lfarm = 54
jointIndex = lhand = 47
jointIndex = lfoot = 49
jointIndex = RightShoulder = 14
jointIndex = Neck = 42
jointIndex = LeftHandPinky2 = 23
jointIndex = LeftHandIndex2 = 35
jointIndex = Hips = 0
jointIndex = RightForeArm = 16
jointIndex = LeftHandMiddle4 = 33
jointIndex = LeftToeBase = 9
jointIndex = tubes = 60
jointIndex = RightFoot = 3
jointIndex = LeftHandRing1 = 26
jointIndex = LeftHandThumb1 = 38
jointIndex = LeftFoot = 8
jointIndex = LeftHandIndex1 = 34
jointIndex = LeftHandMiddle1 = 30
jointIndex = LeftToe_End = 10
jointIndex = LeftForeArm = 20
jointIndex = larm = 53
jointIndex = rfoot = 48
jointIndex = LeftHandIndex4 = 37
jointIndex = piece = 61
jointIndex = LeftHandIndex3 = 36
jointIndex = RightLeg = 2
jointIndex = polySurface12 = 46
jointIndex = RightArm = 15
jointIndex = helm = 67
jointIndex = LeftLeg = 7
jointIndex = LeftHand = 21
jointIndex = LeftHandThumb3 = 40
jointIndex = LeftHandMiddle2 = 31
jointIndex = LeftHandPinky3 = 24
jointIndex = lleg = 66
jointIndex = LeftArm = 19
jointIndex = Spine2 = 13
jointIndex = lknee = 59
jointIndex = body = 50
jointIndex = Spine = 11
jointIndex = LeftHandMiddle3 = 32
jointIndex = lelbow = 55
jointIndex = rshou = 51
jointIndex = visor = 62
jointIndex = LeftShoulder = 18
jointIndex = RightHand = 17
jointIndex = LeftHandThumb4 = 41
jointIndex = LeftHandPinky1 = 22
jointIndex = LeftHandRing2 = 27
jointIndex = chest = 68
