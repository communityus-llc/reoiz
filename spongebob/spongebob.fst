name = spongebob
type = body+head
scale = 1
filename = spongebob/spongebob.fbx
texdir = spongebob/textures
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
joint = jointLean = Spine
joint = jointEyeRight = RightEye
joint = jointHead = Head
joint = jointEyeLeft = LeftEye
joint = jointRightHand = RightHand
joint = jointRoot = Hips
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
bs = BrowsU_L = browUp = 1
bs = JawOpen = mouthOpen = 1
jointIndex = RightFoot = 3
jointIndex = RightHandRing3 = 32
jointIndex = LeftArm = 35
jointIndex = LeftHandIndex2 = 47
jointIndex = RightHandIndex1 = 22
jointIndex = LeftHandMiddle2 = 43
jointIndex = LeftForeArm = 36
jointIndex = LeftHandMiddle4 = 45
jointIndex = LeftToeBase = 9
jointIndex = LeftHandIndex1 = 46
jointIndex = RightHandIndex3 = 24
jointIndex = RightUpLeg = 1
jointIndex = LeftHandRing3 = 40
jointIndex = RightHandRing1 = 30
jointIndex = Head = 55
jointIndex = RightEye = 58
jointIndex = Neck = 54
jointIndex = RightHandMiddle4 = 29
jointIndex = RightToeBase = 4
jointIndex = LeftHandMiddle1 = 42
jointIndex = LeftHandIndex3 = 48
jointIndex = RightHandIndex4 = 25
jointIndex = RightHandThumb2 = 19
jointIndex = body = 59
jointIndex = LeftUpLeg = 6
jointIndex = RightHandThumb3 = 20
jointIndex = Hips = 0
jointIndex = RightHandRing2 = 31
jointIndex = RightArm = 15
jointIndex = LeftHand = 37
jointIndex = Spine = 11
jointIndex = RightHandIndex2 = 23
jointIndex = LeftHandThumb4 = 53
jointIndex = LeftEye = 57
jointIndex = RightLeg = 2
jointIndex = LeftHandIndex4 = 49
jointIndex = LeftHandThumb1 = 50
jointIndex = Spine1 = 12
jointIndex = RightHandRing4 = 33
jointIndex = RightHandMiddle3 = 28
jointIndex = RightShoulder = 14
jointIndex = LeftToe_End = 10
jointIndex = RightHand = 17
jointIndex = LeftHandRing2 = 39
jointIndex = LeftHandThumb2 = 51
jointIndex = LeftFoot = 8
jointIndex = LeftHandThumb3 = 52
jointIndex = RightToe_End = 5
jointIndex = LeftShoulder = 34
jointIndex = RightHandMiddle2 = 27
jointIndex = Spine2 = 13
jointIndex = LeftHandRing1 = 38
jointIndex = RightHandMiddle1 = 26
jointIndex = LeftLeg = 7
jointIndex = HeadTop_End = 56
jointIndex = RightForeArm = 16
jointIndex = LeftHandMiddle3 = 44
jointIndex = RightHandThumb4 = 21
jointIndex = RightHandThumb1 = 18
jointIndex = LeftHandRing4 = 41
