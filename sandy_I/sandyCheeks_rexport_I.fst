name = sandyCheeks_rexport_I
type = body+head
scale = 1
filename = sandyCheeks_rexport_I/sandyCheeks_rexport_I.fbx
texdir = sandyCheeks_rexport_I/textures
joint = jointRoot = Hips
joint = jointHead = HeadTop_End
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
bs = EyeBlink_R = Blink_Right = 1
bs = MouthFrown_R = Frown_Right = 1
bs = EyeSquint_R = Squint_Right = 1
bs = ChinUpperRaise = UpperLipUp_Right = 0.5
bs = ChinUpperRaise = UpperLipUp_Left = 0.5
bs = MouthDimple_L = Smile_Left = 0.25
bs = EyeSquint_L = Squint_Left = 1
bs = JawFwd = JawForeward = 1
bs = EyeOpen_R = EyesWide_Right = 1
bs = MouthSmile_R = Smile_Right = 1
bs = BrowsU_C = BrowsUp_Right = 1
bs = BrowsU_C = BrowsUp_Left = 1
bs = LipsUpperClose = UpperLipIn = 1
bs = MouthSmile_L = Smile_Left = 1
bs = EyeBlink_L = Blink_Left = 1
bs = BrowsD_L = BrowsDown_Left = 1
bs = LipsFunnel = TongueUp = 1
bs = LipsFunnel = MouthWhistle_NarrowAdjust_Right = 0.5
bs = LipsFunnel = MouthWhistle_NarrowAdjust_Left = 0.5
bs = LipsFunnel = MouthNarrow_Right = 1
bs = LipsFunnel = MouthNarrow_Left = 1
bs = LipsFunnel = Jaw_Down = 0.36
bs = LipsFunnel = JawForeward = 0.39
bs = LipsLowerOpen = LowerLipOut = 1
bs = MouthLeft = Midmouth_Left = 1
bs = BrowsU_L = BrowsUp_Left = 1
bs = LipsLowerClose = LowerLipIn = 1
bs = JawOpen = MouthOpen = 0.7
bs = JawRight = Jaw_Right = 1
bs = LipsPucker = MouthNarrow_Right = 1
bs = LipsPucker = MouthNarrow_Left = 1
bs = JawLeft = JawRotateY_Left = 0.5
bs = MouthRight = Midmouth_Right = 1
bs = BrowsD_R = BrowsDown_Right = 1
bs = LipsUpperOpen = UpperLipOut = 1
bs = MouthFrown_L = Frown_Left = 1
bs = Puff = CheekPuff_Right = 1
bs = Puff = CheekPuff_Left = 1
bs = LipsUpperUp = UpperLipUp_Right = 0.7
bs = LipsUpperUp = UpperLipUp_Left = 0.7
bs = ChinLowerRaise = Jaw_Up = 1
bs = EyeOpen_L = EyesWide_Left = 1
bs = LipsLowerDown = LowerLipDown_Right = 0.7
bs = LipsLowerDown = LowerLipDown_Left = 0.7
bs = BrowsU_R = BrowsUp_Right = 1
bs = MouthDimple_R = Smile_Right = 0.25
bs = Sneer = Squint_Right = 0.5
bs = Sneer = Squint_Left = 0.5
bs = Sneer = NoseScrunch_Right = 0.75
bs = Sneer = NoseScrunch_Left = 0.75
jointIndex = LeftFoot = 9
jointIndex = Spine1 = 13
jointIndex = RightHandIndex2 = 20
jointIndex = LeftHandThumb3 = 37
jointIndex = RightHandIndex4 = 22
jointIndex = RightShoulder = 15
jointIndex = RightHandThumb1 = 23
jointIndex = RightFoot = 4
jointIndex = RightHand = 18
jointIndex = LeftToe_End = 11
jointIndex = Spine = 12
jointIndex = RightToeBase = 5
jointIndex = LeftHandIndex2 = 32
jointIndex = RightHandThumb3 = 25
jointIndex = RightLeg = 3
jointIndex = RightUpLeg = 2
jointIndex = RightHandThumb4 = 26
jointIndex = Head = 40
jointIndex = HeadTop_End = 41
jointIndex = RightHandIndex3 = 21
jointIndex = RightHandIndex1 = 19
jointIndex = LeftHandThumb1 = 35
jointIndex = LeftArm = 28
jointIndex = RightHandThumb2 = 24
jointIndex = LeftToeBase = 10
jointIndex = LeftShoulder = 27
jointIndex = LeftHand = 30
jointIndex = LeftForeArm = 29
jointIndex = Mesh = 1
jointIndex = Neck = 39
jointIndex = LeftHandThumb2 = 36
jointIndex = LeftHandIndex1 = 31
jointIndex = LeftUpLeg = 7
jointIndex = RightArm = 16
jointIndex = Spine2 = 14
jointIndex = LeftHandThumb4 = 38
jointIndex = RightForeArm = 17
jointIndex = RightToe_End = 6
jointIndex = LeftHandIndex3 = 33
jointIndex = LeftLeg = 8
jointIndex = LeftHandIndex4 = 34
jointIndex = Hips = 0
