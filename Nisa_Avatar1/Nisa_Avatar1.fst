name = Nisa_Avatar1
type = body+head
scale = 1.04
filename = Nisa_Avatar1/Nisa_Avatar1.fbx
texdir = Nisa_Avatar1/textures
joint = jointRightHand = RightHand
joint = jointLean = Spine
joint = jointHead = Head
joint = jointLeftHand = LeftHand
joint = jointRoot = Hips
joint = jointNeck = Neck
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = LeftHandMiddle4 = 53
jointIndex = RightHandIndex4 = 33
jointIndex = LeftToe_End = 10
jointIndex = RightHandPinky4 = 21
jointIndex = RightToeBase = 4
jointIndex = LeftHandPinky4 = 45
jointIndex = LeftHandMiddle2 = 51
jointIndex = RightHandRing3 = 24
jointIndex = LeftToeBase = 9
jointIndex = LeftHandRing2 = 47
jointIndex = LeftArm = 39
jointIndex = RightHandMiddle1 = 26
jointIndex = polySurface2 = 65
jointIndex = transform1 = 69
jointIndex = LeftHandThumb4 = 61
jointIndex = RightHandIndex2 = 31
jointIndex = LeftHandRing3 = 48
jointIndex = RightHandThumb1 = 34
jointIndex = LeftHandPinky1 = 42
jointIndex = LeftHandThumb1 = 58
jointIndex = LeftShoulder = 38
jointIndex = Head = 63
jointIndex = SMD_root_3 = 70
jointIndex = RightHandMiddle4 = 29
jointIndex = RightHandRing4 = 25
jointIndex = RightHandIndex3 = 32
jointIndex = LeftHandIndex2 = 55
jointIndex = RightHandMiddle2 = 27
jointIndex = RightHandPinky3 = 20
jointIndex = LeftHandIndex4 = 57
jointIndex = RightHandIndex1 = 30
jointIndex = RightUpLeg = 1
jointIndex = HeadTop_End = 64
jointIndex = Spine = 11
jointIndex = RightHandThumb2 = 35
jointIndex = LeftHandIndex1 = 54
jointIndex = RightLeg = 2
jointIndex = RightHandThumb4 = 37
jointIndex = LeftUpLeg = 6
jointIndex = Spine2 = 13
jointIndex = LeftLeg = 7
jointIndex = RightHandRing1 = 22
jointIndex = LeftHand = 41
jointIndex = RightHand = 17
jointIndex = LeftForeArm = 40
jointIndex = SMD_root_1 = 68
jointIndex = RightToe_End = 5
jointIndex = LeftFoot = 8
jointIndex = SMD_root_ = 67
jointIndex = polySurface1 = 66
jointIndex = Spine1 = 12
jointIndex = LeftHandPinky3 = 44
jointIndex = LeftHandThumb2 = 59
jointIndex = LeftHandIndex3 = 56
jointIndex = RightHandPinky2 = 19
jointIndex = LeftHandRing1 = 46
jointIndex = RightHandThumb3 = 36
jointIndex = LeftHandMiddle3 = 52
jointIndex = LeftHandMiddle1 = 50
jointIndex = RightHandRing2 = 23
jointIndex = RightForeArm = 16
jointIndex = RightHandMiddle3 = 28
jointIndex = Hips = 0
jointIndex = LeftHandThumb3 = 60
jointIndex = Neck = 62
jointIndex = RightArm = 15
jointIndex = RightHandPinky1 = 18
jointIndex = LeftHandRing4 = 49
jointIndex = RightShoulder = 14
jointIndex = LeftHandPinky2 = 43
jointIndex = RightFoot = 3
