name = Plutia_Avatar3
type = body+head
scale = 1.1
filename = Plutia_Avatar3/Plutia_Avatar3.fbx
texdir = Plutia_Avatar3/textures
joint = jointLean = Spine
joint = jointRoot = Hips
joint = jointLeftHand = LeftHand
joint = jointNeck = Neck
joint = jointHead = Head
joint = jointRightHand = RightHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightToe_End = 5
jointIndex = LeftHandThumb2 = 59
jointIndex = polySurface13 = 70
jointIndex = LeftHandPinky4 = 45
jointIndex = LeftFoot = 8
jointIndex = RightHandIndex2 = 31
jointIndex = LeftHandMiddle4 = 53
jointIndex = RightHandPinky4 = 21
jointIndex = HeadTop_End = 64
jointIndex = RightHandRing1 = 22
jointIndex = RightHandRing4 = 25
jointIndex = LeftArm = 39
jointIndex = LeftHandPinky3 = 44
jointIndex = Neck = 62
jointIndex = LeftForeArm = 40
jointIndex = RightLeg = 2
jointIndex = RightHandIndex4 = 33
jointIndex = RightHandMiddle4 = 29
jointIndex = Plutia_smd_SMD_root_2 = 65
jointIndex = LeftHandRing4 = 49
jointIndex = RightHandThumb2 = 35
jointIndex = RightHandMiddle2 = 27
jointIndex = LeftUpLeg = 6
jointIndex = LeftLeg = 7
jointIndex = LeftHandRing2 = 47
jointIndex = LeftHandIndex1 = 54
jointIndex = LeftHandThumb4 = 61
jointIndex = RightHandThumb1 = 34
jointIndex = RightHandPinky1 = 18
jointIndex = LeftHandPinky2 = 43
jointIndex = LeftHandIndex3 = 56
jointIndex = LeftHandMiddle2 = 51
jointIndex = Spine1 = 12
jointIndex = Spine = 11
jointIndex = RightHandPinky2 = 19
jointIndex = RightHandIndex3 = 32
jointIndex = RightArm = 15
jointIndex = LeftHandRing1 = 46
jointIndex = LeftHand = 41
jointIndex = LeftHandThumb1 = 58
jointIndex = Plutia_smd_SMD_root_1 = 68
jointIndex = RightHandRing2 = 23
jointIndex = LeftToe_End = 10
jointIndex = LeftHandMiddle1 = 50
jointIndex = LeftHandPinky1 = 42
jointIndex = RightHandMiddle1 = 26
jointIndex = LeftToeBase = 9
jointIndex = RightHandThumb3 = 36
jointIndex = Spine2 = 13
jointIndex = RightHandThumb4 = 37
jointIndex = LeftHandThumb3 = 60
jointIndex = RightHandIndex1 = 30
jointIndex = RightShoulder = 14
jointIndex = RightHandRing3 = 24
jointIndex = LeftHandIndex2 = 55
jointIndex = RightToeBase = 4
jointIndex = polySurface9 = 67
jointIndex = LeftHandRing3 = 48
jointIndex = Head = 63
jointIndex = RightHand = 17
jointIndex = RightForeArm = 16
jointIndex = LeftHandMiddle3 = 52
jointIndex = RightUpLeg = 1
jointIndex = RightHandMiddle3 = 28
jointIndex = RightHandPinky3 = 20
jointIndex = polySurface1 = 69
jointIndex = RightFoot = 3
jointIndex = polySurface12 = 66
jointIndex = LeftHandIndex4 = 57
jointIndex = LeftShoulder = 38
jointIndex = Hips = 0
