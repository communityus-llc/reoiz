name = Mario_Avatar
type = body+head
scale = 5
filename = Mario_Avatar/Mario_Avatar.fbx
texdir = Mario_Avatar/textures
joint = jointRoot = Hips
joint = jointNeck = Neck
joint = jointHead = Head
joint = jointLeftHand = LeftHand
joint = jointRightHand = RightHand
joint = jointLean = Spine
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = Mario_Eye_VIS_O_OBJ_2 = 79
jointIndex = transform20 = 99
jointIndex = RightToeBase = 4
jointIndex = RightHandMiddle2 = 27
jointIndex = transform10 = 111
jointIndex = RightHandIndex1 = 30
jointIndex = LeftHandPinky3 = 44
jointIndex = transform12 = 105
jointIndex = LeftHandIndex2 = 55
jointIndex = RightHandPinky2 = 19
jointIndex = polySurface10 = 112
jointIndex = transform5 = 121
jointIndex = transform24 = 67
jointIndex = LeftHandMiddle3 = 52
jointIndex = RightForeArm = 16
jointIndex = LeftHandThumb4 = 61
jointIndex = RightHandRing3 = 24
jointIndex = RightHand = 17
jointIndex = LeftUpLeg = 6
jointIndex = Hips = 0
jointIndex = transform21 = 101
jointIndex = transform22 = 103
jointIndex = transform7 = 117
jointIndex = transform27 = 135
jointIndex = RightHandIndex4 = 33
jointIndex = RightHandIndex3 = 32
jointIndex = polySurface22 = 90
jointIndex = polySurface20 = 94
jointIndex = Gamemodel_ = 81
jointIndex = LeftHandRing2 = 47
jointIndex = transform18 = 95
jointIndex = LeftShoulder = 38
jointIndex = transform6 = 123
jointIndex = polySurface4 = 128
jointIndex = RightShoulder = 14
jointIndex = RightHandPinky4 = 21
jointIndex = polySurface15 = 104
jointIndex = polySurface11 = 83
jointIndex = polySurface24 = 86
jointIndex = Mario_Hathead_VIS_O_OBJ_ = 130
jointIndex = RightUpLeg = 1
jointIndex = RightHandPinky1 = 18
jointIndex = polySurface51 = 69
jointIndex = RightHandIndex2 = 31
jointIndex = polySurface18 = 98
jointIndex = group = 72
jointIndex = transform26 = 137
jointIndex = polySurface161 = 66
jointIndex = transform13 = 109
jointIndex = Mario_Down_VIS_O_OBJ_ = 126
jointIndex = LeftLeg = 7
jointIndex = Neck = 62
jointIndex = transform14 = 97
jointIndex = transform2 = 141
jointIndex = LeftHandIndex4 = 57
jointIndex = polySurface16 = 102
jointIndex = LeftHandMiddle1 = 50
jointIndex = transform1 = 139
jointIndex = transform11 = 107
jointIndex = transform15 = 87
jointIndex = RightHandRing4 = 25
jointIndex = LeftHandPinky4 = 45
jointIndex = polySurface25 = 85
jointIndex = LeftHandThumb3 = 60
jointIndex = LeftHand = 41
jointIndex = RightHandThumb3 = 36
jointIndex = polySurface7 = 118
jointIndex = RightHandThumb4 = 37
jointIndex = LeftHandThumb1 = 58
jointIndex = LeftHandPinky2 = 43
jointIndex = LeftHandPinky1 = 42
jointIndex = LeftHandRing3 = 48
jointIndex = RightHandMiddle3 = 28
jointIndex = Mario_Eye_VIS_O_OBJ_3 = 131
jointIndex = LeftHandMiddle4 = 53
jointIndex = polySurface5 = 122
jointIndex = transform25 = 125
jointIndex = polySurface71 = 68
jointIndex = polySurface19 = 96
jointIndex = polySurface31 = 129
jointIndex = LeftForeArm = 40
jointIndex = polySurface17 = 100
jointIndex = transform23 = 70
jointIndex = transform17 = 91
jointIndex = polySurface8 = 116
jointIndex = RightFoot = 3
jointIndex = polySurface14 = 106
jointIndex = transform28 = 133
jointIndex = RightToe_End = 5
jointIndex = transform3 = 113
jointIndex = transform8 = 119
jointIndex = transform16 = 89
jointIndex = LeftToe_End = 10
jointIndex = LeftFoot = 8
jointIndex = Mario_Hair_VIS_O_OBJ_ = 124
jointIndex = polySurface1 = 136
jointIndex = RightHandThumb1 = 34
jointIndex = polySurface21 = 92
jointIndex = Mario_Eye_VIS_O_OBJ__Layer_3 = 138
jointIndex = RightHandMiddle4 = 29
jointIndex = LeftHandThumb2 = 59
jointIndex = Spine1 = 12
jointIndex = LeftHandMiddle2 = 51
jointIndex = transform4 = 115
jointIndex = RightLeg = 2
jointIndex = polySurface6 = 120
jointIndex = Mario_Eye_VIS_O_OBJ_ = 140
jointIndex = transform9 = 84
jointIndex = polySurface13 = 108
jointIndex = RightHandRing1 = 22
jointIndex = RightArm = 15
jointIndex = polySurface91 = 71
jointIndex = transform19 = 93
jointIndex = RightHandRing2 = 23
jointIndex = Spine2 = 13
jointIndex = RightHandThumb2 = 35
jointIndex = LeftHandRing4 = 49
jointIndex = polySurface23 = 88
jointIndex = RightHandMiddle1 = 26
jointIndex = Spine = 11
jointIndex = LeftHandIndex1 = 54
jointIndex = Head = 63
jointIndex = LeftArm = 39
jointIndex = polySurface26 = 65
jointIndex = polySurface2 = 134
jointIndex = polySurface3 = 132
jointIndex = LeftToeBase = 9
jointIndex = LeftHandRing1 = 46
jointIndex = polySurface12 = 110
jointIndex = RightHandPinky3 = 20
jointIndex = polySurface9 = 114
jointIndex = LeftHandIndex3 = 56
jointIndex = HeadTop_End = 64
