name = Barney-Gumble
type = body+head
scale = 1
filename = Barney-Gumble/Barney-Gumble.fbx
texdir = Barney-Gumble/textures
joint = jointNeck = Neck
joint = jointRightHand = RightHand
joint = jointEyeLeft = LeftEye
joint = jointRoot = Hips
joint = jointLeftHand = LeftHand
joint = jointHead = Head
joint = jointEyeRight = RightEye
joint = jointLean = Spine
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
jointIndex = RightToe_End = 5
jointIndex = LeftHand = 25
jointIndex = RightHandIndex1 = 18
jointIndex = RightShoulder = 14
jointIndex = barney_h_merged = 35
jointIndex = LeftToeBase = 9
jointIndex = RightForeArm = 16
jointIndex = LeftToe_End = 10
jointIndex = Spine2 = 13
jointIndex = RightToeBase = 4
jointIndex = HeadTop_End = 34
jointIndex = Hips = 0
jointIndex = Spine = 11
jointIndex = LeftHandIndex1 = 26
jointIndex = Neck = 30
jointIndex = RightHand = 17
jointIndex = LeftShoulder = 22
jointIndex = RightLeg = 2
jointIndex = LeftForeArm = 24
jointIndex = LeftHandIndex2 = 27
jointIndex = LeftUpLeg = 6
jointIndex = LeftHandIndex4 = 29
jointIndex = RightUpLeg = 1
jointIndex = LeftEye = 32
jointIndex = RightArm = 15
jointIndex = RightFoot = 3
jointIndex = LeftLeg = 7
jointIndex = Spine1 = 12
jointIndex = RightHandIndex4 = 21
jointIndex = LeftArm = 23
jointIndex = Head = 31
jointIndex = LeftHandIndex3 = 28
jointIndex = LeftFoot = 8
jointIndex = RightHandIndex3 = 20
jointIndex = RightEye = 33
jointIndex = RightHandIndex2 = 19
