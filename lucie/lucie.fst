name = lucie
type = body+head
scale = 1
filename = lucie/lucie.fbx
texdir = lucie/textures
joint = jointRoot = Hips
joint = jointHead = HeadTop_End
joint = jointLean = Spine
joint = jointRightHand = RightHand
joint = jointNeck = Neck
joint = jointLeftHand = LeftHand
freeJoint = LeftArm
freeJoint = LeftForeArm
freeJoint = RightArm
freeJoint = RightForeArm
bs = EyeBlink_R = Blink_Right = 1
bs = MouthFrown_R = Frown_Right = 1
bs = EyeSquint_R = Squint_Right = 1
bs = ChinUpperRaise = UpperLipUp_Right = 0.5
bs = ChinUpperRaise = UpperLipUp_Left = 0.5
bs = MouthDimple_L = Smile_Left = 0.25
bs = EyeSquint_L = Squint_Left = 1
bs = JawFwd = JawForeward = 1
bs = EyeOpen_R = EyesWide_Right = 1
bs = MouthSmile_R = Smile_Right = 1
bs = BrowsU_C = BrowsUp_Right = 1
bs = BrowsU_C = BrowsUp_Left = 1
bs = LipsUpperClose = UpperLipIn = 1
bs = MouthSmile_L = Smile_Left = 1
bs = EyeBlink_L = Blink_Left = 1
bs = BrowsD_L = BrowsDown_Left = 1
bs = LipsFunnel = TongueUp = 1
bs = LipsFunnel = MouthWhistle_NarrowAdjust_Right = 0.5
bs = LipsFunnel = MouthWhistle_NarrowAdjust_Left = 0.5
bs = LipsFunnel = MouthNarrow_Right = 1
bs = LipsFunnel = MouthNarrow_Left = 1
bs = LipsFunnel = Jaw_Down = 0.36
bs = LipsFunnel = JawForeward = 0.39
bs = LipsLowerOpen = LowerLipOut = 1
bs = MouthLeft = Midmouth_Left = 1
bs = BrowsU_L = BrowsUp_Left = 1
bs = LipsLowerClose = LowerLipIn = 1
bs = JawOpen = MouthOpen = 0.7
bs = JawRight = Jaw_Right = 1
bs = LipsPucker = MouthNarrow_Right = 1
bs = LipsPucker = MouthNarrow_Left = 1
bs = JawLeft = JawRotateY_Left = 0.5
bs = MouthRight = Midmouth_Right = 1
bs = BrowsD_R = BrowsDown_Right = 1
bs = LipsUpperOpen = UpperLipOut = 1
bs = MouthFrown_L = Frown_Left = 1
bs = Puff = CheekPuff_Right = 1
bs = Puff = CheekPuff_Left = 1
bs = LipsUpperUp = UpperLipUp_Right = 0.7
bs = LipsUpperUp = UpperLipUp_Left = 0.7
bs = ChinLowerRaise = Jaw_Up = 1
bs = EyeOpen_L = EyesWide_Left = 1
bs = LipsLowerDown = LowerLipDown_Right = 0.7
bs = LipsLowerDown = LowerLipDown_Left = 0.7
bs = BrowsU_R = BrowsUp_Right = 1
bs = MouthDimple_R = Smile_Right = 0.25
bs = Sneer = Squint_Right = 0.5
bs = Sneer = Squint_Left = 0.5
bs = Sneer = NoseScrunch_Right = 0.75
bs = Sneer = NoseScrunch_Left = 0.75
jointIndex = Alchemist_GEO = 1
jointIndex = LeftFoot = 9
jointIndex = Spine1 = 13
jointIndex = RightHandIndex2 = 24
jointIndex = LeftHandThumb3 = 45
jointIndex = RightHandIndex4 = 26
jointIndex = RightShoulder = 15
jointIndex = RightHandThumb1 = 27
jointIndex = RightFoot = 4
jointIndex = RightHand = 18
jointIndex = LeftToe_End = 11
jointIndex = RightHandRing3 = 21
jointIndex = Spine = 12
jointIndex = RightToeBase = 5
jointIndex = LeftHandIndex2 = 40
jointIndex = RightHandRing2 = 20
jointIndex = LeftHandRing3 = 37
jointIndex = RightHandThumb3 = 29
jointIndex = RightLeg = 3
jointIndex = RightHandRing1 = 19
jointIndex = RightUpLeg = 2
jointIndex = RightHandThumb4 = 30
jointIndex = Head = 48
jointIndex = HeadTop_End = 49
jointIndex = RightHandIndex3 = 25
jointIndex = RightHandIndex1 = 23
jointIndex = LeftArm = 32
jointIndex = RightHandThumb2 = 28
jointIndex = LeftHandThumb1 = 43
jointIndex = LeftToeBase = 10
jointIndex = LeftShoulder = 31
jointIndex = LeftHand = 34
jointIndex = LeftForeArm = 33
jointIndex = LeftHandRing2 = 36
jointIndex = Neck = 47
jointIndex = LeftHandThumb2 = 44
jointIndex = LeftUpLeg = 7
jointIndex = RightHandRing4 = 22
jointIndex = LeftHandIndex1 = 39
jointIndex = RightArm = 16
jointIndex = LeftHandRing4 = 38
jointIndex = Spine2 = 14
jointIndex = LeftHandThumb4 = 46
jointIndex = RightForeArm = 17
jointIndex = LeftHandRing1 = 35
jointIndex = RightToe_End = 6
jointIndex = LeftHandIndex3 = 41
jointIndex = LeftLeg = 8
jointIndex = LeftHandIndex4 = 42
jointIndex = Hips = 0
